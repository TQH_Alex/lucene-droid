package com.example.alexbacker.hellodroid40;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.*;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Alex Backer on 11/1/2016.
 */
public class SearchableActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, android.support.v7.widget.SearchView.OnQueryTextListener {

    private SearchView searchView;
    private String mSearchString;
    private static final String SEARCH_KEY = "search";
    private TextView txt; // hien thi query tu khoa tim kiem
    private TextView txtM; // hien thi so ket qua tim kiem
    private ListView lv; // hien thi listview
    private EditText editTextSearch; // thay doi search query
    private static String sendQuery; // de chuyen qua lop khac
    ArrayList<Doc> your_array_list = new ArrayList<Doc>(); // full list
    private SuggestWord suggestWord;
//menu check dau tieng viet
    MenuItem itemAccent;
    boolean itemAccentCheckBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);
        txt = (TextView) findViewById(R.id.textView);
        txtM = (TextView) findViewById(R.id.textView2);
        editTextSearch = (EditText)findViewById(R.id.editTextSearch);
        getSupportActionBar().setTitle("Tìm kiếm");
        txt.setTextIsSelectable(true);
        // duong dan toi root view, bo vao lam path cho lucene
        Intent intent = getIntent();
        final String query;
        String path;

        //lay du lieu check box tim kiem khong dau
        itemAccentCheckBox = getFromSP("checkAccent");

        // xet chuoi di tu dau
        if (!(intent.ACTION_SEARCH.equals(intent.getAction()))) {
            query = intent.getStringExtra("View_Histoty_Item_ID");

        } else {
            query = intent.getStringExtra(SearchManager.QUERY);

        }
        if (IOStreamData.isExsit("ViewHistory.txt", query, this)) {
            IOStreamData.writeToFile("ViewHistory.txt", query, this);
        }
        sendQuery = query;

        txt.setText("Bạn đang tìm kiếm : ");
        editTextSearch.setText(query);


        // Tu goi y
        try {
            suggestWord = SuggestLuceneService.suggestLucene(MainActivity.indexPathSpell,"contents",query);
        } catch (Exception e) {
            Log.d("Suggest",e.toString());
            e.printStackTrace();
        }
        if(suggestWord.isShoudSuggest() == true) {
            txt.setText("Ý của bạn là : ");
            editTextSearch.setText(suggestWord.getSuggestWord());
            editTextSearch.setTextColor(Color.parseColor("#2F0DAB"));
            editTextSearch.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEvent.ACTION_UP == event.getAction()) {
                        searchView.setQuery(suggestWord.getSuggestWord(), true);
                    }

                    return true; // return is important...
                }
            });
        }
        // ket thuc phan tu goi y
        SearchFileLucene searchLucene = new SearchFileLucene();

        try {
            // kiem tra check box tim kiem khong dau
            if(!itemAccentCheckBox)
            {
               path = MainActivity.indexPath;
            }
            else
            {
                path = MainActivity.indexPathNoAccent;
            }
            your_array_list = searchLucene.Search(query, path,itemAccentCheckBox);
            if (your_array_list.isEmpty()) {
                txtM.setText("Không có kết quả nào cho từ khóa này.");
            } else {

                txtM.setText("Có " + SearchFileLucene.getTotalHit() + " kết quả trùng khớp với từ khóa bạn tìm kiếm");
            }


        } catch (Exception e) {

            e.printStackTrace();
        }
        lv = (ListView) findViewById(R.id.lvLucene);
        lv.setAdapter(new CustomListAdapter(this, your_array_list));
        final ArrayList<Doc> finalYour_array_list = your_array_list;
        // nhan vao mot trong cac
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                // chuyen du lieu qua lop WebViewActivity
                if (finalYour_array_list.get((int) l).get_name().contains("wikipedia")) {
                    String docName = finalYour_array_list.get((int) l).get_name().toString();
                    docName = docName.replace("txt", "html");
                    Intent intent = new Intent(getBaseContext(), WebViewActivity.class);
                    intent.putExtra("View_Item_ID", docName);
                    startActivity(intent);

                } else {
                    // chuyen du lieu qua lop ViewItemActivity
                    Intent intent = new Intent(getBaseContext(), ViewItemActivity.class);
                    intent.putExtra("View_Item_ID", finalYour_array_list.get((int) l).get_name().toString());

                    startActivity(intent);
                }
            }
        });

        //save state
//        LoadPreferences();
        if (savedInstanceState != null) {
            mSearchString = savedInstanceState.getString(SEARCH_KEY);
        } else {
            mSearchString = query;
        }
    }
    // lay query
    public static String getSendQuery(){return sendQuery;}

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mSearchString =  searchView.getQuery().toString();
        outState.putString(SEARCH_KEY, mSearchString);
    }
    @Override
    // thay doi ham sau de  truy cap vao vung muon doc du lieu
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_search, menu);
        final MenuItem searchItem = menu.findItem(R.id.search);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView)MenuItemCompat.getActionView(searchItem);
        if (null != searchView) {
            searchView.clearFocus();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            searchView.setIconifiedByDefault(false);
            searchView.setMaxWidth(Integer.MAX_VALUE);
            searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        }
        // Giu chu khi quay man hinh
       if (mSearchString != null && !mSearchString.isEmpty()) {

           searchItem.expandActionView();
            searchView.setQuery(mSearchString, false);
            searchView.clearFocus();
       }

        // luu du lieu checkbox khong dau
        itemAccent = menu.findItem(R.id.check_accent);
        itemAccent.setChecked(getFromSP("checkAccent"));

        return super.onCreateOptionsMenu(menu);
    }

    // Action bar - thanh di chuyen nhanh
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // quay ve trang chu
               Intent mainPage = new Intent(this,MainActivity.class);
                startActivity(mainPage);
                finish();
                break;
            case  R.id.search_history :
                //  di toi trang lich su

                Intent historyPage = new Intent(this,ViewHistoryActivity.class);
                startActivity(historyPage);
                //onPause();
                finish();
                break;
            // xu ly tim kiem khong dau
            case  R.id.check_accent :
                if (item.isChecked())
                {
                    item.setChecked(false);
                    saveInSp("checkAccent",false);
                    finish();
                    startActivity(getIntent());
                }
                else {
                    item.setChecked(true);
                    saveInSp("checkAccent",true);
                    finish();
                    startActivity(getIntent());
                }
                break;
          //  default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
             //   return super.onOptionsItemSelected(item);

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
    // ngung focus sau khi resume, chuyen che do ban phim khong bao het man hinh
    @Override
    public void onResume(){
        super.onResume();
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getWindow().getDecorView().clearFocus();
    }
    @Override
    public  void onBackPressed()
    {
        this.finish();
        super.onBackPressed();

    }
    private boolean getFromSP(String key){
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("PROJECT_NAME", android.content.Context.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }

    private void saveInSp(String key,boolean value){
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("PROJECT_NAME", android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.putBoolean(key, value);
        editor.commit();
    }
}

