package com.example.alexbacker.hellodroid40;

/**
 * Created by Alex Backer on 11/19/2016.
 */
public class ActivityController {
    public interface ActivityConstants {
        public static final int ACTIVITY_1 = 1001; // khoi dong tu mainActivity vao SearchableActivity
        public static final int ACTIVITY_2 = 1002;
        public static final int ACTIVITY_3 = 1003;
    }
}
