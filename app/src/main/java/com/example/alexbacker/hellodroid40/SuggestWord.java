package com.example.alexbacker.hellodroid40;

/**
 * Created by Alex Backer on 12/26/2016.
 */
public class SuggestWord {
    public boolean isShoudSuggest() {
        return shoudSuggest;
    }

    public void setShoudSuggest(boolean shoudSuggest) {
        this.shoudSuggest = shoudSuggest;
    }

    public String getSuggestWord() {
        return suggestWord;
    }

    public void setSuggestWord(String suggestWord) {
        this.suggestWord = suggestWord;
    }

    private boolean shoudSuggest;
    private  String suggestWord;
    public  SuggestWord()
    {

    }
    public  SuggestWord(boolean shoudSuggest,String suggestWord)
    {
        this.shoudSuggest = shoudSuggest;
        this.suggestWord = suggestWord;
    }
}
