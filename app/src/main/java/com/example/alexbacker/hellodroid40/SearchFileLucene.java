package com.example.alexbacker.hellodroid40;

/**
 * Created by Alex Backer on 11/2/2016.
 */

import android.util.Log;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.FSDirectory;
import org.lukhnos.portmobile.file.Files;
import org.lukhnos.portmobile.file.Path;
import org.lukhnos.portmobile.file.Paths;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;


public class SearchFileLucene
{
    public  static  int totalHit;

    public static ArrayList<Path> pathR()
    {
        String index = "index";
        ArrayList<Path> allListP = new ArrayList<Path>();

        allListP.add(Paths.get(index).toAbsolutePath());

        return allListP;
    }
    public static int getTotalHit()
    {
        return totalHit;
    }

    /** Simple command-line based search demo. */
    public ArrayList<Doc> Search(String inputQuery, String indexPath,boolean useAccentData) throws Exception {
        ArrayList<String> allList = new ArrayList<String>();
        ArrayList<Doc> allListDocs = new ArrayList<Doc>();
        String index =  indexPath;
        String field = "contents"; // su ly noi dung cua bai
        String field2 = "title"; // su ly title cua bai
        String queries = null;
        int repeat = 0;
        boolean raw = false;
        String queryString = null;
        int hitsPerPage = 250; // bao nhieu cho mot trang
        Analyzer analyzer;



        IndexReader reader = DirectoryReader.open(FSDirectory.open(Paths.get(index)));
        IndexSearcher searcher = new IndexSearcher(reader);
        // dung bo xu ly nao theo boolean cua tim kieam tieng viet
        if(!useAccentData) {
            analyzer = new StandardAnalyzer();
        }
        else
        {
             analyzer = new CustomAnalyzer();
        }
        BufferedReader in = null;
        if (queries != null) {
            in = Files.newBufferedReader(Paths.get(queries), Charset.forName("UTF-8"));
        } else {
            in = new BufferedReader(new InputStreamReader(System.in, Charset.forName("UTF-8")));
        }

        QueryParser parserContent = new QueryParser(field, analyzer);
        QueryParser parserTitle = new QueryParser(field2, analyzer);


        String line = inputQuery; // query duoc truyen vao tu ham


        /* old code
        // dua query vao de bat dau tim kiem
        //QueryParser.escape loai tru cac ki tu dac biet
        //Query query = parser.parse(QueryParser.escape(line));
        // System.out.println("Searching for: " + query.toString(field));
        */

        Query queryTitle = parserTitle.parse(QueryParser.escape(line));
        // max distance cua fuzzy la 2, de co the tra ve nhieu ket qua hon nen su dung SpellCheck
        Query queryContent = new FuzzyQuery(new Term(field,line));

        // Boolean Query de tim kiem tren nhieu mien du lieu khac nhau
        BooleanQuery.Builder booleanQuery = new BooleanQuery.Builder();
        booleanQuery.add(queryContent, BooleanClause.Occur.SHOULD);
        booleanQuery.add(queryTitle, BooleanClause.Occur.SHOULD);

        allListDocs = doPagingSearch(in, searcher, booleanQuery.build(), hitsPerPage, raw, queries == null && queryString == null);
        reader.close();

        return allListDocs;
    }

    public static ArrayList<Doc> doPagingSearch(BufferedReader in, IndexSearcher searcher, Query query,
                                                   int hitsPerPage, boolean raw, boolean interactive) throws IOException {
        ArrayList<Doc> allListDocs = new ArrayList<Doc>();
        TopDocs results = searcher.search(query, 5
                * hitsPerPage);
        // lay 5 * hitperPage de ra tong so hit doc
        ScoreDoc[] hits = results.scoreDocs;
        Log.d("hits:", String.valueOf(hits.length));

        int numTotalHits = results.totalHits;
        // lay tong so doc hit trong toan bo cac duong link co lien quan
        totalHit = numTotalHits;
        int start = 0;
        int end = Math.min(numTotalHits, hitsPerPage);
        while (true) {
            if (end > hits.length) {
                String line = in.readLine();
                if (line.length() == 0 || line.charAt(0) == 'n') {
                    break;
                }

                hits = searcher.search(query, numTotalHits).scoreDocs;
            }

            end = Math.min(hits.length, start + hitsPerPage);

            for (int i = start; i < end; i++) {
                if (raw) {                              // output raw format
                    //   System.out.println("doc=" + hits[i].doc + " score=" + hits[i].score);
                    continue;
                }

                Document doc = searcher.doc(hits[i].doc);
                String path = doc.get("path");
                String name = doc.get("name");
                String content = doc.get("title");
                String url = doc.get("url");

                if (path != null) {
                    //System.out.println((i+1) + ". " + path);
                    allListDocs.add(new Doc(name, url, content));
                }
            }
            return allListDocs;
        }
        return allListDocs;
    }


}
