package com.example.alexbacker.hellodroid40;

import java.util.ArrayList;

/**
 * Created by Alex Backer on 11/9/2016.
 */
public class Doc implements Cloneable{
    String _name;
    String _url;
    String _title;
    public Doc() {
    }
    public Doc(String name,String url ,String title)
    {
        this._name = name;
        this._url = url;
        this._title = title;
    }
    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }

    public String get_url() {
        return _url;
    }

    public void set_url(String _url) {
        this._url = _url;
    }
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
    public static ArrayList<Doc> limitList(ArrayList<Doc> list, int size) throws CloneNotSupportedException {
        ArrayList<Doc> clone = new ArrayList<>(list.subList(0, size));
        return clone;
    }

}
