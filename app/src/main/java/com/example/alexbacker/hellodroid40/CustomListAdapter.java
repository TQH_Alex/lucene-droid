package com.example.alexbacker.hellodroid40;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.Html;
import android.text.Spannable;
import android.text.style.BackgroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.Normalizer;
import java.util.List;

/**
 * Created by Alex Backer on 11/9/2016.
 */
public class CustomListAdapter extends BaseAdapter implements View.OnClickListener {

    private List<Doc> listData;
    private LayoutInflater layoutInflater;
    private Context context;

    @Override
    public void onClick(View view) {

    }

    static class ViewHolder {
        TextView fileNameView;
        TextView urlView;
        TextView titleView;
    }
    public CustomListAdapter(Context aContext,  List<Doc> listData) {
        this.context = aContext;
        this.listData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    @Override
    public Object getItem(int i) {
        return listData.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {

            convertView = layoutInflater.inflate(R.layout.list_item_layout, null);
            holder = new ViewHolder();
            holder.fileNameView = (TextView) convertView.findViewById(R.id.textView_FileName);
            holder.urlView = (TextView) convertView.findViewById(R.id.textView_Url);
            holder.titleView = (TextView) convertView.findViewById(R.id.textView_OpenPara);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final Doc doc = this.listData.get(position);
        String docName = doc.get_name();
        // neu la wiki thi sua duoi
        if(docName.contains("wikipedia"))
        {
            docName  =  docName.replace("txt","html");
        }
        Spannable spanText = Spannable.Factory.getInstance().newSpannable(docName);
        int end = docName.length();
        spanText.setSpan(new BackgroundColorSpan(0xFFFFFF00), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        holder.fileNameView.setText(spanText);


        // chinh mau cho url
        holder.urlView.setTextColor(Color.rgb(23, 102, 95));
        holder.urlView.setText(doc.get_url());


        // to dam chu
        String query = SearchableActivity.getSendQuery();
        String[] arr = query.split(" ");
        String origString = doc.get_title();
        origString = origString.replaceAll("[-+.^:,]","");
        for (String ss : arr) {
            if (ss.length() >= 1) {
                ss = ss.replace("*", "");
                ss= ss.replaceAll("[-+.^:,]","");
                origString = origString.replaceAll("(?iu)(" + ss + ")", "<B>" + "$1" + "</B>");
                //origString = Normalizer.normalize(origString, Normalizer.Form.NFD).replaceAll("(?iu)(" + ss + ")", "<b>" + "$1" + "</b>");
            }
        }
        //origString   = origString.replaceAll("(?i)"+query,"<b>"+query+"</b>");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.titleView.setText(Html.fromHtml(origString,Html.FROM_HTML_MODE_LEGACY));
        } else {
            holder.titleView.setText(Html.fromHtml(origString));
        }




        // su kien nhan ten file
        final String finalDocName = docName;
        holder.fileNameView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Neu la html chuyen qua WebViewActivity khong chuyen qua ViewItemActivity
                if(finalDocName.contains("html"))
                    {
                        Intent intent = new Intent(context, WebViewActivity.class);
                        intent.putExtra("View_Item_ID",holder.fileNameView.getText().toString());
                        context.startActivity(intent);
                    }
                else
                    {
                        // chuyen du lieu qua lop ViewItemActivity
                        Intent intent = new Intent(context, ViewItemActivity.class);
                        intent.putExtra("View_Item_ID",holder.fileNameView.getText().toString());
                        context.startActivity(intent);
                    }
            }
        });



        // su kien nhan Url o day
        holder.urlView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(holder.urlView.getText().toString()));
                context.startActivity(browserIntent);

            }
        });

        return convertView;
    }

}
