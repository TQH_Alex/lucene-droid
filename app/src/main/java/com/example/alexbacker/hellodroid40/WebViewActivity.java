package com.example.alexbacker.hellodroid40;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;

public class WebViewActivity extends AppCompatActivity {

    private WebView browser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        browser = (WebView) findViewById(R.id.webkit);



        // nhan du lieu tu  CustomListAdapter
        Intent intent = getIntent();
        String fileName = intent.getStringExtra("View_Item_ID");
        browser.loadUrl("file:///android_asset/database/"+fileName);

        browser.getSettings().setJavaScriptEnabled(false);

    }
}
