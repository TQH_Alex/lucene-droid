package com.example.alexbacker.hellodroid40;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class ViewHistoryActivity extends AppCompatActivity  implements AdapterView.OnItemClickListener, SearchView.OnQueryTextListener {
    private ListView lv; // hien thi listview
    //menu check dau tieng viet
    MenuItem itemAccent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_history);
        ArrayList<String> your_array_list = new ArrayList<String>();
        getSupportActionBar().setTitle("Các từ đã tìm");



        // doc tu file ViewHistory de hien thi
        your_array_list = IOStreamData.readFromFile("ViewHistory.txt",this);
        Collections.reverse(your_array_list);
        lv = (ListView)findViewById(R.id.lvHistory);
        lv.setAdapter(new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, your_array_list));
        // viet vao mot dong trong
        if(your_array_list.size() == 0)
        {
            IOStreamData.writeToFile("ViewHistory.txt","",this);
        }


        // nhan vao item de tim kiem lai
            final ArrayList<String> finalYour_array_list = your_array_list;
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    if(finalYour_array_list.get((int)l).toString().equalsIgnoreCase(""))
                    {}
                    else {
                        Intent intent = new Intent(getBaseContext(), SearchableActivity.class);
                        Toast.makeText(getBaseContext(), "You clicked Item: " + finalYour_array_list.get((int) l), Toast.LENGTH_SHORT).show();
                        intent.putExtra("View_Histoty_Item_ID", finalYour_array_list.get((int) l).toString());
                        startActivity(intent);
                    }
                }
            });


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    //xoa du lieu History
    public void clearHistory(View view) {
        ArrayList<String> your_array_list = new ArrayList<String>();
        your_array_list = IOStreamData.readFromFile("ViewHistory.txt",this);
        // kiem tra xem co du lieu de xoa khong
        if(your_array_list.size() == 1)
        {
            final Toast toast = Toast.makeText(getApplicationContext(), "Không có gì để xóa", Toast.LENGTH_SHORT);
            toast.show();

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    toast.cancel();
                }
            }, 300);

        }
        else {
            IOStreamData.clearFile("ViewHistory.txt", this);
            finish();
            startActivity(getIntent());
        }
    }
    @Override
    // thay doi ham sau de  truy cap vao vung muon doc du lieu
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main_search, menu);
        MenuItem searchItem = menu.findItem(R.id.search);

        final android.support.v7.widget.SearchView searchView = (android.support.v7.widget.SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(
                new ComponentName(this, SearchableActivity.class)));

        searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        searchView.setIconifiedByDefault(true); // true bo dau tim kiem , false hien dau tim kiem
        searchView.setMaxWidth(Integer.MAX_VALUE);
        // luu du lieu checkbox khong dau
        itemAccent = menu.findItem(R.id.check_accent);
        itemAccent.setChecked(getFromSP("checkAccent"));
        return true;
    }
    // Action bar
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                // quay ve trang chu
                Intent mainPage = new Intent(this,MainActivity.class);
                startActivity(mainPage);
                finish();
                break;
            case  R.id.search_history :
                //  di toi trang lich su
                Intent historyPage = new Intent(this,ViewHistoryActivity.class);
                startActivity(historyPage);
                //onPause();
                finish();
                break;
            case  R.id.check_accent :
                if (item.isChecked())
                {
                    item.setChecked(false);
                    saveInSp("checkAccent",false);
                }
                else {
                    item.setChecked(true);
                    saveInSp("checkAccent",true);
                }
                break;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
               break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }
    private boolean getFromSP(String key){
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("PROJECT_NAME", android.content.Context.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }

    private void saveInSp(String key,boolean value){
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("PROJECT_NAME", android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.putBoolean(key, value);
        editor.commit();
    }

}
