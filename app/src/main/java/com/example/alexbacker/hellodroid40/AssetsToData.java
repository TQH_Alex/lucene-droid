package com.example.alexbacker.hellodroid40;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.*;

/**
 * Created by Alex Backer on 11/3/2016.
 */
public class AssetsToData {
    public void copyAssets(  AssetManager assetManager ,String dirPath) {

        String[] files = null;
        try {
            files = assetManager.list("index");
        } catch (IOException e) {
            Log.e("tag", "Failed to get asset file list.", e);
        }
        if (files != null) for (String filename : files) {
            InputStream in = null;
            OutputStream out = null;
            try {
                in = assetManager.open(filename);
              File outFile = new File(dirPath, filename);
                out = new FileOutputStream(outFile);
                copyFile(in, out);
                Log.d("copy"," big success");
            } catch (IOException e) {
                Log.e("tag", "Failed to copy asset file: ", e);
            } finally {
                if (in != null) {
                    try {
                        in.close();
                    } catch (IOException e) {
                        // NOOP
                    }
                }
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e) {
                        // NOOP
                    }
                }
            }
        }
    }
    public File createFileFromInputStream(InputStream inputStream,String my_file_name) {

        try{
            File f = new File(my_file_name);
            OutputStream outputStream = new FileOutputStream(f);
            byte buffer[] = new byte[1024];
            int length = 0;

            while((length=inputStream.read(buffer)) > 0) {
                outputStream.write(buffer,0,length);
            }

            outputStream.close();
            inputStream.close();

            return f;
        }catch (IOException e) {
            //Logging exception
        }

        return null;
    }
    // Dung ham nay de chep du lieu tu asset vao root user
    public static boolean copyAssetFolder(AssetManager assetManager,
                                           String fromAssetPath, String toPath) {
        try {
            String[] files = assetManager.list(fromAssetPath);
           // new File(toPath).mkdirs();
            boolean res = true;
            for (String file : files)
                //if (file.contains("."))
                    res &= copyAsset(assetManager,
                            fromAssetPath + "/" + file,
                            toPath + "/" + file);
//                else
//                    res &= copyAssetFolder(assetManager,
//                            fromAssetPath + "/" + file,
//                            toPath + "/" + file);
            return res;
        } catch (Exception e) {
            Log.e("tag", "Failed to copy asset file: ", e);
            e.printStackTrace();
            return false;
        }
    }

    private static boolean copyAsset(AssetManager assetManager,
                                     String fromAssetPath, String toPath) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(fromAssetPath);
            //new File(toPath).createNewFile();
            out = new FileOutputStream(toPath);
            copyFile(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
            return true;
        } catch(Exception e) {
            Log.e("tag", "Failed to copy asset file: ", e);
            e.printStackTrace();
            return false;
        }
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }
    // save du lieu vao root
    public static void saveFolderToRoot(String folderName,String folderSaveToPath, AssetManager assetMgr )
    {
        File projDir = new File(folderSaveToPath);
        // File projDirHistory = new File(dirPathHistory);
        if (!projDir.exists()) {
            // chep du lieu tu index vao dirPath
            projDir.mkdirs();
            copyAssetFolder(assetMgr, folderName, folderSaveToPath);
        } else {
            Log.v(folderName, folderName +" dirPAth already exsit");
        }
    }
}
