package com.example.alexbacker.hellodroid40;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ViewItemActivity extends AppCompatActivity {

    TextView txtFileView; // text view cho file duoc mo
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_item);
        txtFileView = (TextView)findViewById(R.id.textView_FileItemName);




        //  nhan du lieu tu CustomListAdapter
        Intent intent = getIntent();
        String fileName = intent.getStringExtra("View_Item_ID");



        StringBuilder textFileContent = readFileAsset(fileName);



        //xu lu chuoi, xuong dong va boi den tu can tim
        String process = textFileContent.toString();
        String query = SearchableActivity.getSendQuery();
        String[] arr = query.split(" ");
        for (String ss : arr) {
            if (ss.length() >= 2) {
                ss = ss.replace("*", "");
                process = process.replaceAll("(?i)(" + ss + ")", "<b style = \"color:#2575ff\">$1</b>");
            }
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            txtFileView.setText(Html.fromHtml(process,Html.FROM_HTML_MODE_LEGACY));
        } else {
            txtFileView.setText(Html.fromHtml(process));
        }


    }
    // doc file tu asset
    public StringBuilder readFileAsset(String fileName)
    {
       // String fullText = "";
        BufferedReader reader = null;
        StringBuilder total = new StringBuilder();

        try {
            reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("database/"+fileName), "UTF-8"),2048);

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                total.append(mLine);
                total.append("<br />");
                total.append(System.getProperty("line.separator"));

            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return total;
    }
}
