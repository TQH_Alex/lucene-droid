package com.example.alexbacker.hellodroid40;

import android.content.Context;
import android.util.Log;

import java.io.*;
import java.util.ArrayList;

/**
 * Created by Alex Backer on 11/19/2016.
 */
public class IOStreamData {
    // Xoa du lieu
    public static void clearFile(String fileName,Context context) {
        try {
            BufferedWriter oFile = new BufferedWriter(new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_PRIVATE)));
            PrintWriter out = new PrintWriter(oFile);
            out.println("");
            out.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    // Kiem tra tu da ton tai trong danh sach chua
    public static boolean isExsit(String fileName,String data,Context context) {

        try {
            InputStream inputStream = context.openFileInput(fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString;
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    // dung equal de so sanh chuoi duoi moi truong hop
                    if(receiveString.equalsIgnoreCase(data))
                    {
                        return  false;
                    }
                }

                inputStream.close();

            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return true;
    }
    //viet du lieu vao file
    public static void writeToFile(String fileName, String data,Context context) {
        try {
            String output = String.format(data,System.getProperty("line.separator"));
            BufferedWriter oFile = new BufferedWriter(new OutputStreamWriter(context.openFileOutput(fileName, Context.MODE_APPEND)));
            PrintWriter out = new PrintWriter(oFile);
            out.println(output);
            out.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    //Doc du lieu tu file
    public static ArrayList<String> readFromFile(String fileName, Context context) {
        ArrayList<String>  returnList = new ArrayList<>();


        try {
            InputStream inputStream = context.openFileInput(fileName);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    returnList.add(receiveString);
                }

                inputStream.close();

            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return returnList;
    }
}
