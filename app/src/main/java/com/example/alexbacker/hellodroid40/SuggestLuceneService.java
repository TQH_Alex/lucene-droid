package com.example.alexbacker.hellodroid40;

import android.util.Log;
import org.apache.lucene.search.spell.NGramDistance;
import org.apache.lucene.search.spell.SpellChecker;
import org.apache.lucene.search.spell.SuggestMode;
import org.apache.lucene.store.FSDirectory;
import org.lukhnos.portmobile.file.Paths;
import java.util.ArrayList;
/**
 * Created by Alex Backer on 12/26/2016.
 */
public class SuggestLuceneService {
    public static SuggestWord suggestLucene(String INDEX_FILE_SPELL,String INDEX_FIELD,String query) throws Exception {
        SearchFileLucene searchLucene = new SearchFileLucene();
        String  suggestWord;
        SpellChecker sc = new SpellChecker(FSDirectory.open(Paths.get(INDEX_FILE_SPELL)));

        String text = query;
        String[] arrText = text.toLowerCase().trim().split("\\s+");
        suggestWord = "";

        // neu 1 tu khong ton tai
        boolean isOneWordDoNotExsit = false;
        sc.getStringDistance();
        // chu finalWord chon bang N-gram
        String finalWord = "";
        //NGramDistance a = new NGramDistance(text.length());
        for ( String ss : arrText) {

            if (sc.exist(ss)) {
                if(arrText.length != 0)
                    suggestWord += ss + " ";

            }
            if( ss.length() <= 2)
            {
                suggestWord = suggestWord  + ss + " " ;
            }
            // neu tu khong ton tai
            if (!sc.exist(ss) && ss.length() > 2)
            {
                isOneWordDoNotExsit = true;
                NGramDistance a = new NGramDistance(ss.length());
                float beforeScore;
                String[] strs = sc.suggestSimilar(ss, 10, null, INDEX_FIELD, SuggestMode.SUGGEST_MORE_POPULAR,0.25f);
                Log.d("similar",ss );
                if (strs != null && strs.length > 0) {
                    float score = 0;
                    beforeScore = a.getDistance(strs[0],ss);
                    for (String word : strs) {
                        Log.d("similar-suggest",word);
                        if(score < a.getDistance(word,ss))
                        {

                            finalWord = word;
                            score = a.getDistance(word,ss);
                        }
                    }
                    // neu diem so tren let nhau khong qua lon thi lay ket qua dau
                    if(Math.abs(beforeScore - score) <= 0.35)
                    {
                        //System.out.print(beforeScore);
                        finalWord = strs[0];
                    }
                }
                // neu tim duoc
                if (strs.length != 0)
                {
                    suggestWord = suggestWord  + finalWord + " ";
                }
                // neu khong tim duoc lay lai tu goc
                if (strs.length == 0)
                {
                    suggestWord = suggestWord  + ss + " " ;
                }
            }

        }
        if(isOneWordDoNotExsit) {
            if (suggestWord != "") {
                ArrayList<Doc> allListDoc;
                allListDoc = searchLucene.Search(suggestWord,MainActivity.indexPath,false);
                if (allListDoc.size() > 5) {
                    return new SuggestWord(true,suggestWord);
                } else {
                    suggestWord = "null";
                    return new SuggestWord(false,suggestWord);
                }
            }
        }
        return new SuggestWord(false,suggestWord);
    }
}
