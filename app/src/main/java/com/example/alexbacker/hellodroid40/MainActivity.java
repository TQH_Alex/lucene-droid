package com.example.alexbacker.hellodroid40;

import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    private String checSearchkNull;
    private SearchView searchView;
    private String mSearchString;
    private static final String SEARCH_KEY = "search";
    // duong dan den index
    public static String indexPath;
    public static String indexPathNoAccent;
    public static String indexPathSpell;



    //menu check dau tieng viet
    MenuItem itemAccent;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // chep du lieu vao root may
        /*----------------------------*/
        // viet tu assets vao data/data
        AssetManager assetMgr = this.getAssets();
        AssetsToData aTD = new AssetsToData();
        // tao folder de chep asset
        // du lieu index co dau
        indexPath = getFilesDir().getAbsolutePath() + File.separator + "index";
        // du lieu index khong dau
        indexPathNoAccent = getFilesDir().getAbsolutePath() + File.separator + "indexNoAccent";
        // di lieu danh cho tu goi y
        indexPathSpell = getFilesDir().getAbsolutePath() + File.separator + "indexSpell";
        // chep du lieu
        AssetsToData.saveFolderToRoot("index",indexPath,assetMgr);
        AssetsToData.saveFolderToRoot("indexNoAccent",indexPathNoAccent,assetMgr);
        AssetsToData.saveFolderToRoot("indexSpell",indexPathSpell,assetMgr);
        // luu ket qua tim kiem khi xoay man hinh
        if (savedInstanceState != null) {
            mSearchString = savedInstanceState.getString(SEARCH_KEY);
        }

    }
    //luu trang thai goc sau khi quay man hinh
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(checSearchkNull != null) {
            mSearchString = searchView.getQuery().toString();
        }
        outState.putString(SEARCH_KEY, mSearchString);
    }

    @Override
    // thay doi ham sau de  truy cap vao vung muon doc du lieu
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main_search, menu);
        MenuItem searchItem = menu.findItem(R.id.search);

        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        checSearchkNull = searchView.getQuery().toString();
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(new ComponentName(this, SearchableActivity.class)));
        searchView.setIconifiedByDefault(true); // true bo dau tim kiem , false hien dau tim kiem
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);
        if (mSearchString != null && !mSearchString.isEmpty()) {
            searchItem.expandActionView();
            searchView.setQuery(mSearchString, false);
           // searchView.clearFocus();
        }
        // luu du lieu checkbox khong dau
        itemAccent = menu.findItem(R.id.check_accent);
        itemAccent.setChecked(false);
        itemAccent.setChecked(getFromSP("checkAccent"));
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case  R.id.search_history :
                //  di toi trang lich su
                Intent historyPage = new Intent(this,ViewHistoryActivity.class);
                startActivity(historyPage);
                //onPause();
                finish();
                break;

            case  R.id.check_accent :
                if (item.isChecked())
                {
                    item.setChecked(false);
                    saveInSp("checkAccent",false);
                }
                else {
                    item.setChecked(true);
                    saveInSp("checkAccent",true);
                }
                break;
            //  default:
            // If we got here, the user's action was not recognized.
            // Invoke the superclass to handle it.
            //   return super.onOptionsItemSelected(item);

        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onQueryTextSubmit(String query) {
        // User pressed the search button
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        // User changed the text
        return false;
    }

    // chuyen qua ViewHistoryActivity
    public void sendMessage(View view) {
        Intent intent = new Intent(this, ViewHistoryActivity.class);
        startActivity(intent);
    }
    private boolean getFromSP(String key){
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("PROJECT_NAME", android.content.Context.MODE_PRIVATE);
        return preferences.getBoolean(key, false);
    }

    private void saveInSp(String key,boolean value){
        SharedPreferences preferences = getApplicationContext().getSharedPreferences("PROJECT_NAME", android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.putBoolean(key, value);
        editor.commit();
    }

}
