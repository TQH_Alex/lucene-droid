url:http://datviettour.com.vn/cong-ty-son-toa-to-chuc-team-building-tai-kdl-bcr-thang-92016-53422.html
Tittle :Du Lịch Kết Hợp Team Building | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Du lịch kết hợp Team Building								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Công ty Sơn Toa tổ chức team building tại KDL BCR tháng 9/2016                    
					Với mong muốn thắt chặt thêm tình đoàn kết và mang đến những khoảnh khắc vui vẻ cho tập thể cán bộ nhân viên, công ty TNHH Sơn Toa Việt Nam đã phối hợp cùng Đất Việt Tour tổ chức chương trình team building “Change For Tomorrow” tại Khu du lịch BCR Quận 9. Đây là dịp để tất cả mọi thành viên có cơ hội gặp gỡ, giao lưu và thoải mái vui chơi bên nhau sau những ngày làm việc căng thẳng, xóa tan mọi khoảng cách giữa các bộ phận phòng ban trong công ty.
Sau khi xe và hướng dẫn viên đón đoàn tại địa điểm hẹn trước ở Bình Dương, mọi người nhanh chóng có mặt và tập trung từ rất sớm tại Khu du lịch BCR. Không khí ngay từ khi mới bắt đầu đã vô cùng phấn khởi và hào hứng bởi một đội ngũ nhân viên năng động trong màu áo hồng đồng phục nổi bật. Nét mặt ai ai cũng rất vui vẻ và đầy mong đợi trước những hoạt động team building được tổ chức dành cho riêng mình.
Theo hiệu lệnh của người quản trò, cả đoàn tập hợp và tự phân chia thành 4 - 5 team. Đồng thời thảo luận chọn ra người thủ lĩnh đảm nhận trọng trách quản lý và định hướng cho các phần thi nhằm mang lại chiến thắng chung cho cả team.
Để thể hiện sự đoàn kết, đồng lòng và cá tính đặc biệt của mỗi đội, Ban tổ chức đã chuẩn bị phần thi trang trí cờ và sáng tạo slogan “vừa vui vừa ý nghĩa”. Đây có lẽ là lúc mọi người được cùng nhau trao đổi và đưa ra những ý kiến, góp phần tạo nên một màu sắc nổi bật riêng cho team của mình. Người đội trưởng phải là người thống nhất ý kiến và trình bày thông điệp đó trước cả một tập thể sao cho thật hài hước và thu hút được nhiều người hưởng ứng.
Tiếp theo là phần khởi động làm nóng cơ thể với một vài động tác nhảy múa theo nhạc rất sôi động. Ban đầu mọi người có phần hơi e dè nhưng ngay sau đó lại hòa mình và “feel the beat” vô cùng máu lửa.
Đương nhiên phần hấp dẫn nhất phải kể đến là các trò chơi chính trong chương trình team building. Nhằm phát huy hết tinh thần đoàn kết và khả năng làm việc nhóm hiệu quả nhưng vẫn đảm bảo yếu tố vui là chính, Ban tổ chức đã mang đến những trò chơi tập thể có mức độ dễ khó khác nhau đòi hỏi các thành viên trong cùng một team phải luôn nỗ lực và cháy hết mình trong từng phần thi đấu.
Thông qua sự thể hiện của tất cả các đội tham gia, có thể dễ dàng nhận thấy rằng đội ngũ nhân viên của công ty Sơn Toa là những “chiến binh tinh nhuệ” và luôn biết cách quan tâm, yêu thương nhau như một đại gia đình thật sự. Thắng thua ngay thời điểm này chẳng còn ý nghĩa gì so với món quà quý báu hơn cả mà họ nhận được. Đó là tình cảm gắn bó giữa các thành viên, được sát cánh cùng nhau chiến đấu vì mục tiêu chung của cả team và niềm tự hào khi mình là một thành viên của Sơn Toa Việt Nam.
Chương trình team building đã kết thúc tốt đẹp và hoàn thành đúng sứ mệnh ban đầu của nó là Change For Tomorrow – Thay đổi vì một tương lai tốt tươi sáng hơn. Sự thay đổi này nằm ở việc tất cả mọi thành viên của gia đình Sơn Toa đã biết tự thân vận động để hòa mình vào sự phát triển chung của công ty. Họ hiểu rõ được tinh thần đoàn kết và khả năng làm việc teamwork là quan trọng như thế nào khi triển khai bất cứ một công việc gì. Chính những điều này đã giúp cho chương trình trở nên đầy thú vị và để lại trong tâm trí mọi người những kỷ niệm thật đáng nhớ.                
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Teambuilding Paintball và Sức mạnh đồng đội
                                Xuyên rừng Cúc Phương đại ngàn
                                Du lịch Teambuilding Đầm Nha Phu
                                Du lịch Teambuilding Văn Minh Miệt Vườn
                                Du lịch Teambuilding Phan Thiết 2 Ngày 1 Đêm
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
