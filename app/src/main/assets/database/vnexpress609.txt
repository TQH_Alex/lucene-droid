url:http://dulich.vnexpress.net/tin-tuc/cong-dong/hoi-dap/page/2.html
Tittle :Hỏi và đáp về du lịch trong nước và quốc tế  - VnExpress Du Lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
		        24h qua
            Du lịch Cộng đồng 
                        Dấu chânTư vấnHỏi - Đáp
                                        Du Lịch 
                                        Hỏi - Đáp 
                                 
                                Việt NamQuốc tếCộng đồngẢnh
                            Nên đi phương tiện gì qua đèo Prenn để hạn chế tai nạn
                                                             
                                Gia đình tôi chuẩn bị đi nghỉ mát ở Đà Lạt, xuất phát tại TP HCM và phải đi qua đèo Prenn nên khá lo sợ.                            
                            Năm nay nên đi biển nào để an toàn?
                                                             
                                Sau vụ cá chết hàng loạt ở các bãi biển trong nước do ảnh hưởng từ ô nhiễm môi trường, gia đình chúng tôi rất phân vân không biết năm nay nên đi tắm biển ở đâu thì sạch sẽ và an toàn.                            
                            Hành lý du lịch tiết lộ điều gì về bạn
                                                             
                                Khi đi du lịch, mỗi người lại chọn cho mình một loại vali, túi du lịch khác nhau, ẩn chứa nhiều bí mật thú vị về tính cách của bạn.                            
                            Có bao nhiêu chuyến bay qua Bắc Đại Tây Dương mỗi ngày
                                                             
                                Một video mô phỏng các chuyến bay trong ngày 6/5 đã cho thấy phần nào công việc căng thẳng của những nhân viên kiểm soát không lưu nhằm tránh va chạm.                            
                            Thử tài nhìn món ăn, đoán địa danh
                                                             
                                Hãy thử tài nhìn bức ảnh check in cùng đồ ăn để đoán xem tác giả đang ở địa danh nào.                            
                            Chất lỏng tối đa được xách tay lên máy bay là 100 ml hay một lít?
                                                             
                                Bạn có biết mình được phép mang tối đa bao nhiêu ml chất lỏng trong hành lý xách tay không?                            
                            Đi du lịch tuần trăng mật có cần hướng dẫn viên
                                                             
                                Tôi mới cưới vợ và dự kiến đi tuần trăng mật ở nước ngoài. Tuy nhiên, tôi thấy nói tour du lịch nước ngoài bắt buộc phải có hướng dẫn viên nên băn khoăn không biết thế nào. Chúng tôi chỉ muốn có 2 người thôi. Nhờ các bạn giải đáp giúp.                            
                            Cách cầm đũa thể hiện tính cách của bạn
                                                             
                                Người cầm như cầm bút có tính tỉ mỉ cao, người kẹp đũa chắc chắn lại là người đầy nhiệt huyết.                            
                            Đoán biệt danh của các thành phố nổi tiếng
                                                             
                                Bạn có biết biệt danh Tiểu Paris hay Thành phố Vàng là của địa danh nào?                            
                            Trắc nghiệm bạn có nhầm thủ đô các nước
                                                             
                                Hãy thử xem bạn đoán đúng bao nhiêu tên thủ đô các nước.                            
                            Đoán tên nước qua hình dáng lãnh thổ đặc biệt
                                                             
                                Bạn có biết quốc gia nào có lãnh thổ giống hình kền kền và cây cọ không?                            
                            Đoán địa danh qua món ăn đường phố
                                                             
                                Đôi khi chỉ cần nhắc tên món ăn với hương vị khó quên, du khách có thể nhớ ngay đến một miền đất mình từng đặt chân qua.                            
                            Đoán tên sân bay qua mã ký hiệu 3 chữ cái
                                                             
                                Thử tài hiểu biết của bạn bằng việc đoán chính xác tên các sân bay trên thế giới qua 3 chữ cái ký hiệu.                            
                            Đoán tên nước qua quốc kỳ
                                                             
                                Hãy thử xem bạn biết được quốc kỳ của bao nhiêu nước dưới đây.                            
                            Thủ tục xin visa Nhật Bản thế nào?
                                                             
                                Tôi rất muốn ngắm hoa anh đào ở Nhật Bản vào tháng 4. Xin tư vấn cho tôi thủ tục xin visa và thời gian chờ đợi được cấp trong vòng bao lâu. Xin cảm ơn.                            
                            Cần thủ tục gì để tham quan khi đang quá cảnh tại Hàn Quốc?
                                                             
                                Chuyến bay của tôi quá cảnh ở sân bay Hàn Quốc. Tôi muốn tranh thủ thời gian này để tham quan thì phải chuẩn bị những thủ tục gì?                            
                            Đi du lịch Lào theo tour hay tự túc?
                                                             
                                Nhóm chúng tôi gồm 4 người và muốn đi du lịch Lào trong vòng một tuần. Xin tư vấn chúng tôi nên đi tour hay tự túc, nếu tự túc cần lưu ý những gì, nên đi những tỉnh nào có biển?                            
                            Đi du lịch Phan Thiết vào thời điểm nào đẹp?
                                                             
                                Tôi và gia đình có ý định du lịch Phan Thiết. Xin các bạn tư vấn nên đi vào thời điểm nào, những địa điểm tham quan và các nhà hàng địa phương nổi tiếng. Xin cảm ơn.                            
                            Đi lễ bà chúa Xứ vào thời điểm nào?
                                                             
                                Tôi muốn đi lễ bà chúa Xứ ở An Giang nhưng không biết thời điểm nào diễn ra lễ hội. Xin các bạn tư vấn cho tôi cách đi từ TP HCM. Ngoài ra ở An Giang có những địa điểm du lịch nổi tiếng, hấp dẫn nào?                            
                            Cuối tháng 2 đi Mộc Châu còn hoa mận không?
                                                             
                                Cuối tháng 2 tôi có chuyến công tác ở Mộc Châu (Sơn La). Xin hỏi thời gian đó có còn hoa mận không, mong các bạn tư vấn cho tôi những địa điểm có nhiều hoa để chụp hình.                            
                            Tết du xuân ở đâu gần Hà Nội?
                                                             
                                Xin các bạn tư vấn giúp những điểm du lịch ở gần Hà Nội để có thể vui chơi, ăn uống, đi trong ngày. Tôi muốn tìm chỗ để cho trẻ con từ 3-6 tuổi có thể vui chơi. Xin cảm ơn.                            
                            Nên ăn ở quán nào ở Phan Thiết dịp Tết?
                                                             
                                Gia đình tôi năm nay đi du lịch Tết ở Phan Thiết. Xin các bạn tư vấn những quán ăn được nhiều người ưa thích ở thành phố này, những món nào là đặc sản của Phan Thiết? Xin cảm ơn.                            
                            Xin tư vấn những điểm homestay ở Hà Giang?
                                                             
                                Ra Tết, nhóm chúng tôi lần đầu rủ nhau lên Hà Giang để ngắm hoa đào. Xin các bạn tư vấn những homestay để có thể ăn ở và tìm hiểu về cuộc sống của người dân bản địa.                            
                            Xin tư vấn những quán ngon ở Vũng Tàu?
                                                             
                                Cuối tuần này tôi và bạn gái sẽ chạy xe từ TP HCM xuống Vũng Tàu. Xin các bạn tư vấn những điểm ăn uống và những món ăn nổi tiếng ở nơi đây.                            
                            Xin chia sẻ kinh nghiệm đi du lịchTết mà vẫn tiết kiệm?
                                                             
                                Nhóm chúng tôi gồm 4 người, mới đi làm được một năm và đang dự định đi du lịch Tết Xin các bạn tư vấn làm thế nào để đi du lịch ở Campuchia mà vẫn tiết kiệm. Xin tư vấn cho chúng tôi nơi ăn, chỗ ở hợp lý.                            
                            Xin tư vấn những nơi đi lễ chùa dịp Tết?
                                                             
                                Xin các bạn tư vấn những điểm du lịch tâm linh gần Hà Nội để vừa có thể đi lễ đầu năm, vừa có thể du xuân, vãn cảnh.                            
                            Mua đồ ăn về khách sạn có bị tính phí không?
                                                             
                                Vợ chồng chúng tôi đi du lịch nhưng không ăn ở resort, khách sạn mà mua đồ ăn từ bên ngoài vào. Tôi muốn hỏi việc mua đồ ăn này vào có bị tính thêm phí dịch vụ dọn dẹp hay không? Xin cảm ơn.                            
                            Làm thế nào để không bị 'chặt chém' khi du lịch dịp Tết?
                                                             
                                Tôi đọc trên các phương tiện truyền thông thấy nhiều khách du lịch bị hét giá cao, tính giá đắt khi vào nhà hàng, quán ăn ở các nơi. Xin hỏi làm cách nào để tránh được tình trạng này. Xin cảm ơn.                            
                            Muốn ngắm hoa tử đằng ở Nhật, đi vào tháng mấy?
                                                             
                                Tôi nghe nói Nhật Bản có mùa hoa tử đằng rất đẹp và muốn đi du lịch. Xin hỏi hoa tử đằng rộ nhất vào tháng mấy. Tôi nên đi theo lịch trình như thế nào? Xin cảm ơn các anh chị đã tư vấn giúp.                            
                         12345                     
                    Xem tiếp
         Gửi câu hỏi về tòa soạn
        Khuyến mãi Du lịch
		  prev
		   next
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
        Việt Nam
         
                Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                        Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịchViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnKhông gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
        Video
         
            Du khách đứng tim vì voi chặn đầu xe
            Đoàn du khách được một phen hú hồn vì gặp đàn voi khi thăm một khu bảo tồn thiên nhiên tại châu Phi.
                Du khách la hét vì chơi tàu lượn dốc nhất thế giới
                Cả công viên 'nín thở' chờ du khách béo trượt máng
         
             
        Dọc ngang đất nước
                        Quán bún măng vịt mỗi ngày chỉ bán một tiếng
            Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịchViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnKhông gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
        Được hợp tác bởi:
        Xem nhiều nhất
    'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹpGóc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban NhaCặp du khách Trung Quốc quan hệ tình dục trong công viênCả công viên 'nín thở' chờ du khách béo trượt mángBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonChàng tiên cá có thật giữa đời thường
        Hỏi đáp
                            Bạn có nhận ra các điểm check-in của Sao Việt?                            
                                                             
                            7 câu đố bằng thơ ai mê du lịch Việt cũng đoán ra                            
                                                             
                            Những câu hỏi về bản đồ chứng tỏ bạn là phượt thủ thứ thiệt                            
                                                             
                            8 câu hỏi đúng - sai tiết lộ bạn có phải du khách thông minh                            
                                                             
		 
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
