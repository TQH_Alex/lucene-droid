url:http://dulich.vnexpress.net/tin-tuc/cong-dong/tu-van/mai-chi-lac-buoc-o-phuong-hoang-co-tran-3488774.html?utm_campaign=boxtracking&utm_medium=box_topic&utm_source=detail
Tittle :Mai Chi lạc bước ở Phượng Hoàng cổ trấn - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
		        24h qua
            Du lịch Cộng đồng 
                        Dấu chânTư vấnHỏi - Đáp
                                        Du Lịch 
                                        Tư vấn 
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Thứ năm, 27/10/2016 | 14:05 GMT+7
|
						Mai Chi lạc bước ở Phượng Hoàng cổ trấn												
					Không chỉ tư vấn cách đi, cô nàng Lala trong phim "Bộ tứ 10A8" còn ghi lại hành trình 5 ngày bằng nhật ký sinh động và lôi cuốn.
					9 cổ trấn Trung Quốc đẹp như tranh  /  Vẻ quyến rũ của Phượng Hoàng cổ trấn                    
					Phượng Hoàng cổ trấn (Hồ Nam, Trung Quốc) là một thị trấn cổ xinh đẹp, được lựa chọn làm bối cảnh cho nhiều bộ phim điện ảnh nổi tiếng. Năm nay, cổ trấn nghìn năm tuổi trở thành điểm đến được nhiều người Việt quan tâm, dù quá trình di chuyển khá vất vả.
					Có nhiều bài viết chia sẻ cảm nhận và kinh nghiệm đến Phượng Hoàng để bạn tìm hiểu trước khi bắt đầu hành trình. Nhưng MC Mai Chi, người từng nổi danh qua vai diễn cô nàng Lala xinh xắn trong bộ phim sitcom "Bộ tứ 10A8" chọn cho mình một cách khác để ghi lại hành trình khám phá cổ trấn.
					Ngoài những thông tin cơ bản về cách di chuyển, các bước chuẩn bị, đặt phòng khách sạn, món nên ăn, Mai Chi dành thời gian viết lại những chuyện thú vị suốt 5 ngày trong chuyến đi của mình. Cách kể chuyện đầy cảm xúc của cô nàng khiến bài chia sẻ nhận được hàng nghìn lượt like và bình luận. 
					Phượng Hoàng quyến rũ khách du lịch bởi vẻ cổ kính, đầy nghệ thuật, dịch vụ du lịch cũng rất sẵn, tiện lợi. Nàng MC mê mẩn khi được lang thang khắp các con hẻm, dốc nhỏ trong cổ trấn. "Ở Phượng Hoàng cổ trấn, thích nhất là được đi dạo và len lỏi vào các con ngõ nhỏ, ngắm nghía những đôi giày vải, ăn món đường phố lạ và chụp ảnh", cô viết.
					Có 3 cách để tới Phượng Hoàng cổ trấn: đi máy bay 2 chặng, đi tàu hỏa 2 chặng hoặc kết hợp giữa ôtô và tàu hỏa. Nàng MC và người bạn đồng hành đã chọn cách cuối cùng, vừa không mất quá nhiều thời gian như cách đi tàu hỏa, vừa không tốn nhiều chi phí như máy bay.
					Do không chuẩn bị kịp áo lạnh, Mai Chi đã mua tạm một chiếc áo khoác, họa tiết lạ mắt tại cổ trấn nhưng lại rất ưng ý. Không chỉ bán những sản phẩm lưu niệm, Phượng Hoàng cổ trấn còn có nhiều cửa tiệm thời trang mang phong cách riêng.
					Cảnh sắc mơ màng, cổ kính, đẹp tựa tranh vẽ ở cổ trấn nghìn năm tuổi khiến nàng Lala mê đắm.
					Ban đầu, Mai Chi và bạn đồng hành định chơi một ngày ở Trương Gia Giới sau đó mới di chuyển về Phượng Hoàng. Nhưng khi đến nơi mà không tìm ra khách sạn, sẵn "máu" muốn đến ngay cổ trấn trong truyền thuyết, Mai Chi đã quyết định thay đổi vào phút chót.
					Mỗi góc ở cổ trấn đều mang vẻ đẹp riêng, thậm chí mỗi thời điểm: sáng, chiều, bất kể khi mưa hay nắng thì du khách cũng có thể tìm cho mình những khung hình độc đáo.
					"Đặc sản" ở thị trấn Phượng Hoàng chính là những cây cầu đá cổ, như cầu Vũ, cầu Phong, cầu Hồng Kiều...
					Tổng chi phí chuyến hành trình của Mai Chi khoảng 10 triệu đồng mỗi người. Cô nàng cũng tư vấn rất kỹ càng về các món ăn ở Phượng Hoàng. Sẵn tâm hồn ăn uống, Mai Chi quyết tâm thử tất cả các món ngon ở nơi mình đi qua.
					Ngày cuối cùng, nàng Lala quay trở lại Trương Gia Giới để khám phá núi Thiên Môn Sơn, nơi mà cô phải thốt lên: "Kinh khủng vì quá vĩ đại, tôi thắc mắc sao con người có thể làm được như vậy nhỉ? Đường cáp dài và dốc. Cảm giác gần như thẳng đứng. Cáp bám trên những sợi dây chắc khoẻ kéo chúng tôi - những con người háo hức lên thẳng đỉnh dãy núi kỳ vĩ kia... Sương mù làm những ngọn núi ẩn hiện mơ hồ, đẹp và ấn tượng".
	>> Đọc nhật ký hành trình đầy cảm xúc của Mai Chi
	>> Xem tiếp hình ảnh đẹp trong chuyến đi
	Theo Ngôi sao                        
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
                Theo Sao đi du lịch
                    Vẻ đẹp của thị trấn nơi William và Kate gặp nhau (17/11)
                                             
                    Bạn có nhận ra các điểm check-in của Sao Việt? (6/11)
                                             
                    Vật bất ly thân của sao Hollywood trên đường du lịch (12/10)
                                             
                    Bên trong khách sạn Kim 'siêu vòng 3' bị cướp (4/10)
                                             
                    Mariah Carey du lịch Hy Lạp cùng người tình tỷ phú (25/9)
                                             
                Xem thêm
            Xem nhiều nhất
                            'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                                                             
                                Góc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban Nha
                                                                     
                                Cặp du khách Trung Quốc quan hệ tình dục trong công viên
                                                                     
                                Cả công viên 'nín thở' chờ du khách béo trượt máng
                                                                     
                                Bí mật đáng sợ bên dưới hệ thống tàu điện ngầm London
                                                                     
                 
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                Tags
                                    Phượng Hoàng cổ trấn
                                Kinh nghiệm du lịch
                                Mai Chi
                                Mai Chi Bộ tứ 10A8
    Tin Khác
                            5 vùng đất Samurai nổi tiếng của Nhật Bản
                                                             
                            Khu cắm trại giữa rừng dương cách Sài Gòn 3 giờ chạy xe
                                                             
                            5 tuyệt chiêu hẹn hò với người lạ trên đường du lịch
                                                             
                                                     
                            Quang Vinh khám phá hai mặt đối lập của Sài Gòn
                                                             
                                    Khách Tây truyền nhau bí kíp mua bia ở Việt Nam
                                                                             
                                    16 điều bạn nên tránh khi du lịch Nhật Bản
                                                                             
                                    Chàng trai Sài Gòn du lịch tự túc Đài Loan với 15 triệu đồng
                                                                             
                                    6 món ngon Quy Nhơn níu chân du khách
                                                                             
                                    Điều cần biết khi tắm khỏa thân ở Nhật Bản
                                                                             
                                    Khách sạn mini - xu hướng mới ở Phú Quốc
                                                                             
                                    5 món nướng cho ngày mưa Đà Lạt
                                                                             
                                    Những nơi được check in khi Hà Nội đón gió lạnh đầu mùa
                                                                             
                                    3 loài hoa dại khiến phượt thủ lên Đà Lạt tháng 11
                                                                             
                                    Bỏ qua bún chửi, Hà Nội còn nhiều món bún ngon nên thử
                                                                             
        Khuyến mãi Du lịch
		  prev
		   next
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
        Việt Nam
         
                Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                        Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịchViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnKhông gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
         Gửi bài viết về tòa soạn
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
        Ảnh
         
                             
            Quang Vinh khám phá hai mặt đối lập của Sài Gòn
            Sinh ra và lớn lên ở Sài Gòn, Quang Vinh muốn giới thiệu những điểm thú vị mà mọi người có thể đã bỏ qua với loại hình du lịch mới - staycation.
                                     
                Khách sạn mini - xu hướng mới ở Phú Quốc
                                     
                Homestay nằm giữa hồ sen ở Ninh Bình
        Video
         
            Du khách đứng tim vì voi chặn đầu xe
            Đoàn du khách được một phen hú hồn vì gặp đàn voi khi thăm một khu bảo tồn thiên nhiên tại châu Phi.
                Du khách la hét vì chơi tàu lượn dốc nhất thế giới
                Cả công viên 'nín thở' chờ du khách béo trượt máng
        Bình luận hay
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
