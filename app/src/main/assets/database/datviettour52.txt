url:http://datviettour.com.vn/tour-tet-duong-lich-2016-nha-trang-hon-mot-vinpearl-land-38603.html
Tittle :Danh Sách Lịch Khởi Hành | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Danh sách Lịch khởi hành								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Tour du lịch Tết 2017: Nha Trang – Hòn Một – Vinpearl Land                    
Tận hưởng kỳ nghỉ tết 2017 tuyệt vời tại thành phố biển Nha Trang xinh đẹp, lặn biển ngắm san hô, khám phá thiên đường vui chơi giải trí cao cấp Vinpearl Land. Còn chần chờ gì mà không nắm lấy cơ hội với Tour du lịch Tết 2017  Nha Trang 3 ngày 3 đêm với giá tiết kiệm!
TỐI NGÀY 1: TP HCM – BÌNH THUẬN – KHÁNH HÒA
20h30: Xe và Hướng dẫn viên của Du lịch Đất Việt đón quý khách tại điểm hẹn khởi hành đi Nha Trang.
Quý khách nghỉ ngơi trên xe.
NGÀY 2: BIỂN BÃI DÀI – CHÙA LONG SƠN – NHÀ YẾN – THÁP BÀ – TẮM BÙN
Sáng: Đoàn đến Cam Ranh, dừng chân vệ sinh cá nhân và dùng bữa sáng. Tiếp chương trình Quý khách cùng tắm biển Bãi Dài, nhận ghế lều cùng sinh hoạt và tắm biển tự do, thưởng thức hải sản tươi sống, tham gia các môn thể thao trên biển như: Chèo thuyền Kayak, Mô tô nước, dù kéo…
Trưa: Quý khách đến với thành phố biển Nha Trang, dùng bữa trưa tại nhà hàng.
Sau đó đến khách sạn nhận phòng và nghỉ ngơi.
Chiều: Xe đưa đoàn tham quan trung tâm thành phố.
Chùa Long Sơn: Qúy khách chiêm ngưỡng tượng Ông Phật Trắng, cầu gia đình an lành, hạnh phúc.Nhà nuôi yến Nha Trang: Qúy khách có dịp tìm hiểu về quá trình nuôi và chế biến các sản phẩm được làm từ tổ chim yến.Tháp Bà Ponagar: Tham quan, tìm hiểu về kiến trúc xây dựng, văn hóa, phong tục tính ngưỡng thờ cúng của dân tộc Chăm.Tắm Bùn Khoáng Nóng: Qúy khách trải nghiệm tắm bùn khoáng, với lớp bùn tự nhiên chứa nhiều chất dinh dưỡng và nước khoáng nóng có tác dụng phục hồi sức khỏe rất nhanh, rất tốt cho sức khỏe (chi phí tự túc ).
Tối: Xe đưa Quý khách đến nhà hàng dùng cơm chiều. Quý khách nghỉ ngơi tự do.
NGÀY 3: HÒN MỘT – VỊNH SAN HÔ – VINPEARL LAND
Sáng: Đoàn dùng điểm tâm sáng Buffet, bắt đầu khám phá cuộc sống và văn hóa của ngư dân vịnh biển Nha Trang.
Hòn Một – ngắm san hô: Với hệ sinh thái thực vật phong phú cùng những rạn san hô với nhiều màu sắc đã tạo nên một điểm tham quan khám phá khá lý thú cho du khách.Làng Chài: Quý khách cùng tìm hiểu về đời sống, cách nuôi tôm, cá của ngư dân miền biển.Vịnh San Hô: Với những bãi tắm tuyệt đẹp, tự do tắm biển và thưởng thức hải sản tại đây.
Trưa: Quý khách dùng bữa trưa tại nhà hàng trên vịnh san hô, về lại đất liền.
Chiều: Xe đưa đoàn khám phá khu vui chơi giải trí đạt tiêu chuẩn 5 sao Vinpearl Land, với hệ thống cáp treo vượt biển dài nhất châu Á, cùng khu vui chơi giải trí trong nhà như: Xem phim 4D, xe điện đụng, triển lãm tranh nghệ thuật…kết hợp với khu vui chơi ngoài trời như: Tàu hải tặc, cói xoay gió, vòng đu quay, thủy cung…
Ngoài ra, Quý khách có thể tắm biển với bãi biển nhân tạo, hồ bơi trẻ em đẹp nhất Nha Trang (chi phí tự túc 650.000 vnđ/vé).
20h00: Quý khách về lại đất liền đến nhà hàng dùng đặc sản nem nướng thay cho bữa tối.
Quý khách tự do nghỉ ngơi hoặc thưởng thức kem bốn mùa.
NGÀY 4: CHỢ ĐẦM – CHIA TAY NHA TRANG
Sáng: Quý khách dùng điểm tâm sáng buffet. Làm thủ tục trả phòng, xe đưa đoàn đến chợ Đầm tham quan và mua sắm.
Đoàn khởi hành về lại TP HCM, trên đường về Quý khách dừng chân mua sắm đặc sản địa phương làm quà tặng cho người thân.
Trưa: Quý khách dùng bữa trưa tại Cà Ná.
Chiều: Về đến TP HCM, chia tay và hẹn gặp lại Quý khách.
Có gì hấp dẫn?
- Khởi hành:
Tối 30/12/2016 (Tết Dương lịch)
Tối 28, 29, 31/01/2017 (Tối Mùng 1, 2, 4 Tết Âm lịch)
- Tư vấn & giao vé miễn phí
- Cam kết dịch vụ chất lượng
- Quà tặng: Nón du lịch, gối hơi, bao lì xì may mắn
Hotline: 0909 897 499 - 0966 980 945
BẢNG GIÁ TOUR
Lượng kháchKhách sạn
2 saoKhách sạn
3 saoKhách sạn
4 saoNgày khởi hànhKhách Lẻ - Ghép Đoàn
(02 - 08 khách)2.400.0002.790.0003.150.000Tối 30/12/2016
(Tết Dương lịch)2.590.0002.990.0003.350.000Tối 28, 29, 31/01/2017 (Tối Mùng 1, 2, 4
Tết)Khách Đoàn - Tour Riêng
(trên 40 khách)Mọi ngày
Theo yêu cầu
Lưu ý:
Giá tour trẻ em từ 05 -11 tuổi:
Khách sạn 2 sao: 1.295.000 vnđ/kháchKhách sạn 3 sao: 1.495.000 vnđ/kháchKhách sạn 4 sao: 1.675.000 vnđ/khách
Phụ thu phòng đơn
Khách sạn 2 sao: 500.000 vnđ/kháchKhách sạn 3 sao: 1.000.000 vnđ/kháchKhách sạn 4 sao: 1.400.000 vnđ/khách
GIÁ TOUR BAO GỒM
Phương tiện: Xe du lịch 16, 29, 35, 45 chỗ, đạt chuẩn du lịch phục vụ đưa đón đoàn suốt tuyến tham quan.Khách sạn:
Khách sạn 2 sao: Sài Gòn, Contempo,…
Khách sạn 3 sao: Thelight 2, Olympic, Thăng Long, Nhật Thành,…
Khách sạn 4 sao: Mường Thanh, Dendro Gold, Green World,…
Tiêu chuẩn 2,3,4 khách/phòng (Trường hợp 3 khách vì lý do giới tính). Tiện nghi: máy lạnh, tivi, nước nóng, vệ sinh…Ăn uống:
- Ăn sáng ngày 01: Bánh canh, hủ tiếu,...
- Ăn sáng ngày 02, 03: Buffet
- Ăn trưa, chiều: Nhà hàng địa phương tiêu chuẩn, hợp vệ sinh.Hướng dẫn viên: Đoàn có hướng dẫn viên thuyết minh và phục vụ ăn, nghỉ, tham quan cho quý khách. Hoạt náo viên tổ chức trò chơi vận động tập thể, sinh hoạt, ca hát.Bảo hiểm: Khách được bảo hiểm du lịch trọn gói, mức bồi thường tối đa 20.000.000 đồng/vụ. Thuốc y tế thông thường.Quà tặng:
- Nón du lịch Đất Việt
- Khăn ướt + nước đóng chai
- Gối hơi + bao lì xì may mắnTham quan: Giá vé đã bao gồm phí vào cổng tại các điểm tham quan theo chương trình..
GIÁ TOUR KHÔNG BAO GỒM
Không bao gồm phí tại khu du lịch Vinpearl Land.Các chi phí cá nhân khác ngoài chương trình (Giặt ủi, điện thoại, thức uống trong minibar,...).Tiền Típ cho hướng dẫn viên và tài xế (nếu có).
*ĐIỀU KIỆN HỦY TOUR: (Không tính thứ bảy, chủ nhật và ngày lễ)
Nếu hủy tour, Quý khách thanh toán các khoản lệ phí hủy tour sau:
Trước ngày đi 20 Ngày: 30% giá tourTrước ngày đi 10 Ngày: 50% giá tourTrước ngày đi 3-7 ngày: 70% giá tourTrước ngày đi 0-3 Ngày : 100% giá tour
Trường hợp quý khách có yếu tố khách quan về cá nhân vui lòng báo sớm trước 5 ngày không tính thứ 7, chủ nhật & ngày lễ, để công ty hỗ trợ giải quyết .
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Tour du lịch Tết 2017: Sihanouk Ville – Bokor – Koh Rong
                                Tour du lịch Tết 2017: Mỹ Tho – Cần Thơ – Châu Đốc – Hà Tiên
                                Tour du lịch Tết 2017: Hong Kong – Freeday 4 ngày 3 đêm
                                Tour du lịch Tết 2017: Đà Lạt ngàn thông 3 ngày 3 đêm
                                Tour du lịch Tết 2017: Nha Trang – Hòn Một – Vinpearl Land
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
