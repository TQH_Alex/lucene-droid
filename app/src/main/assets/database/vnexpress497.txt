url:http://dulich.vnexpress.net/tin-tuc/cong-dong/dau-chan/an-tuong-lan-dau-di-tau-cao-toc-vien-dan-o-nhat-ban-3497269.html
Tittle :Ấn tượng lần đầu đi tàu cao tốc 'viên đạn' ở Nhật Bản - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
		        24h qua
            Du lịch Cộng đồng 
                        Dấu chânTư vấnHỏi - Đáp
                                        Du Lịch 
                                        Dấu chân 
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Thứ bảy, 12/11/2016 | 15:03 GMT+7
|
						Ấn tượng lần đầu đi tàu cao tốc 'viên đạn' ở Nhật Bản												
					Chỗ duỗi chân rộng rãi, góc ngả ghế lớn, vận tốc trung bình hơn 280 km/h nhưng ít rung lắc là những ấn tượng mạnh nhất của tôi khi đi tàu cao tốc Shinkansen, niềm tự hào công nghệ của người Nhật trong nửa thế kỷ.
					Trưởng tàu Nhật Bản vô tình khiếm nhã khi xin lỗi hành khách  /  Những 'thiên thần' trên tàu cao tốc Shinkansen                    
			Lần đầu công tác Nhật hồi tháng 10, lên các chuyến tàu điện ngầm ở thủ đô Tokyo, tôi không khỏi ngưỡng mộ sự hiện đại, tiện lợi của phương tiện này. "Nhưng thế này vẫn còn ồn và lắc lắm, cậu phải đi Shikansen mới thấy mê, ngồi chưa ấm chỗ đã tới nơi rồi”, Doãn Hoài Nam, 27 tuổi, một người bạn đã học và làm việc tại Nhật nhiều năm, nói.
	Tôi có cơ hội lên hai chuyến Shinkansen đi và về giữa thành phố Tokyo và tỉnh Shizuoka, nơi có núi Phú Sĩ nổi tiếng. Hai địa điểm cách nhau 167 km được kết nối bằng tuyến tàu Tokaido Shikansen, tuyến lâu đời nhất (1964) và cũng thông dụng nhất.
	Với khoảng cách này, trong khi tàu thông thường di chuyển khoảng ba giờ mới tới thì tàu Shinkansen giúp rút ngắn thời gian xuống chỉ còn một giờ, nhờ vận tốc trung bình 285 km/h.
	Trong tiếng Nhật, Shinkansen nghĩa là "đường tàu mới", ngoài ra phương tiện còn được gọi là "bullet train" (tàu viên đạn), do hình dạng thuôn gọn của đầu tàu, cùng tốc độ cao được ví như viên đạn bay khỏi nòng súng. Tốc độ cao là vậy, nhưng trong lịch sử nửa thế kỷ của tàu Shinkansen, chưa có tai nạn chết người nào do va chạm.
	Ông Hideo Shima, cha đẻ của Shinkansen, từng mong muốn thiết kế một loại tàu "đem lại cảm giác như máy bay". Và thực tế, tàu Shinkansen có nhiều điểm tương đồng, thậm chí thoải mái hơn máy bay nhờ không gian rộng rãi, tiếng ồn và độ rung lắc thấp. 
	Từng chi tiết nhỏ nhất trên tàu cũng có thể cho thấy sự tỉ mỉ của người Nhật, dù có thể người mới đi lần đầu ít để ý: từ chiếc móc áo chìm trên thành khoang, ổ cắm điện, cửa tự động, vòi nước tự động, tới chiếc ghế thoải mái nhất tôi từng ngồi. Tàu di chuyển nhanh tới mức mưa rơi chảy thành hàng ngang song song mặt đất, như lúc máy bay cất cánh, nhưng đồng thời, nước trong chai trên bàn hầu như không dao động.
	Một điều thú vị nữa là đường tàu nằm rất sát nhà dân, trong khi người Nhật đặc biệt tôn trọng sự yên tĩnh và tính riêng tư. Trong quá khứ, tàu Shinkansen từng gây ô nhiễm âm thanh, dẫn đến một số cuộc biểu tình ở đất nước có mật độ dân cư cao. Do vậy, công nghệ được cải tiến để tiếng ồn của tàu thấp hơn 70 dB tại khu dân cư (nhỏ hơn tiếng máy hút bụi). 
	Riêng trên tuyến Tokaido, có ba loại tàu Shinkansen, từ nhanh đến chậm. Tàu Nozomi (Hy vọng) là tàu nhanh nhất, sau đó tới Hikari (Tia sáng) và cuối cùng là Kodama (Tiếng vọng). Loại nhanh nhất (300 km/h) chỉ dừng tại những ga lớn trong khi loại chậm nhất (285 km/h) dừng tại tất cả ga dọc đường.
	Cũng không thể không nhắc tới tác phong làm việc chuyên nghiệp của nhân viên phục vụ toa tàu, từ những người dọn dẹp nhanh thoăn thoắt, xong hết mọi việc chỉ trong 7 phút khi tàu đỗ tại ga, tới nhân viên trên tàu, mỗi lần bước tới cửa khoang là quay người 180 độ, cúi chào và mỉm cười với khách. 
	Chất lượng đi cùng với giá thành. Vé tàu cao tốc Shinkansen có giá cao gấp hai đến ba lần giá vé tàu thường. Tuy nhiên, cách khách du lịch nước ngoài tiết kiệm chi phí là mua vé Japan Rail Pass, theo loại thời hạn 7, 14 và 21 ngày, giúp đi lại khắp nước Nhật bằng các loại tàu do Japan Railway Group vận hành. Một tấm vé mua trọn gói hành trình 7 ngày có giá khoảng 6 triệu đồng dành cho khoang thường.
	Japan Rail Pass chỉ được bán bên ngoài Nhật, vì vậy, bạn có thể đến các công ty du lịch ở Việt Nam để mua vé. Còn nếu muốn mua theo từng chuyến tại Nhật, bạn có thể đến các cửa hàng bán vé, voucher (kinken shop) hoặc đến ga tàu.
	Xem thêm: Sự thật thú vị về tàu cao tốc Nhật Bản
	Trọng Giáp                        
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
            Xem nhiều nhất
                            'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                                                             
                                Góc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban Nha
                                                                     
                                Cặp du khách Trung Quốc quan hệ tình dục trong công viên
                                                                     
                                Cả công viên 'nín thở' chờ du khách béo trượt máng
                                                                     
                                Bí mật đáng sợ bên dưới hệ thống tàu điện ngầm London
                                                                     
                 
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                Tags
                                    Shinkansen
                                Tàu cao tốc
                                Nhật Bản
                                Du lịch
                                Tài điện ngầm
                                Japan Rail Pass
    Tin Khác
                                                     
                            Diễm My ăn trưa, trồng rau cùng người dân Hội An
                                                             
                            Du khách đứng tim vì voi chặn đầu xe
                                                             
                                                     
                            Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                                             
                            Kỷ niệm hài hước với toilet trên đường du lịch
                                                             
                                    Hành khách phát hiện gián trong bữa ăn chay trên máy bay
                                                                             
                                    Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
                                                                             
                                    Du khách mỏi tay câu cá khổng lồ tại Thái Lan
                                                                             
                                    Cả công viên 'nín thở' chờ du khách béo trượt máng
                                                                             
                                    Cặp du khách Trung Quốc quan hệ tình dục trong công viên
                                                                             
                                    Quang Vinh khám phá hai mặt đối lập của Sài Gòn
                                                                             
                                    Khách Tây nói người Việt bấm còi xe như chơi nhạc
                                                                             
                                    Giây phút kinh hoàng của chàng trai thoát chết trong lễ hội 'bò đuổi'
                                                                             
                                    Cách tiêu tiền của khách Trung Quốc và sự khác biệt về đẳng cấp
                                                                             
                                    Hòn đảo chỉ phục vụ khách VIP ở Philippines
                                                                             
        Khuyến mãi Du lịch
		  prev
		   next
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
        Việt Nam
         
                Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                        Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịchViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnKhông gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
         Gửi bài viết về tòa soạn
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
        Ảnh
         
                             
            Diễm My ăn trưa, trồng rau cùng người dân Hội An
            Diễn viên Diễm My đã có dịp dùng bữa cùng một cặp vợ chồng ở làng Trà Quế, Hội An, trước khi thực hiện bộ ảnh "Vẻ đẹp không tuổi" cùng nhiếp ảnh gia Pháp.
                                     
                Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                     
                Quang Vinh khám phá hai mặt đối lập của Sài Gòn
        Video
         
            Du khách đứng tim vì voi chặn đầu xe
            Đoàn du khách được một phen hú hồn vì gặp đàn voi khi thăm một khu bảo tồn thiên nhiên tại châu Phi.
                Du khách la hét vì chơi tàu lượn dốc nhất thế giới
                Cả công viên 'nín thở' chờ du khách béo trượt máng
        Bình luận hay
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
