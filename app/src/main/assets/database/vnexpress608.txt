url:http://dulich.vnexpress.net/tin-tuc/cong-dong/hoi-dap/page/5.html
Tittle :Hỏi và đáp về du lịch trong nước và quốc tế  - VnExpress Du Lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
		        24h qua
            Du lịch Cộng đồng 
                        Dấu chânTư vấnHỏi - Đáp
                                        Du Lịch 
                                        Hỏi - Đáp 
                                 
                                Việt NamQuốc tếCộng đồngẢnh
                            Có nên đi Đà Lạt vào dịp Noel
                                                             
                                Gia đình tôi định trải nghiệm Noel tại Đà Lạt vì nơi này nghe nói đón Giáng sinh rất vui. Xin mọi người cho thêm kinh nghiệm về ăn uống, đi lại, ngủ nghỉ... tại nơi này. Tôi định đi 2 đêm 3 ngày và xuất phát từ Hòa Bình.                            
                            Địa chỉ nhà nghỉ ở Y Tý
                                                             
                                Bạn nào từng nghỉ ở Y Tý để săn mây xin chia sẻ giúp địa chỉ nhà nghỉ hay homestay nào sạch sẽ, tiện nghi. Mình cần liên hệ trước bao lâu để còn chỗ.                            
                            Trẻ một tuổi có nên cho đi chơi Ecopark trong ngày không
                                                             
                                Gia đình tôi định cho hai cháu nhỏ (1 tuổi và 4 tuổi) sang Ecopark, Hà Nội chơi cả ngày trong dịp này. Xin mọi người chia sẻ thêm kinh nghiệm vui chơi và giá cả ở đây.                            
                            Đi Surabaya, Indonesia nên mua gì?
                                                             
                                Tôi có một tuần tại Surabaya, Indonesia trước khi đi tiếp Bali. Mong mọi người tư vấn nên mua đồ, quà gì và đi chơi ở đâu?                             
                            Nên ngắm hoa tam giác mạch ở Hà Giang hay Cao Bằng
                                                             
                                Hoa tam giác mạch ở Hà Giang nhiều nhưng tôi sợ đông vào dịp cuối tuần, còn Cao Bằng chưa biết có nhiều không.                            
                            Khi nào đến mùa hoa dã quỳ
                                                             
                                Tôi và nhóm bạn muốn đi chụp hình kỷ niệm với mùa hoa dã quỳ ở Đà Lạt. Tuy nhiên, chúng tôi không rõ thời điểm nào dã quỳ bắt đầu nở và nhiều hoa nhất?                            
                            Quán cà phê chụp hình đẹp ở Sài Gòn
                                                             
                                Từ Hà Nội vào Sài Gòn, tôi muốn thực hiện một bộ ảnh trong quán cà phê nhưng chưa biết đi đâu. Bạn nào biết quán đẹp chỉ giúp tôi với.                             
                            Tháng 10 đi Mộc Châu có gì
                                                             
                                Đi Mộc Châu tháng 10, tôi nên đưa nhóm bạn từ Sài Gòn ra tham quan những đâu, ăn uống ra sao.                             
                            Có nên đi du lịch trái mùa
                                                             
                                Bạn chọn bên nào, giữa việc đi du lịch đúng mùa, thời tiết thuận lợi, giá cao và trái mùa là chi phí thấp nhưng không chắc được ngắm cảnh đẹp, đồ ăn uống ngon.                            
                            Tháng 11 đi du lịch Phú Quốc hay Đà Nẵng
                                                             
                                Tháng 11 này mình dự định đi du lịch nhưng đang phân vân chưa biết chọn Phú Quốc hay Đà Nẵng. Xin cho mình hỏi nên đi nơi nào hợp nhất.                            
                            Có cần trả tiền để chụp hình ở phố lồng đèn
                                                             
                                Tôi có cần phải trả tiền để được chụp ảnh không hay cứ thế ra mua lồng đèn cho con và chụp thôi.                            
                            6 triệu có nên du lịch Singapore
                                                             
                                Em là sinh viên, tiết kiệm được 6 triệu đồng và có ý định đi Singapore chơi, thăm bạn du học bên đó.                             
                            Thưởng thức các món chè ở Sài Gòn
                                                             
                                Tôi rất thích ăn mấy loại chè như đậu đen, chè bà ba, chè khoai mì hay trôi nước... nhưng mà không biết bán ở đâu. Bạn nào có địa chỉ ngon ở Sài Gòn chỉ giúp tôi với. Tôi cảm ơn.                             
                            Ăn chè bột lọc thịt quay ở đâu ngon
                                                             
                                Xin tư vấn giúp địa chỉ bán món chè bột lọc thịt quay ngon ở Huế. Hà Nội có bán món này không? Nếu có xin chỉ giúp nơi bán.                            
                            Khách sạn view nhìn ra biển ở Nha Trang
                                                             
                                Vợ chồng tôi lên kế hoạch trăng mật ở Nha Trang và muốn tìm thuê phòng khách sạn có view nhìn ra biển khoảng 3 ngày. Xin tư vấn giúp khách sạn có view đẹp và giá cả?                            
                            Hỏi cách mua vé máy bay giá rẻ từ châu Âu về Việt Nam
                                                             
                                Xin mọi người chia sẻ trang web đặt các chuyến bay giá rẻ từ châu Âu về Việt Nam. Sắp tới tôi có chuyến du lịch sang Pháp, Bỉ, vé máy bay chiều đi được người nhà chi trả nhưng lượt về phải tự túc.                            
                            Lúa ở Hoàng Su Phì đã chín chưa
                                                             
                                Mình dự định ngắm lúa chín ở Hoàng Su Phì, Hà Giang nhưng không biết thời điểm nào là đẹp nhất. Điểm nào lý tưởng nhất để ngắm lúa ở huyện này.                            
                            Có 5 triệu vẫn muốn đi nước ngoài
                                                             
                                Em là sinh viên, với 5 triệu đồng, liệu có đủ để du lịch Thái Lan hoặc Campuchia không.                            
                            Tháng 10 đi biển nào ở Việt Nam
                                                             
                                Tôi muốn đi biển cùng gia đình vào tháng 10 này nhưng chưa biết chọn vùng nào để hợp thời tiết cũng như phong cảnh đẹp.                             
                            Đà Lạt có những quán cà phê nào thú vị
                                                             
                                Sắp tới tôi có chuyến công tác dài ngày ở Đà Lạt, muốn hỏi mọi người có quán cà phê nào thú vị để thư giãn.                             
                            Có 300 triệu chứng minh tài sản vẫn không xin được visa
                                                             
                                Tôi năm nay 30 tuổi, đang dự định đi Hàn Quốc nhưng nộp hồ sơ ở các công ty du lịch đều bị trả về. Lý do là tôi có 300 triệu đồng trong sổ tiết kiệm để chứng minh tài chính, nhưng chỉ gửi trong vòng 1 tháng.                            
                            Có nên cắm trại ở Coco Beach Camp
                                                             
                                Tôi định tổ chức team building cho công ty ở khu cắm trại mới của Bình Thuận nhưng một số cho rằng nó chỉ hợp để chụp ảnh.                            
                            Mua cốm Hà Nội ở đâu
                                                             
                                Tôi đang công tác ở Hà Nội và  muốn mùa cốm về làm quà cho người thân, bạn bè. Chỗ nào bán cốm ngon, xin tư vấn giúp. Giá cả thế nào? Ngoài ăn ngay, cốm còn có thể làm gì để tôi hướng dẫn mọi người.                            
                            Có 3.000 USD nên đi Sơn Đoòng hay Maldives
                                                             
                                Tôi là người đam mê chinh phục nhưng cũng rất thích biển. Với hơn 3.000 USD, tôi hoàn toàn có thể đăng ký tour thám hiểm Sơn Đoòng nhưng tôi cũng chưa có dịp đến thiên đường nghỉ dưỡng Maldives. Xin tư vấn giúp tôi nên đi đâu trước?                            
                            Xin kinh nghiệm đi Tam Đảo
                                                             
                                Tôi nghe mọi người nói Tam Đảo có thời tiết cũng khá giống Sa Pa, tới đây vừa tiết kiệm chi phí lại không phải đi quá xa. Xin được mọi người tư vấn về việc ăn gì, chơi gì, tham quan những đâu, ở nhà nghỉ nào... Gia đình chúng tôi gồm 2 vợ chồng và 2 con nhỏ.                            
                            Đi cắm trại nên chọn địa điểm nào
                                                             
                                Tôi xem phim nước ngoài thấy nhiều gia đình thường vác theo lều và đi cắm trại qua đêm ngoài bãi biển. Xin hỏi ở Việt Nam nếu muốn cắm trại kiểu đó thì nên đi đâu mà vẫn đảm bảo được an toàn và vệ sinh? Có cần phải trả tiền để được cắm lều không?                             
                            Bún đậu mẹt ở Hà Nội chỗ nào ngon rẻ
                                                             
                                Tôi rất thích ăn bún đậu mẹt, nhưng lên Hà Nội chưa lâu nên không rõ ở thủ đô có những quán nào bán ngon, đặc biệt là mắm tôm phải pha ngon, chả cốm phải thơm. Giá cả không quá quan trọng.                            
                            Đặt phòng khách sạn qua mạng
                                                             
                                Trước giờ mình toàn đi trong nước nên có thể gọi điện thoại đặt phòng trực tiếp. Sắp tới ra nước ngoài, mình muốn xin kinh nghiệm đặt phòng qua mạng.                            
                            Mua kẹo dừa, bánh tráng sữa ở đâu tại Sài Gòn?
                                                             
                                Tôi rất thích ăn kẹo dừa, bánh tráng sữa Bến Tre và khi có dịp về miền tây đều tranh thủ mua. Nay tôi muốn mua hai món này ở khu vực Sài Gòn thì có bán không và mua ở đâu?                            
                         34567                     
                    Xem tiếp
         Gửi câu hỏi về tòa soạn
        Khuyến mãi Du lịch
		  prev
		   next
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
        Việt Nam
         
                Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                        Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịchViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnKhông gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
        Video
         
            Du khách đứng tim vì voi chặn đầu xe
            Đoàn du khách được một phen hú hồn vì gặp đàn voi khi thăm một khu bảo tồn thiên nhiên tại châu Phi.
                Du khách la hét vì chơi tàu lượn dốc nhất thế giới
                Cả công viên 'nín thở' chờ du khách béo trượt máng
         
             
        Dọc ngang đất nước
                        Quán bún măng vịt mỗi ngày chỉ bán một tiếng
            Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịchViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnKhông gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
        Được hợp tác bởi:
        Xem nhiều nhất
    'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹpGóc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban NhaCặp du khách Trung Quốc quan hệ tình dục trong công viênCả công viên 'nín thở' chờ du khách béo trượt mángBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonChàng tiên cá có thật giữa đời thường
        Hỏi đáp
                            Bạn có nhận ra các điểm check-in của Sao Việt?                            
                                                             
                            7 câu đố bằng thơ ai mê du lịch Việt cũng đoán ra                            
                                                             
                            Những câu hỏi về bản đồ chứng tỏ bạn là phượt thủ thứ thiệt                            
                                                             
                            8 câu hỏi đúng - sai tiết lộ bạn có phải du khách thông minh                            
                                                             
		 
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
