url:http://dulich.vnexpress.net/tin-tuc/cong-dong/tu-van/ke-hoach-4-ngay-kham-pha-dao-thien-duong-o-campuchia-3206720.html
Tittle :Kế hoạch 4 ngày khám phá đảo thiên đường ở Campuchia - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
		        24h qua
            Du lịch Cộng đồng 
                        Dấu chânTư vấnHỏi - Đáp
                                        Du Lịch 
                                        Tư vấn 
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Chủ nhật, 3/5/2015 | 15:00 GMT+7
|
						Kế hoạch 4 ngày khám phá đảo thiên đường ở Campuchia												
					Với bờ cát trắng mịn, nước biển trong xanh, sạch và vắng người, Koh Rong Samloem mang đến cho du khách cảm giác thư thái hoàn toàn.
					Vẻ đẹp huyền ảo của Campuchia trong mưa  /  Trải nghiệm làm Robinson trên đảo Thỏ                    
	Koh Rong Samloem cách Sihanoukville khoảng 20 km. Từ tháng 3 tới tháng 5 là mùa đẹp nhất để đến hòn đảo xinh đẹp này. Trước khi bắt đầu hành trình khoảng 2 tuần, bạn nên đặt trước khách sạn và phương tiện đi lại.
	Phương tiện di chuyển
	Cách 1: Xe buýt chạy tuyến TP HCM – Phnom Penh – Sihanoukville, thời gian di chuyển khoảng 11-12 tiếng, giá vé khoảng 400.000 đồng. Ngoài ra, còn có nhiều hãng xe tập trung ở đường Phạm Ngũ Lão chạy các chặng TP HCM – Phnom Penh, và Phnom Penh – Sihanoukville. Với hướng đi này, bạn sẽ làm thủ tục hải quan ở cửa khẩu Bavet.
					Bãi biển vắng người với cát trắng, nước trong là nơi nghỉ dưỡng lý tưởng. Ảnh: Ngọc Hà
	Cách 2: Đi tuyến TP HCM – Hà Tiên, bạn có thể chọn xe Phương Trang, Kumho, giá từ 175.000 đồng mỗi người. Qua cửa khẩu Xà Xía, bạn có thể chọn đi bộ hoặc xe ôm để ra cửa khẩu Campuchia và đến bến xe buýt. Những người chở xe ôm thường giới thiệu mức phí xe buýt cao hơn bình thường nên bạn cần chú ý.
	Xe buýt hoặc tuktuk đi Sihanoukville giá khoảng 150.000 đồng, tuy nhiên chất lượng khá tệ. 
	Từ Sihanoukville bạn đi tàu tới đảo. Với cano nhanh, thời gian di chuyển mất 45 phút, giá vé khứ hồi là 500.000 đồng mỗi người, xuất phát lúc 13h30, về 14h30. Nếu ở lại qua đêm trên đảo, bạn nhớ cầm cuống vé để hôm sau đúng giờ hẹn ra cầu tàu về. Với tàu chậm, mất khoảng 2 tiếng di chuyển, giá vé khứ hồi là 400.000 đồng mỗi người, bao gồm ăn trưa, lặn ngắm biển.
	Ở tại Sihanoukville và Koh Rong Samloem
	Bạn nên ở một đêm ở Koh Rong Samloem và một đêm ở Sihanoukville để khám phá thêm cuộc sống của người Campuchia.
					Biển trong xanh và không có sóng, bạn có thể tận hưởng làn nước mát lạnh. Ảnh: Ngọc Hà
	Tại Koh Rong Samloem, có một số khách sạn cho bạn tham khảo như The Beach, The Fredoom, The Lazy beach… Trong đó The Beach có nhiều dạng phòng. Nhóm đi đông có thể thuê ở khu nhà 2 tầng, mỗi tầng có 9 - 10 tấm nệm, mỗi tấm cho 2 người nằm. Giá khoảng 150.000 đồng mỗi người.
	Tại Sihanoukville, bạn tham khảo các hostel gần khu biển, dọc đường ra cầu tàu để tiện buổi tối đi dạo. Xung quanh đó cũng có rất nhiều quán ăn. Địa chỉ gợi ý cho bạn là One Stop. Mỗi phòng ở đây có 6 – 8 giường tầng, trang bị máy lạnh, chăn gối thơm tho, giá khoảng 150.000 đồng một người.
	Ăn uống
	Dọc bờ biển có rất nhiều quán ăn, hầu hết là các món Âu, giá từ 80.000 đến 160.000 đồng.
	Ở biển Sihanoukville, nên thử món mực nướng chấm mắm, giá 50.000 đồng cho 10 con.
	Hoạt động tại Koh Rong Samloem
	Đảo Koh Rong Samloem chủ yếu dành cho du khách nghỉ ngơi. Bãi biển dài, bờ cát mịn, nước biển trong xanh, không có sóng phù hợp để bạn thư giãn trong làn nước mát.
	Bạn có thể sử dụng miễn phí các hàng ghế, xích đu, nhà trên cây, võng ở bãi biển, thậm chí có thể vào bất cứ quán nào ngồi nghỉ ngơi mà không gọi món cũng không sao. Ngoài tắm biển bạn có thể băng rừng để qua các khu resort khác như HubaHuba, RobinSon, The Lazy Beach. Bãi biển ở khu này sóng lớn, nhiều gió hơn và hầu như không có người.
	Bạn cũng có thể tham gia "Full Moon party" nếu đi đến đảo đúng ngày trăng tròn. 
					Những khu nghỉ dưỡng ở đây rất tĩnh lặng, hài hòa với thiên nhiên. Ảnh: Ngọc Hà
	Lịch trình tham khảo:
	Ngày 1: Xuất phát từ TP HCM với chuyến xe lúc 9h tối
	Ngày 2: 5h30 sáng hôm sau tới Hà Tiên, ăn sáng, làm thủ tục qua cửa khẩu. Đón xe tới Sihanoukville. Ăn trưa, nghỉ ngơi. 13h30 đón tàu đi Koh Rong Samloem. Chiều ăn chơi ở đây.
	Ngày 3: Ăn sáng, sau đó băng rừng qua khu Huba Huba, Robinson hoặc The Lazy Beach. Ăn trưa, nghỉ ngơi. Quay ngược lại trả phòng và đi ra cầu tàu về lại SihanoukVille. Ăn tối và dạo biển đêm ở Sihanoukville.
	Ngày 4: Ăn sáng, tham quan Sihanoukville. Ăn trưa, trả phòng và đón xe về cửa khẩu. Làm thủ tục tại cửa khẩu về Hà Tiên. Tham quan Hà Tiên. Tối lên xe chuyến 8h30 về lại TP HCM.
	Ngọc Hà                        
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
                Khám phá Campuchia
                    Đặc sản nhện đen ở Campuchia (10/9)
                                             
                    Thiên đường 'đẹp như Maldives' gần Sài Gòn (24/6)
                                             
                    Mùa hè khám phá thành phố biển đẹp nhất Campuchia (5/6)
                                             
                    Chùa gần 100 tuổi trên cao nguyên nổi tiếng Campuchia (16/5)
                                             
                    25 trải nghiệm không thể bỏ qua khi du lịch Siem Reap (5/1)
                                             
                Xem thêm
            Xem nhiều nhất
                            'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                                                             
                                Góc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban Nha
                                                                     
                                Cặp du khách Trung Quốc quan hệ tình dục trong công viên
                                                                     
                                Cả công viên 'nín thở' chờ du khách béo trượt máng
                                                                     
                                Bí mật đáng sợ bên dưới hệ thống tàu điện ngầm London
                                                                     
                 
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                Tags
                                    Kinh nghiệm du lịch
                                Du lịch Koh Rong Samloem
                                Sihanoukville
                                Phượt Cam
    Tin Khác
                            5 vùng đất Samurai nổi tiếng của Nhật Bản
                                                             
                            Khu cắm trại giữa rừng dương cách Sài Gòn 3 giờ chạy xe
                                                             
                            5 tuyệt chiêu hẹn hò với người lạ trên đường du lịch
                                                             
                                                     
                            Quang Vinh khám phá hai mặt đối lập của Sài Gòn
                                                             
                                    Khách Tây truyền nhau bí kíp mua bia ở Việt Nam
                                                                             
                                    16 điều bạn nên tránh khi du lịch Nhật Bản
                                                                             
                                    Chàng trai Sài Gòn du lịch tự túc Đài Loan với 15 triệu đồng
                                                                             
                                    6 món ngon Quy Nhơn níu chân du khách
                                                                             
                                    Điều cần biết khi tắm khỏa thân ở Nhật Bản
                                                                             
                                    Khách sạn mini - xu hướng mới ở Phú Quốc
                                                                             
                                    5 món nướng cho ngày mưa Đà Lạt
                                                                             
                                    Những nơi được check in khi Hà Nội đón gió lạnh đầu mùa
                                                                             
                                    3 loài hoa dại khiến phượt thủ lên Đà Lạt tháng 11
                                                                             
                                    Bỏ qua bún chửi, Hà Nội còn nhiều món bún ngon nên thử
                                                                             
        Khuyến mãi Du lịch
		  prev
		   next
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
        Việt Nam
         
                Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                        Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịchViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnKhông gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
         Gửi bài viết về tòa soạn
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
        Ảnh
         
                             
            Quang Vinh khám phá hai mặt đối lập của Sài Gòn
            Sinh ra và lớn lên ở Sài Gòn, Quang Vinh muốn giới thiệu những điểm thú vị mà mọi người có thể đã bỏ qua với loại hình du lịch mới - staycation.
                                     
                Khách sạn mini - xu hướng mới ở Phú Quốc
                                     
                Homestay nằm giữa hồ sen ở Ninh Bình
        Video
         
            Du khách đứng tim vì voi chặn đầu xe
            Đoàn du khách được một phen hú hồn vì gặp đàn voi khi thăm một khu bảo tồn thiên nhiên tại châu Phi.
                Du khách la hét vì chơi tàu lượn dốc nhất thế giới
                Cả công viên 'nín thở' chờ du khách béo trượt máng
        Bình luận hay
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
