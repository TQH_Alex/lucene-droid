url:http://haidangtravel.com/tin-tuc/Bang-rung-tren-cung-duong-trek-dep-nhat-Viet-Nam.html
Tittle :Băng rừng trên cung đường trek đẹp nhất Việt Nam
content :Toggle navigation
                             Trang Chủ
                              Tour Du Lịch
                         Tour Trong Nước Tour Nước Ngoài
                              Tổ Chức Sự Kiện
                             Thuê Xe Du Lịch
                         Thuê Xe 4 Chổ Thuê Xe 7 Chổ Thuê Xe 16 Chổ Thuê Xe 29 Chổ Thuê Xe 35 Chổ Thuê Xe 45 Chổ
                             Khách Sạn
                              Cẩm nang du lịch
                              Tin tức du lịch
                              Bảng Giá Tour
                     1900 2011
                    08.3849.3838 - 0948.99.1080
                    Đăng Ký
                                            ×
                                            ĐĂNG KÝ
                                                        Tên đăng nhập (bắt buộc)
                                                        Nhập số điện thoại ( bắt buôt )
                                                        Nhập họ tên của bạn
                                                        Nhập mail của bạn
                                                        Nhập mật khẩu ( bắt buột )
                                                        Nhập lại mật khẩu ( bắt buột )
                                                        NamNữKhác
                                                        Địa chỉ nhà bạn
                                                        Ngày sinh của bạn
                                                 Nhận mail từ Hải Đăng Travel
                                                Đăng Ký
                        Đăng nhập
                                            ×
                                            ĐĂNG NHẬP
                                        Quên mật khẩu ?
                                        Đăng Ký
                                                Ghi nhớ mật khẩu
                                                Đăng nhập
                    Trang chủ
                 ›
                    Băng rừng trên cung đường trek đẹp nhất Việt Nam
        Cẩm nang du lịch
                        KHÁM PHÁ
                    An GiangBắc CạnBắc GiangBạc LiêuBắc NinhBến TreBình DươngBình ĐịnhBình PhướcCà MauCần ThơCao BằngChâu ĐôcCôn ĐảoĐà LạtĐà NẵngĐăk LakĐăk NôngĐảo Bà LụaĐảo Bình BaĐảo Bình HưngĐảo Điệp SơnĐảo Nam DuĐiện BiênĐồng NaiĐồng ThápGia LaiHà GiangHà NộiHà TiênHà TĩnhHải PhòngHậu GiangHồ Chí MinhHòa BìnhKiên GiangKon TumLai ChâuLạng SơnLào CaiLong AnNam ĐịnhNghệ AnNha TrangNinh BìnhNinh ChữPhan ThiếtPhú QuốcPhú ThọPhú YênQuảng BìnhQuảng NamQuảng NgãiQuảng NinhQuảng TrịQuy NhơnSóc TrăngSơn LaTây NinhThái BìnhThái NguyênThanh HóaThừa Thiên HuếTiền GiangTrà VinhTuyên QuangVĩnh LongVĩnh PhúcVũng TàuYên Bái
                            Kinh nghiệm
                                                                            Du lịch trong nướcDu lịch nước ngoàiKinh Nghiệm Đi Biển Đảo
                            Ẩm Thực
                            Thư giản
                                                                            Truyện cườiTop du lịchẢnh đẹpChuyện lạHình chếPhượt viết
                            Du Lịch & Thời Trang
                            Tôi Viết
                                    Go
                                    Băng rừng trên cung đường trek đẹp nhất Việt Nam
                                        19/11/2016Đà Lạt
                                    Những ai từng đi cung đường Tà Năng – Phan Dũng qua 3 tỉnh Lâm Đồng, Ninh Thuận, Bình Thuận sẽ không ngần ngại thốt lên: không nơi nào có tuyến đường trek đẹp hơn được nữa. 
                                    Những ai từng đi cung đường Tà Năng – Phan Dũng qua 3 tỉnh Lâm Đồng, Ninh Thuận, Bình Thuận sẽ không ngần ngại thốt lên: không nơi nào có tuyến đường trek đẹp hơn được nữa.
Tổng hành trình cung đường là 55km bao gồm băng rừng, leo đèo, vượt suối. Di chuyển từ độ cao 1100m xuống còn 500m so với mực nước biển. Đây là vùng đất chuyển tiếp từ cao nguyên xuống duyên hải miền trung.
Khoảnh khắc dạo bước dưới những tia nắng vàng đầu tiên rải đều trên ngọn cỏ và từng làn sương mỏng bay lãng đãng bên sườn đồi khiến người đi như có cảm giác đang lạc vào thiên đường nơi hạ giới.
Một chút xanh xanh của cây cỏ, một chút vàng óng của những tia nắng mai xuyên qua màn sương mỏng để ta có thể chạm tay vào cái thuần khiết của thiên nhiên.
Giữa rừng sâu thẳm mọi thứ như chậm lại, nơi này chỉ có cỏ cây, hoa lá, chim muông, đất trời giao hòa, không gian và thời gian hoàn toàn tách biệt với nhịp sống hối hả thường ngày. Ở đây, sóng điện thoại là thứ xa xỉ.
Ngày mới được đánh thức bằng tiếng chim hót véo von. Cánh cửa lều mở, những đồi cỏ xanh rì nối tiếp nhau dài vô tận, kết nối nhau bởi con đường mòn.
 
Đôi khi chúng tôi thấy choáng ngợp trước sự hùng vĩ của núi đồi trùng trùng điệp điệp. Cảnh sắc thiên nhiên nơi đây kỳ vĩ, thật sự khác nhiều so với những vùng đất Tây Nguyên khác.
Con đường mòn dẫn lên ngọn đồi cao với lác đác vài cây thông đứng lẻ loi và màn sương mỏng càng làm cho không gian thêm huyền bí, phiêu du.
Khi đi cung Tà Năng – Phan Dũng, bạn có thể lên 3 ngọn đồi cao để ngắm nhìn hoàng hôn, bình minh hay những con đường mòn đất đỏ bazan như dải lụa uốn lượn giữa triền đồi.
Một điểm không thể bỏ qua trong hành trình là mốc tam giác, nơi giao nhau giữa 3 tỉnh Lâm Đồng, Ninh Thuận, Bình Thuận.
Khi đôi chân đã mỏi, gương mặt lấm tấm những giọt mồ hôi, dòng thác Yavly nước trắng xóa xuất hiện trước mắt chúng tôi, xua tan mệt nhọc của hành trình dài.
Ngọn thác hùng vĩ nổi bật giữa màu xanh đại ngàn.
Đây là cung đường lý tưởng dành cho những ai đam mê trekking, muốn tìm chốn bình yên, hòa mình với thiên nhiên.
Cung trekking Tà Năng – Phan Dũng đi qua 3 tỉnh Lâm Đồng, Ninh Thuận, Bình Thuận. Điểm xuất phát từ xã Tà Năng, huyện Đức Trọng, tỉnh Lâm Đồng cách thành phố Đà Lạt 60 km về phía Nam, điểm kết thúc là xã miền núi Phan Dũng, huyện Tuy Phong, Bình Thuận.
Hành trang:
- Thời tiết, trang phục: Mùa này, sau 14h trời thường mưa, vì vậy cần chuẩn bị áo mưa cho người và balo. Buổi trưa trời nắng. Về khuya, trời khá lạnh nhất là khoảng 2-3h. Vì vậy bạn nên mang áo khoác dài tay, khăn rằn vừa chống nắng vừa chống lạnh.
- Thức ăn, nước uống: Đây là cung đường từ 3 ngày trở lên, cần chuẩn bị thức ăn đảm bảo chất dinh dưỡng nhưng gọn nhẹ. Ngày thứ 2 trekking trên những đồi trọc không có suối, cần chuẩn bị nước uống đầy đủ, ít nhất 3 lít nước.
- Lều: khu vực hạ trại là đồi trọc nên gió thổi khá mạnh, cần chuẩn bị cọc cắm lều chắc chắn.
- Túi y tế: Rừng rậm có nhiều bò sát như rắn, bò cạp; nhiều côn trùng – nhất là ong vò vẽ – rất độc.
Đô Râu (Theo Zing News)
        Đọc Nhiều Nhát
                Kỷ Niệm Đà Lạt 
                Chương trình khuyến mãi tour du lịch tết 2016
                Bảng Giá Tour Lễ 30 Tháng 4 Và Giỗ Tổ
        Bài viết mới nhất
                    Biển Của Người Xứ Nẫu
                    Quy Nhơn Thiên Đường Của Biển
                    Ôi Nha Trang
        Cẩm Nang Du Lịch
         Cẩm nang Ninh Chữ Cẩm nang Nha Trang Cẩm nang Đà Lạt Cẩm nang Phú Yên Cẩm nang Phú Quốc Cẩm nang Quảng Ngãi Cẩm nang Quảng Nam Cẩm nang Phú Thọ Cẩm nang Phan Thiết Cẩm nang Quảng Bình Cẩm nang Ninh Bình Cẩm nang Lạng Sơn
        Kinh nghiệmThư giản
                    Du lịch trong nướcDu lịch nước ngoàiKinh Nghiệm Đi Biển Đảo
                    Phượt viếtHình chếChuyện lạ
                    Ảnh đẹpTop du lịchTruyện cười
    Ảnh Khách Hàng
                    TRỤ SỞ CHÍNH
                         225 Bàu Cát, P12, Q.Tân Bình, HCM
                         19002011 - 08 3849 3838 
                        08 3849 2999   
                          www.haidangtravel.com  
                         info@haidangtravel.com 
                        CSKH: 0948.99.1080
                    CHI NHÁNH
                         Quận 1 : Lầu 7, Tòa Nhà Viễn Đông, 14 Phan Tôn
                        19002011(Ext-107) -  0838204589  
                         Quận 12 : 256 TX25, Thạnh Xuân, Q12 
                        19002011(Ext-111)
                         Biên Hòa : 51A2, KP11, P.Tân Phong 
                        19002011(Ext-108) -  0613999392  
                    TRỢ GIÚP
                    Liên hệ & giới thiệuĐiều khoản chungHình thức thanh toánHướng dẫn mua tourChính sách bảo mật
                        GIỜ LÀM VIỆC
                            Thứ 2 đến 6
                                Sáng: 08:00-12:00Chiều:13:30-17:30
                            Thứ 7 Sáng: 08:00-12:00Nghỉ chủ nhật, lễ, tết
                    Được tìm kiếm nhiều
                        Tour Nha Trang
                        Tour Đà Lạt
                        Tour Cà Mau
                        Tour Phú Yên
                        Tour Ninh Chữ
                        Tour Côn Đảo
                        Tour Miền Bắc
                        Tour Phú Quốc
                        Tour Đà Nẵng
                        Tour Phan Thiết
                        Tour Bình Ba
                        Tour Nam Du
                        Tour Vũng Tàu
                        Tour Bà Lụa
                THÀNH TÍCH
            2015 © All Rights Reserved. Công ty cổ phần xây dựng du lịch Hải Đăng
        Your browser doesn't support canvas. Please download IE9+ on
            IE Test Drive
            +
                Có thể bạn quan tâm ?
                                Tour Ninh Chữ - Vĩnh Hy Khám Phá Bãi Đá Bảy Màu Resort 3sao 2N2Đ
                                 1,250,000 đ
                                Tour Côn Đảo, Khám Phá Lịch Sử Văn Hóa Côn Sơn 2N1Đ
                                 4,590,000 đ
                                Tour Đảo Bình Hưng, Hang Rái, Vĩnh Hy 2n2d
                                 1,111,000 đ
                                Tour du lịch Vũng Tàu, Long Hải, Suối nước nóng Bình Châu 2N1Đ
                                 789,000 đ
                                Tour Đà Lạt Phototrip Khám Phá Những Cung Đường Hoa 3N3Đ
                                 1,250,000 đ
                                Tour Ninh Chữ - Vĩnh Hy - Cổ Thạch đồng giá 2N2Đ
                                 999,000 đ
                                Tour Côn Đảo, Trọn Gói Vé Máy Bay 2N1Đ
                                 3,999,000 đ
                                Tour Đảo Phú Quý, Khám Phá Hòn Đảo Mang Vẻ Đẹp Hoang Sơ 3N3Đ
                                 2,250,000 đ
                                Tour Đảo Hòn Sơn Khám Phá Đỉnh Ma Thiên Lãnh 2N2Đ
                                 1,390,000 đ
                                Tour Ninh Chữ, Vĩnh Hy Hang Rái Siêu Tiết Kiệm 2n2d
                                 850,000 đ
