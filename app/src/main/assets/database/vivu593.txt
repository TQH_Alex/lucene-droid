url:https://www.ivivu.com/blog/2016/11/thac-pongour-diem-den-giup-ban-quen-dinh-kien-da-lat-chi-toan-co-hoa/
Tittle :Thác Pongour – điểm đến giúp bạn quên định kiến Đà Lạt chỉ toàn có hoa
content :Về iVIVU.comCảm nhận của khách hàng iVIVU.com			
					Trang ChủKhuyến MãiMuôn MàuĐiểm đến
Việt Nam
	Du lịch Phan ThiếtDu lịch Đà LạtDu lịch Đà NẵngDu lịch Hà NộiDu lịch Hội AnDu lịch HuếDu lịch Nha TrangDu lịch Phú QuốcDu lịch Vịnh Hạ LongDu lịch Vũng TàuDu lịch SapaDu lịch Tp.Hồ Chí Minh
Đông Nam Á
	Du lịch CampuchiaDu lịch IndonesiaDu lịch LàoDu lịch MalaysiaDu lịch MyanmarDu lịch PhilippinesDu lịch SingaporeDu lịch Thái Lan
Thế Giới
	Châu ÁChâu ÂuChâu MỹChâu PhiChâu Úc
Top iVIVU
Ẩm thựcMẹoĐặt tourĐặt khách sạnCHÚNG TÔI					
	Cẩm nang du lịch > DU LỊCH VIỆT NAM > Du lịch Đà Lạt > Thác Pongour – điểm đến giúp bạn quên định kiến Đà Lạt chỉ toàn có hoa	
		Thác Pongour – điểm đến giúp bạn quên định kiến Đà Lạt chỉ toàn có hoa
			Dù cách Đà Lạt khá xa nhưng sự hấp dẫn, lôi cuốn của thác Pongour đã xóa tan mọi trở ngại để đưa du khách đến chiêm ngưỡng vẻ đẹp của dòng thác này.
Xem thêm: Du lịch Đà Lạt 
Thác Pongour – điểm đến giúp bạn quên định kiến Đà Lạt chỉ toàn có hoa
Thác Pongour còn được mệnh danh là Nam Thiên đệ nhất thác, tọa lạc tại xã Tân Hội, huyện Đức Trọng cách trung tâm thành phố Đà Lạt 50 km về hướng nam trên quốc lộ 2. Hiện thác còn không quá nhiều nước do làm thủy điện, nhưng với dòng chảy bậc thềm rộng lớn vẫn tạo thành khung cảnh độc đáo. Khu thung lũng hạ lưu của thác cũng là địa điểm thích hợp cho cắm trại và nghỉ dưỡng.
Ảnh:@svetaleo
Ảnh:@motomatsu62
Dù cách thành phố Đà Lạt khá xa nhưng sự hấp dẫn, lôi cuốn của thác Pongour đã xóa tan mọi trở ngại để đưa du khách trong và ngoài nước đến chiêm ngưỡng vẻ đẹp của dòng thác có “1-0-2″ này. Nếu như bạn đã từng thăm thú nhiều ngọn thác khác nhau ở Đà Lạt thì chắc chắn thác Pongour sẽ cho bạn một cảm nhận khá mới mẻ.
Ảnh:@sashakon
Ảnh:@chiisaishin
Ảnh:@maxi_krebs
Thác Pongour cao khoảng 50m, và chia thành 7 tầng thác đổ, nhìn từ xa như mái tóc của một người phụ nữ tuyệt đẹp. Những tảng đó to, trên những tầng thác làm cho dòng nước tung bọt trắng xóa.
Ảnh:@nam.h.nguyen
Ảnh:@_.chidolia._
Ảnh:@maddieftw
Vẻ đẹp đặc biệt của dòng thác nằm ở hệ thống các bậc đá bằng phẳng, xếp thành lớp tuy không theo bất cứ một trật tự nào nhưng đã “xé” nguồn nước thành hàng trăm dòng nhỏ, tạo thành những thảm nước tung bọt trắng xóa, hùng vĩ. Bên dưới thác là một mặt hồ rộng thênh thang, yên bình với rất nhiều tảng đá nhấp nhố giữa dòng nước.
Ảnh:@kristina_._anitsirk
Ảnh:@raulpinedamx
Ảnh:@raulpinedamx
Thác Pongour gắn với truyền thuyết hào hùng của người đồng bào dân tộc K’ho. Truyện kể, xưa vùng đất này do một nữ tù trưởng người K’ho xinh đẹp cai quản, nàng tên Kanai. Nàng có tài chinh phục thú dữ, tạo điều kiện thuận lợi phục vụ lợi ích cho con người, trong đó có bốn con tê giác to lớn khác thường luôn tuân lệnh nàng. Chúng dời non, ngăn suối, khai phá nương rẫy, bảo vệ dân làng.
Ảnh:@kevankohl
Ảnh:@ponyclub
Ảnh:@bbq.htr
Một ngày vào rằm tháng giêng, nàng trút hơi thở cuối cùng. Bốn con tê giác quanh quẩn bên nàng ngày đêm không rời, chẳng buồn ăn uống cho đến chết. Không lâu sau nơi nàng yên nghỉ sừng sững một ngọn thác đẹp tuyệt trần. Mái tóc của nàng Kanai đã hóa thành dòng nước trắng xóa, trong xanh, mát rượi, còn những phiến đá xanh rêu to lớn làm nền cho thác đổ là những chiếc sừng của tê giác hóa thành. Đó là biểu tượng của sự đoàn kết, gắn bó vĩnh cửu giữa con người với thiên nhiên.
Ảnh:@jeenliiii
Ảnh:@tth.tuyen
Đường đi tới thác Pongour
 – Thác Pongour nằm cách trung tâm thành phố Đà Lạt khoảng 50km, thuộc huyện Đức Trọng, Lâm Đồng. Trên đường đi từ TP.HCM lên Đà Lạt bạn có thể ghé thác Pongour tại km 260, sau đó rẽ phải và đi thẳng khoảng 8km là tới thác.
 – Giá vé vào cổng: 10.000 đồng/người
San San (tổng hợp)
Xem thêm các bài viết:
Lên kế hoạch rủ nhau du lịch Đà Lạt tham gia lễ hội mai anh đào 2017
Du lịch Đà Lạt check-in 2 điểm đến đẹp như mơ trong MV ‘Điều buồn tênh’ của Quang Vinh
Khu nghỉ dưỡng Terracotta Đà Lạt – nơi lý tưởng để trải nghiệm lễ hội hoa mai anh đào 2017
Tham khảo danh sách khách sạn Đà Lạt khuyến mãi cực hấp dẫn từ iVIVU.com
***
Tham khảo: Cẩm nang du lịch iVIVU.com
			iVIVU.com
			November 18, 2016
				Đánh giá bài viết này
				 (1 votes, average: 5.00 out of 5)
			Loading...			
			DU LỊCH VIỆT NAM Du lịch Đà Lạt Nổi Bật 2 Điểm đến
			cảnh đẹp Đà Lạt du lịch đà lạt giá vé thác Pongour khách sạn Đà Lạt Thác Pongour thác Đà Lạt tham quan Đà Lạt tour đà lạt điểm đến đà lạt đường đi thác Pongour		
				Tìm bài viết	
			Đọc nhiềuBài mới
                                        Du lịch Vũng Tàu: Cẩm nang từ A đến Z                                    
                                        16/10/2014                                    
                                        Du lịch đảo Nam Du: Cẩm nang từ A đến Z...                                    
                                        21/03/2015                                    
                                        Du lịch Nha Trang: 10 điểm du lịch tham quan hấp d...                                    
                                        29/06/2012                                    
                                        Du lịch Đà Lạt - Cẩm nang du lịch Đà Lạt từ A đến ...                                    
                                        26/09/2013                                    
                                        Tất tần tật những kinh nghiệm bạn cần biết trước k...                                    
                                        25/07/2014                                    
                                        Lên kế hoạch rủ nhau du lịch Đà Lạt tham gia lễ hộ...                                    
                                16/11/2016                            
                                        Tour Đài Loan giá sốc trọn gói chỉ 8.990.000 đồng,...                                    
                                15/11/2016                            
                                        Check-in Victoria Hội An 4 sao 3 ngày 2 đêm bao lu...                                    
                                15/11/2016                            
                                        Du lịch Đà Lạt check-in 2 điểm đến đẹp như mơ tron...                                    
                                14/11/2016                            
                                        Có một Việt Nam đẹp mê mẩn trong bộ ảnh du lịch củ...                                    
                                13/11/2016                            
		Khách sạn Việt Nam			 Khách sạn Đà Lạt  Khách sạn Đà Nẵng  Khách sạn Hà Nội  Khách sạn Hội An  Khách sạn Huế  Khách sạn Nha Trang   Khách sạn Phan Thiết   Khách sạn Phú Quốc  Khách sạn Sapa  Khách sạn Sài gòn Khách sạn Hạ Long  Khách sạn Vũng Tàu
		Khách sạn Quốc tế			 Khách sạn Campuchia  Khách sạn Đài Loan  Khách sạn Hàn Quốc  Khách sạn Hồng Kông  Khách sạn Indonesia  Khách sạn Lào  Khách sạn Malaysia  Khách sạn Myammar  Khách sạn Nhật Bản  Khách sạn Philippines  Khách sạn Singapore  Khách sạn Thái Lan  Khách sạn Trung Quốc 
		Cẩm nang du lịch 2015			
         Du lịch Nha Trang 
         Du lịch Đà Nẵng 
         Du lịch Đà Lạt 
         Du lịch Côn đảo Vũng Tàu 
         Du lịch Hạ Long 
         Du lịch Huế 
         Du lịch Hà Nội 
         Du lịch Mũi Né 
         Du lịch Campuchia 
         Du lịch Myanmar 
         Du lịch Malaysia 
         Du lịch Úc 
         Du lịch Hong Kong 
         Du lịch Singapore 
         Du lịch Trung Quốc 
         Du lịch Nhật Bản 
         Du lịch Hàn Quốc 
Tweets by @ivivudotcom
		Bạn quan tâm chủ đề gì?book khách sạn giá rẻ
book khách sạn trực tuyến
book phòng khách sạn giá rẻ
Cẩm nang du lịch Hà Nội
du lịch
Du lịch Hàn Quốc
du lịch Hà Nội
Du lịch Hội An
Du lịch Nha Trang
du lịch nhật bản
Du lịch Singapore
du lịch sài gòn
du lịch Thái Lan
du lịch thế giới
Du lịch Đà Nẵng
du lịch đà lạt
hà nội
hệ thống đặt khách sạn trực tuyến
ivivu
ivivu.com
khách sạn
Khách sạn giá rẻ
khách sạn Hà Nội
khách sạn hà nội giá rẻ
khách sạn khuyến mãi
khách sạn nhật bản
khách sạn sài gòn
khách sạn Đà Lạt
khách sạn Đà Nẵng
kinh nghiệm du lịch
Kinh nghiệm du lịch Hà Nội
mẹo du lịch
Nhật bản
sài gòn
vi vu
vivu
Việt Nam
Đặt phòng khách sạn online
đặt khách sạn
đặt khách sạn giá rẻ
đặt khách sạn trực tuyến
đặt phòng giá rẻ
đặt phòng khách sạn
đặt phòng khách sạn trực tuyến
đặt phòng trực tuyến
                                                            Like để cập nhật cẩm nang du lịch			
                                                                    iVIVU.com ©
                                                                        2016 - Hệ thống đặt phòng khách sạn & Tours hàng đầu Việt Nam
                                                                            HCM: Lầu 7, Tòa nhà Anh Đăng, 215 Nam
                                                                            Kỳ Khởi Nghĩa, P.7, Q.3, Tp. Hồ Chí Minh
                                                                            HN: Lầu 12, 70-72 Bà Triệu, Quận Hoàn
                                                                            Kiếm, Hà Nội
                                                                    Được chứng nhận
                iVIVU.com là thành viên của Thiên Minh Groups
                                                                    Về iVIVU.com
                                                                            Chúng tôiiVIVU BlogPMS - Miễn phí
                                                                    Thông Tin Cần Biết
                                                                            Điều kiện & Điều
                                                                                khoảnChính sách bảo mật
                                                                        Câu hỏi thường gặp
                                                                    Đối Tác & Liên kết
                                                                            Vietnam
                                                                                Airlines
                                                                            VNExpressTiki.vn - Mua sắm
                                                                                online
                                                            Chuyên mụcĂn ChơiCHÚNG TÔIChụpDU LỊCH & ẨM THỰCDU LỊCH & SHOPPINGDu lịch hèDu lịch TếtDU LỊCH THẾ GIỚIDu lịch tự túcDU LỊCH VIỆT NAM►Du lịch Bình Thuận►Du lịch Mũi NéĐảo Phú QuýDu lịch Bình ĐịnhDu lịch Cô TôDu lịch Hà GiangDu lịch miền TâyDu lịch Nam DuDu lịch Ninh ThuậnDu lịch Quảng BìnhDu lịch đảo Lý SơnHóngKểKhách hàng iVIVU.comKhuyến Mãi►Mỗi ngày một khách sạnMuôn MàuThư ngỏTiêu ĐiểmTIN DU LỊCHTour du lịchĐI VÀ NGHĨĐiểm đến
