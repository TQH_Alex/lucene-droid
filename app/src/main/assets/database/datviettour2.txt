url:http://datviettour.com.vn/tour-du-lich-he-ha-noi-ha-long-sa-pa-34681.html
Tittle :Tour Du Lịch Hè 2016 | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Tour du lịch hè 2016								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Tour du lịch Hà Nội – Hạ Long – Sa Pa (5 ngày 4 đêm)                    
Miền Bắc có vô vàn cảnh đẹp, nhiều di tích lịch sử và đặc sản hấp dẫn níu chân du khách phương xa. Đặt ngay Tour du lịch Hà Nội - Hạ Long - Sa Pa để có một chuyến du lịch tràn đầy niềm vui và ý nghĩa bên những người thân yêu.
NGÀY 01: TP.HỒ CHÍ MINH - HÀ NỘI - HẠ LONG – TUẦN CHÂU
Xe và hướng dẫn viên công ty du lịch Đất Việt đón khách tại điểm hẹn khởi hành đến sân bay Tân Sơn Nhất, làm thủ tục đón chuyến bay đi Hà Nội.
Xe và hướng dẫn viên đón đoàn tại sân bay Nội Bài khởi hành đi Hạ Long.
Đến Hạ Long đoàn ăn trưa, nhận phòng nghỉ ngơi.
Quý khách tự do tắm biển Hạ Long và thưởng thức hải sản tươi sống trên biển (chi phí tự túc).
Quý khách dùng cơm tối tại nhà hàng, sau đó xe đưa đoàn tham quan ngắm cầu Bãi Cháy, vui chơi tại khu du lịch quốc tế Tuần Châu, xem chương trình nhạc nước hoặc show xiếc cá voi (tự túc chi phí).
NGÀY 02: VỊNH HẠ LONG – HÀ NỘI
Đoàn dùng điểm tâm sáng tại nhà hàng. Xe đưa đoàn đến bến thuyền thăm vịnh Hạ Long - Kỳ quan Thiên nhiên thế giới. Du khách tham động Thiên Cung và hang Đầu Gỗ. Sau đó, đoàn lên du thuyền khám phá vịnh Hạ Long, chiêm ngưỡng muôn vàn đảo đá với hình thù kì lạ như: Hòn Chó Đá, Hòn Đỉnh Hương, Hòn Trống Mái, Hòn Gà Chọi,…Quý khách dùng cơm trưa trên du thuyền.
Đoàn về lại đất liền, làm thủ tục trả phòng khách sạn và khởi hành về lại Hà Nội. Trên đường về, đoàn ghé tham quan và mua sắm bánh đặc sản bánh đậu xanh Hải Dương.
Đến Hà Nội, đoàn nhận phòng khách sạn nghỉ ngơi
Đoàn ăn tối nhà hàng, sau đó tự do vui chơi và khám phá phố cổ Hà Nội về đêm.
NGÀY 03: HÀ NỘI - LÀO CAI – SA PA
Quý khách dùng điểm tâm sáng tại nhà hàng, làm thủ tục trả phòng và khởi hành đi Sa Pa theo con đường cao tốc Hà Nội – Lào Cai. Trên đường đi, quý khách chiêm được ngưỡng cảnh đẹp hùng vỹ của núi rừng Tây Bắc.
Đoàn đến Sa Pa đoàn nhận phòng, ăn trưa, nghỉ ngơi.
Xe đưa quý khách đến khám phá bản Cát Cát. Tại đây đoàn được tìm hiểu nghề dệt nhuộm của người H’Mông, giao lưu với người H’mông, người Dáy, đi chợ Sa Pa - nơi buôn bán đặc sản của các dân tộc ít người vùng Tây Bắc và chiêm ngưỡng thác Bạc – ngọn thác đẹp nhất Sa Pa.
Đoàn dùng cơm tối tại nhà hàng.
Quý khách khám phá TT.Sa Pa về đêm, thưởng thức các đặc sản tại chợ đêm Sa Pa, tham quan nhà thờ Đá, tham dự chợ tình Sa Pa (nếu đúng vào đêm thứ bảy). Đoàn nghỉ đêm tại TT. Sa Pa.
NGÀY 04: SA PA – LÀO CAI – HÀ NỘI
Đoàn dùng điểm tâm sáng tại nhà hàng khách sạn, khởi hành đi tham quan KDL Núi Hàm Rồng. Qúy khách được tham quan vườn lan nhiệt đới, vườn hoa trung tâm, sân Mây, cổng Trời,… thưởng thức chương trình biển diễn nhạc dân tộc đặc sắc và nhiều món đặc sản của núi rừng Tây Bắc.
Đoàn về lại khách sạn làm thủ tục trả phòng, dùng cơm trưa tại nhà hàng và nghỉ ngơi.
Quý khách khởi hành về Hà Nội theo con đường cao tốc Hà Nội – Lào Cai.
Về đến Hà Nội, đoàn nhận phòng, dùng cơm tối và tự do vui chơi khám phá, thưởng thức café Hà Nội.
NGÀY 05: DANH THẮNG HÀ NỘI – TP. HỒ CHÍ MINH
Quý đoàn dùng điểm tâm sáng tại nhà hàng khách sạn. Xe đưa đoàn tham quan lăng Bác, quảng trường Ba Đình, phủ Chủ Tịch, nhà sàn Bác Hồ, chùa Một Cột, bảo tàng Hồ Chí Minh, đền Ngọc Sơn, văn miếu Quốc Tử Giám và tự do mua sắm đặc sản Hà Nội tại chợ Đồng Xuân.
Quý khách dùng cơm trưa tại nhà hàng và làm thủ tục trả phòng khách sạn.
Xe tiễn đoàn ra sân bay Nội Bài, đón chuyến bay về lại Tp. Hồ Chí Minh, kết thúc chuyến tham quan chia tay. Hướng dẫn viên Đất Việt Tour chia tay, chúc sức khoẻ và hẹn ngày gặp lại đoàn trong những chương trình tham quan sau.
Có gì hấp dẫn?
- Tư vấn nhiệt tình, miễn phí
- Lịch trình, ngày khởi hành theo yêu cầu
- Cam kết dịch vụ chất lượng cao
- Quà tặng: Voucher du lịch, nón, ly sứ, ba lô, quà tặng trò chơi
Hotline: 0976 046 046 – 0989 120 120
BẢNG GIÁ TOUR
Lượng kháchKhách sạn 2 saoKhách sạn
3 saoKhách sạn
4 saoNgày khởi hànhKhách Lẻ - Ghép Đoàn
(02 - 08 khách)Liên hệKhách Đoàn – Tour Riêng
(từ 40 khách)3.995.0004.765.000Mọi ngày
Theo yêu cầu
Lưu ý: Giá tour chưa bao gồm vé máy bay
 
GIÁ TOUR BAO GỒM
Phương tiện: Xe 45 chỗ Aero Space, đời mới, đạt tiêu chuẩn du lịch, đưa đón đoàn tham quan suốt tuyến.
Vé máy bay khứ hồi Sài Gòn – Hà Nội (Tùy hãng hàng không quý khách chọn).Khách sạn: Khách sạn đạt tiêu chuẩn 2 - 3 sao. Bố trí 02 – 03 khách/phòng.
- Khách sạn tại Hà Nội: Holiday Sai Gòn, Hòa Bình, Đông Thành, VMQ, Newmoon, ...
- Khách sạn tại Hạ Long: Bạch Đằng, Blue Sky, Kim Cương, Crowhotel,...
- Khách sạn tại Sa Pa: Hoàng Liên, Hoàng Hà, Kim Ngân,…Ăn uống:
- Bữa sáng: Ăn sáng thường hoặc buffet nếu ở khách sạn 3 sao.
- Ăn chính: Gồm 06 -> 07 món ngon, hợp vệ sinh (150.000 vnđ/suất/khách).Hướng dẫn viên: Đoàn có hướng dẫn viên thuyết minh và phục vụ ăn, nghỉ tham quan cho quý khách.Bảo hiểm: Khách được bảo hiểm du lịch Bảo Minh trọn gói, mức bồi thường tối đa 20.000.000 vnđ/trường hợp. Thuốc y tế thông thường.Qùa tặng: Mỗi vị khách trên đường đi được phục vụ nón du lịch, khăn lạnh, 02 chai nước tinh khiết 0.5l/ngày/người và xổ số vui có thưởng.Tham quan: Giá đã bao gồm vé vào cửa các điểm tham quan theo chương trình.
GIÁ TOUR TRẺ EM
Trẻ em từ 5 tuổi trở xuống miễn phí.Trẻ em từ 6 – 11 tuổi tính 50% giá tour người lớn (ngủ chung với ba mẹ).Trẻ em 12 tuổi trở lên tính giá tour như người lớn.
GIÁ TOUR KHÔNG BAO GỒM
Chi phí cá nhân ngoài chương trình như: Giặt ủi, điện thoại, đồ uống trong minibar,…Tiền Típ cho hướng dẫn viên và tài xế (nếu có).Vé máy bay khứ hồi Sài Gòn – Hà Nội.Vé tham quan Tuần Châu và các Show nhạc nước + cá heo.                
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Tour du lịch Nha Trang – đảo Bình Ba 3 ngày 3 đêm
                                Tour du lịch hành hương thập cảnh chùa Đà Lạt
                                Tour du lịch Phan Thiết – Mũi Né – Nha Trang – Đà Lạt
                                Tour du lịch Đà Lạt thành phố ngàn hoa
                                Tour du lich Mỹ Tho – Cần Thơ – Cà Mau – Sóc Trăng – Bạc Liêu
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
