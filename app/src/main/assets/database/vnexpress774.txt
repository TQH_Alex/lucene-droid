url:http://dulich.vnexpress.net/photo/anh-video/ngay-ay-bay-gio-cua-cac-thanh-pho-du-lich-noi-tieng-the-gioi-3449380.html
Tittle :Ngày ấy - bây giờ của các thành phố du lịch nổi tiếng thế giới - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
		        24h qua
            Du lịch Ảnh 
                                        Du Lịch 
                                        Ảnh 
                                 
                                Việt NamQuốc tếCộng đồngẢnh
                            Thứ ba, 9/8/2016 | 05:05 GMT+7                        
|
                        Ngày ấy - bây giờ của các thành phố du lịch nổi tiếng thế giới                                                
                    Gần 2 thập kỷ trước, Dubai trông nhỏ bé và đơn sơ, chỉ như một vùng đất khô cằn đầy nắng, gió, cát bụi bên bờ biển vịnh Ba Tư.
                                Bộ ảnh ghép Côn Đảo xưa và nay
                                Đà Lạt xưa và nay
                                             
	Dubai
	Thành phố Dubai ngày nay được xem như viên ngọc quý nằm phía Đông Nam bờ biển vịnh Ba Tư, trong khu vực các Tiểu vương quốc Ả Rập thống nhất với hàng loạt tụ điểm mua sắm hàng cao cấp, các tòa nhà kiến trúc độc đáo và cuộc sống về đêm sôi động.
	Năm 2012, Dubai đứng thứ 22 trong danh sách quốc gia có mức chi tiêu cao nhất thế giới và dẫn đầu khối các Tiểu vương quốc Ả Rập về độ đắt đỏ. Tới năm 2014, mức giá thuê phòng khách sạn ở thành phố này được xếp hạng cao thứ nhì toàn cầu, chỉ sau Geneva (Thụy Sĩ). Tuy vậy, cách đây 16 năm, Dubai chỉ là một điểm đến nhỏ bé và đơn sơ, đầy nắng, gió, cát bụi như bao vùng đất sa mạc khác.
                                                                                 
                                             
	Abu Dhabi
	Cũng giống người anh em Dubai, Abu Dhabi là thành phố quan trọng thứ hai trong khu vực các Tiểu vương quốc Ả Rập thống nhất, xếp hạng 68 trong danh sách điểm đến có mức chi tiêu đắt đỏ nhất thế giới vào năm 2014. Hơn 4 thập kỷ qua, Abu Dhabi phát triển không ngừng với những tòa nhà chọc trời, trung tâm mua sắm, khu vui chơi giải trí hay khách sạn hạng sang có thể làm say lòng mọi du khách.
                                                                                 
                                             
	Singapore
	Là một trong những quốc gia năng động, sự đổi thay ở Singapore diễn ra với tốc độ nhanh chóng. Chỉ 1-2 năm không tới đây, du khách có thể ngỡ ngàng bởi những công trình hay khu vui chơi mới. Năm 2000, đảo quốc sư tử có nhiều tòa nhà cao tầng nhưng vẫn còn giữ nét hoang sơ. Suốt 16 năm qua, chuyện hút khách du lịch mới dần rõ rệt thông qua nhiều điểm đến mới, xa xỉ như Marina Bay Sands hay Singapore Flyer...
                                                                                 
                                             
	Thượng Hải
	Khởi đầu là một làng chài hẻo lánh, Thượng Hải trở thành thành phố quan trọng bậc nhất ở Trung Quốc và một thời từng là trung tâm tài chính lớn thứ 3 thế giới (sau New York và London). Năm 1987, Thượng Hải đã mang vẻ đẹp hiện đại với những tòa nhà bê tông vuông vức hay bến cảng sầm uất.
	Tuy vậy, gần 3 thập kỷ sau, sự đổi mới rõ rệt hơn khi thành phố có thêm nhiều công trình mới mà du khách có thể ghé thăm như tháp truyền hình Minh Châu Phương Đông.
                                                                                 
                                             
	Macau
	Nằm trên một bán đảo nhỏ, Macau mang vẻ khác biệt khi ảnh hưởng sự pha trộn văn hóa giữa Bồ Đào Nha và Trung Quốc. Nơi đây còn được mệnh danh là “Las Vegas châu Á” với những sòng bài khổng lồ và các trung tâm vui chơi giải trí xa xỉ. Tuy vậy, hơn 50 năm trước, Ma Cao chỉ là một vùng biển đơn sơ với những ngôi nhà thấp tầng.
                                                                                 
                                             
	Hong Kong
	Với mức thu hút gần 28 triệu du khách ghé thăm trong năm, Hong Kong đang được xem là điểm đến hấp dẫn nhất thế giới. Từng là thuộc địa của Anh, đồng thời nằm ở vị trí đắc địa nơi cảng biển, Hong Kong trở thành vùng đất lý tưởng cho bất cứ du khách nào tới đây thỏa mãn thú vui mua sắm hay tìm hiểu văn hóa địa phương. Năm 1980, Hong Kong đã nổi bật với những tòa nhà cao tầng hiện đại nhưng chỉ 36 năm sau, vẻ đẹp này như một cuộc lột xác với hàng loạt tòa tháp chọc trời, lấp lánh ánh đèn cùng khu cảng biển sầm uất.
                                                                                 
                                             
	Fortaleza
	Fortaleza là thủ phủ bang Ceara, Brazil, nổi tiếng với những bãi biển, vách đá đỏ, cây cọ, cồn cát và đầm phá. Du khách đến với Fortaleza sẽ bị mê hoặc bởi những bữa tiệc sôi động ban đêm trên bờ biển hay các nhà hàng, trung tâm du lịch hay bảo tàng nghệ thuật độc đáo. Hơn 4 thập kỷ qua, Fortaleza dường như được "thay da đổi thịt" với vẻ hiện đại, năng động của những tòa nhà cao chọc trời.
                                                                                 
                                    Trần Hằng
                                                                              |  
                Khám phá thế giới qua những bộ ảnh
                    Hội An nhìn từ trên cao (14/11)
                                             
                    Vẻ đẹp cổ tích của mùa thu nước Mỹ nhìn từ trên cao (13/11)
                                             
                    Người đàn ông đi khắp nơi để 'cầm cả thế giới' (6/11)
                                             
                    Cảnh đan lưới ở làng chài Việt đạt giải ảnh quốc tế (1/11)
                                             
                    Cuộc sống ở vùng đất -50 độ C (31/10)
                                             
                Xem thêm
                         
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                        Tags
                                                    Lột xác
                                                    Thập kỷ
                                                    Macao
                                                    Singapore
                                                    Hong Kong
                                                    Thượng Hải
    Tin Khác
                                                     
                            Diễm My ăn trưa, trồng rau cùng người dân Hội An
                                                             
                                                     
                            Khu ổ chuột đầy rẫy tội phạm một thời ở Hong Kong
                                                             
                                                     
                            Góc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban Nha
                                                             
                            Những bữa tiệc 'nóng mắt' với bong bóng xà phòng
                                                             
                                    Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                                                             
                                    Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
                                                                             
                                    'Vườn thú người' - vết nhơ trong lịch sử châu Âu
                                                                             
                                    Vẻ đẹp của thị trấn nơi William và Kate gặp nhau
                                                                             
                                    Khu cắm trại giữa rừng dương cách Sài Gòn 3 giờ chạy xe
                                                                             
                                    Vẻ đẹp thơ mộng mùa lá đỏ Nhật Bản
                                                                             
                                    Bên trong thị trấn ma lớn nhất thế giới ở quê hương Thành Cát Tư Hãn
                                                                             
                                    Cô gái bị fan cuồng sao y bản chính khi đi du lịch
                                                                             
                                    Quang Vinh khám phá hai mặt đối lập của Sài Gòn
                                                                             
                                    Bảo tàng áo dài tư nhân duy nhất ở Sài Gòn
                                                                             
        Khuyến mãi Du lịch
		  prev
		   next
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
        Xem nhiều nhất
    Góc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban NhaCặp du khách Trung Quốc quan hệ tình dục trong công viênCả công viên 'nín thở' chờ du khách béo trượt mángBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonChàng tiên cá có thật giữa đời thường
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
