url:http://datviettour.com.vn/tour-du-lich-he-tham-quan-myanmar-4-ngay-3-dem-33931.html
Tittle :Danh Sách Lịch Khởi Hành | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Danh sách Lịch khởi hành								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Tour du lịch: Khám phá đất nước Myanmar, 4 ngày 3 đêm                    
Đất nước Myanmar với vô số những điều bất ngờ và thú vị đang chờ đón du khách. Hãy cùng Đất Việt Tour thực hiện ngay một chuyến du lịch Myanmar để có cơ hội trải nghiệm ý nghĩa tại đất nước xinh đẹp này. Đặt tour ngay để có cơ hội nhận quà tặng hấp dẫn và du lịch tiết kiệm!
NGÀY 1: TP.HCM - YANGON – CHÙA VÀNG (Ăn: Trưa - Tối)
07h00 : Tập trung tại sân bay Tân Sơn Nhất – Ga đi Quốc tế – Cổng D2. Làm thủ tục đáp chuyến bay Tp.HCM – Yangon lúc 10h25.
12h10: Đến sân bay quốc tế Yangon, xe và hướng dẫn viên đón đoàn đi ăn trưa, về khách sạn nhận phòng nghỉ ngơi.
Chiều: Qúy khách viếng chùa Shwedagon – một trong những chốn linh thiêng bậc nhất của đất nước Myanmar, luôn phát ra những tia sáng vàng lấp lánh với 9300 lá vàng dát mỏng tương đương 500kg và hàng ngàn viên kim cương, đá quý được dát trên toà tháp trung tâm. Trong chùa còn có hàng trăm chiếc chuông vàng khắp mọi nơi.
Tối: Qúy khách ăn tối, tự do khám phá thành phố Yangon từng là thủ đô của Myanmar hoặc nghỉ ngơi tại khách sạn.
NGÀY 2: YANGON – BAGON – KYAIKHTIYO (Ăn: Sáng - Trưa - Tối)
Sáng: Qúy khách ăn sáng, trả phòng và khởi hành đi Bago (cách Yangon 80km).
Trên đường ghé thăm Tượng đài Thouk Chan – nơi an nghỉ của 27.000 phần mộ quân đồng minh đã ngã xuống trong chiến tranh thế giới thứ II tại đất nước Myanmar.
Đến thành phố Bago, quý khách viếng chùa tượng Phật 4 mặt Kyaitpun, dự nghi thức cung tiến bữa trưa cho các nhà sư tại Thiền viện Kyat Khat Wine nơi có hơn 700 nhà sư đang tu niệm.
Trưa: Ăn trưa tại nhà hàng địa phương với các món ăn đặc sản của Myanmar.
Chiều: Đoàn tham quan chùa Phật nằm Shwei Tha Lyaung - ngôi chùa có hơn 1000 năm lịch sử và lắng nghe truyền thuyết về uy quyền, sức mạnh của đức Phật. Tiếp tục hành trình, quy khách viếng chùa vàng Shwei Mor Do và Cung điện hoàng gia Kanbawza thadi.
Qúy khách khởi hành đi Hòn Đá Vàng, dừng chân và nghỉ đêm tại resort Eternity duới chân núi.
Tham khảo >>> Thủ tục xin visa Myanmar
NGÀY 3: HÒN ĐÁ VÀNG – KYAIKHTIYO – YANGON (Ăn: Sáng - Trưa - Tối)
 Sáng: Qúy khách ăn sáng, khởi hành lên trạm dừng chân Kin Pun trên núi. Di chuyển bằng xe chuyên dụng để lên chùa Hòn Đá Vàng hay còn gọi là chùa Kyaithtiyo – một trong những ngôi chùa nổi tiếng và cổ kính nhất của đất nước Myanmar, xây dựng hơn 2500 năm trước, ngay khi Đức Phật còn sống. Chiêm ngưỡng một tảng đá thiêng nằm cheo leo trên bờ vách đá và được bao bọc bởi hàng nghìn lá vàng dát mỏng.
Trưa: Đoàn ăn trưa, khởi hành trở lại thành phố Yangon. Qúy khách ghé tham quan chùa Voi Trắng và tự do mua sắm tại chợ Bogyoke Aung San - ngôi chợ nổi tiếng nhất Yangon, đã có trên 70 năm tuổi, với hơn 2000 quầy hàng bán từ con rối nổi tiếng của Mandalay, đến những viên đá quý lấp lánh.
Tối: Qúy khách ăn tối và nhận phòng nghỉ ngơi, có thể thay đổi bữa tối để tham dự buffet tại cung điện nổi Karaweik Royal Barge trên hồ Kandawgyi thơ mộng va cùng thường thức các điệu múa dân gian đậm đà màu sắc văn hoá Myanmar (Chi phí tự túc).
Xem thêm : Tour du lịch Tết 2017: khám phá đất nước Myanmar, 4 ngày 3 đêm
NGÀY 4: YANGON – CHÙA XÁ LỢI PHẬT -  TP.HCM (Ăn: Sáng - Trưa - Tối)
Sáng: Qúy khách ăn sáng, trả phòng. Đoàn tham quan Bảo tàng Xá lợi Phật Thone Wain và làm lễ thỉnh xá lợi cầu may do đích thân Đức tăng Thống làm lễ trao xá lợi (chi phí giọt dầu thỉnh xá lợi do quý khách tự túc).
Tiếp tục đoàn tham quan chùa Chaukhtatgyi - nơi có tượng Phật nằm khổng lồ dài 72m, cao 16m nằm uy nghi giữa trung tâm thành phố.
09h30, đoàn ra sân bay làm thủ tục về Việt Nam trên chuyến bay lúc 12h10. Đến sân bay Tân Sơn Nhất lúc 14h55, kết thúc chương trình tạm biệt và hẹn gặp lại.
Tìm hiểu thêm: Tour du lịch nước ngoài
Có gì hấp dẫn?
- Khởi hành thứ 6 hàng tuần
- Tư vấn & giao vé miễn phí
- Cam kết dịch vụ chất lượng
- Quà tặng: Nón du lịch, bao da hộ chiếu
Hotline: 0909 108 369 – 0966 980 936
 
BẢNG GIÁ TOUR 
Lượng kháchKhách sạn
2 saoKhách sạn
3 saoKhách sạn
4 saoNgày
khởi hànhKhách Lẻ - Ghép Đoàn
(02 - 08 khách)10.990.000Thứ 6 hàng tuầnKhách Đoàn - Tour Riêng
(trên 40 khách)Mọi ngày
Theo yêu cầu
Lưu ý:
Giá tour trẻ em:
Trẻ em từ 02 - dưới 11 tuổi: 8.790.000 vnđ/kháchTrẻ em dưới 02 tuổi: 2.200.000 vnđ/khách
Phụ thu phòng đơn: 2.500.000 vnđ/khách
GIÁ TOUR BAO GỒM
Phương tiện: Vé máy bay và thuế hãng hàng không Vietjet Air khứ hồi theo chương trình du lịch. Hành lý xách tay 7 kg và 15 kg hành lý ký gửi.
Xe máy lạnh đưa đón tham quan theo chương trình.Khách sạn: 3 sao, tiêu chuẩn 2 khách/phòng (trường hợp 3 khách vì lý do giới tính, nếu khách lẻ nam/nữ sẽ ghép phòng 3 khách hoặc phụ thu phòng đơn).Ăn uống: Ăn sáng: Buffet tại khách sạn.
Ăn trưa, chiều: nhà hang địa phương tiêu chuẩn, hợp vệ sinh.Hướng dẫn viên: Đoàn có hướng dẫn viên tiếng Việt thuyết minh và phục vụ đoàn tham quan suốt tuyến.Bảo hiểm: Bảo hiểm du lịch theo quy định của Bảo hiểm Bảo Minh.Tham quan: Giá tour đã bao gồm phí vào cổng tại các điểm tham quan theo chương trình.Qùa tặng: Nón du lịch, nước, bao da hộ chiếu.
GIÁ KHÔNG BAO GỒM
Bồi dưỡng hướng dẫn viên và tài xế địa phương mức đề nghị : 3USD/ngày/khách=68.000 vnđ.Visa tái nhập vào Việt Nam cho người nước ngoài hoặc khách Việt Kiều 890.000vnđ/Visa nhận tại cửa khẩu và có giá trị vào Việt Nam 1 lần - trong 1 tháng đối với Việt kiều.
** Riêng quốc tịch Mỹ là 3.600.000 vnđ
% VATCác chi phí phát sinh ngoài chương trình như: điện thọai, giặt ủi, hành lý quá cước qui định, chi phí cá nhân khác, chi phí tham quan ngoài chương trình (nếu có và được nêu rõ trong chương trình).
GIÁ TOUR TRẺ EM: Tính theo ngày & tháng sinh
Từ 2 tuổi trở xuống : 35% Giá tour người lớnTừ 2 tuổi đến dưới 10 tuổi: 75% giá tour người lớnTừ 11 tuổi trở lên: bằng giá người lớn.( 100% ngủ theo tiêu chuẩn người lớn)
ĐIỀU KIỆN HỦY TOUR: (Không tính thứ bảy, chủ nhật và ngày lễ)
Đặt cọc 7.000.000 vnđ/khách ngay khi đăng ký tour.
Nếu hủy tour, Quý khách thanh toán các khoản lệ phí hủy tour sau :
Trước ngày đi 20 ngày: 30% giá tourTrước ngày đi 10 ngày: 50% giá tourTrước ngày đi 3-7 ngày: 70% giá tourTrước ngày đi 0-3 ngày: 100% giá tour
Trường hợp quý khách có yếu tố khách quan về cá nhân vui lòng báo sớm trước 5 ngày không tính thứ 7, chủ nhật & ngày lễ, để công ty hỗ trợ giải quyết.
* ĐIỀU KIỆN & HỘ CHIẾU KHI ĐĂNG KÝ TOUR
Hộ chiếu còn 6 tháng tính đến ngày đi tour về.Hộ chiếu đảm bảo các yếu tố sau: hình ảnh không bị hư hỏng, mờ nhòe, thông tin đầy đủ, dù còn hạn sử dụng nhưng nếu hình ảnh bị mờ nhòe, vẫn không được xuất hay nhập cảnh ....trường hợp vào ngày khởi hành. Quý khách không được xuất hoặc nhập cảnh trong nước và ngoài nước công ty sẽ không chịu trách nhiệm và không hoàn tiền các khoản chi phí liên quan khác.Khách quốc tịch nước ngoài/Việt kiều vui lòng kiểm tra visa của khách vào Việt Nam nhiều lần hay 1 lần, khách hàng làm visa tái nhập, ngày đi tour mang theo 2 tấm hình 4 x 6 và mang theo visa vào Việt Nam khi xuất ,nhập cảnhKhi đăng ký tour, vui lòng cung cấp thông tin cá nhân trên hộ chiếu: Họ & tên chính xác, ngày cấp, ngày hết hạn hộ chiếu, số điện thoại liên lạc ,địa chỉ liên lạc,...
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Tour du lịch Tết 2017: Thái Lan 5 ngày 4 đêm
                                Tour du lịch Tết 2017: Khám phá xứ sở Chùa Vàng Thái Lan
                                Tour du lịch Tết 2017: Du ngoạn Phú Quốc 3 ngày 3 đêm
                                Tour du lịch Tết 2017: Buôn Ma Thuột – Kon Tum – Pleiku
                                Tour du lịch Tết 2017: Điệp Sơn – Phú Yên 3 ngày 3 đêm
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
