url:http://dulich.vnexpress.net/tin-tuc/viet-nam/thu-tuong-chi-ra-3-giai-phap-de-phat-trien-du-lich-3500209.html?utm_campaign=boxtracking&utm_medium=box_mostview&utm_source=detail
Tittle :Thủ tướng chỉ ra 3 giải pháp để phát triển du lịch - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
                Các địa danh khác
		        24h qua
            Du lịch Việt Nam 
                                        Du Lịch 
                                        Việt Nam 
                                        Các địa danh
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Thứ năm, 17/11/2016 | 10:18 GMT+7
|
						Thủ tướng chỉ ra 3 giải pháp để phát triển du lịch												
					Xây dựng cộng đồng văn minh làm du lịch, thương hiệu du lịch lớn ở các vùng và thể chế tốt là 3 giải pháp được Thủ tướng Nguyễn Xuân Phúc chỉ ra trong phiên chất vấn sáng nay.
					10 nỗi sợ hãi thường gặp khi du lịch  /  Phó thủ tướng yêu cầu xóa 7 nỗi sợ của du khách khi đến Việt Nam                    
	Trong phiên chất vấn Thủ tướng sáng 17/11, một số đại biểu Quốc hội đặt câu hỏi về tiềm năng du lịch của nước ta lớn nhưng khai thác thế nào cho hiệu quả, giải pháp đột phá để phát triển ngành du lịch trong thời gian tới.
	Theo Thủ tướng, muốn du lịch trở thành mũi nhọn thì phải đóng góp từ 7,5 đến 10% GDP. Mặc dù du lịch có tiềm năng lớn, lượng khách quốc tế trong năm nay tăng nhanh, 25% so với 2015 nhưng tổng số vẫn còn rất thấp so với một số nước trong khu vực, đóng góp của du lịch vẫn chưa đạt 7,5% GPD.
					Thủ tướng Nguyễn Xuân Phúc trả lời chất vấn sáng 17/11.
	Để du lịch trở thành ngành kinh tế mũi nhọn, Thủ tướng cho biết Chính phủ đã triển khai Hội nghị toàn quốc về vấn đề này. Trước hết phải có cộng đồng văn minh làm du lịch, tức một cộng đồng không có ăn xin, ăn mày, chèo kéo khách, dù có thể cơ sở hạ tầng còn nghèo. Tiếp đến là xây dựng thương hiệu du lịch lớn ở các vùng và cuối cùng là phải có thể chế tốt, ưu tiên phát triển du lịch. Đồng thời đẩy mạnh công tác xúc tiến, quảng bá, dành điều kiện, cơ sở hạ tầng cần thiết để phát triển du lịch.
	Thủ tướng cũng dẫn chứng nghị quyết thí điểm làm visa điện tử cho khách du lịch đã được Chính phủ trình Quốc hội thông qua.
	"Với giải pháp đồng bộ như vậy du lịch Việt Nam chắc chắn sẽ cất cánh trong thời gian tới để xứng đáng là kinh tế mũi nhọn. Trong đó chúng tôi đặt vấn đề rất mạnh mẽ đó là nhân lực ngành du lịch, đáp ứng các nhu cầu cần thiết trong điều kiện khách quốc tế đa dạng như hiện nay", Thủ tướng nhấn mạnh.
	Trong 10 tháng đầu năm, lượng khách quốc tế đến Việt Nam đạt hơn 8 triệu lượt, tăng 25% so với cùng kỳ và bằng cả năm 2015. Mục tiêu của ngành du lịch là tiếp tục duy trì tốc độ tăng trưởng về khách du lịch quốc tế và nội địa trong những tháng cuối năm. Dự kiến, Việt Nam sẽ đón 9,6 triệu lượt khách quốc tế và 60 triệu khách nội địa trong năm nay.
	Xem thêm: Tổng cục Du lịch: Khách Tây Âu tăng trưởng chưa từng có                        
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
                                                Khách sạn509Nhà hàng63Tham quan365Giải trí82Mua sắm27                        
				Tin liên quan
							Phó thủ tướng: 'Du lịch và thể thao là nhịp cầu nối các dân tộc'  (25/9/2016)
							Cô gái bị fan cuồng sao y bản chính khi đi du lịch  (16/11/2016)
							Myanmar cấm dân địa phương cho khách du lịch thuê nhà  (30/9/2016)
							Người Trung Quốc thường mang mì tôm khi đi du lịch  (4/10/2016)
            Xem nhiều nhất
                            Anh Tây đi bộ khắp thế giới mê mẩn núi rừng Việt Nam
                                                             
                                Việt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật Bản
                                                                     
                                Những món đồ không thể thiếu khi đi du lịch cuối năm
                                Vị đậm đà tinh tế trong món gà sốt tiêu tỏi
                                Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịch
                                                                     
                 
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                Tags
                                    Chất vấn
                                Quốc hội
                                Thủ tướng
                                Du lịch
                                Kinh tế mũi nhọn
    Tin Khác
                                                     
                            Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                                                             
                            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịch
                                                             
                            Việt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật Bản
                                                             
                            Không gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
                                    Đầm Sen ưu đãi ngày Nhà giáo Việt Nam
                                    Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                                                             
                                    Hoàng tử Anh ăn đêm bên Hồ Gươm
                                    Những món đồ không thể thiếu khi đi du lịch cuối năm
                                    Khu cắm trại giữa rừng dương cách Sài Gòn 3 giờ chạy xe
                                                                             
                                    'Vua đầu bếp' để khách tự sáng tạo món ăn
                                                                             
                                    Anh Tây đi bộ khắp thế giới mê mẩn núi rừng Việt Nam
                                                                             
                                    Vị đậm đà tinh tế trong món gà sốt tiêu tỏi
                                    Món ăn đặc trưng trong ngày lễ Tạ ơn
                                    Những quán ăn không biển hiệu vẫn đông khách ở Hà Nội
                                                                             
        Khuyến mãi Du lịch
		  prev
		   next
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
         Gửi bài viết về tòa soạn
        Ẩm thực
         
                             
            Quán bún măng vịt mỗi ngày chỉ bán một tiếng
            Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
                Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
                'Vua đầu bếp' để khách tự sáng tạo món ăn
        Cộng đồng
         
                'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                        Hãy lên kế hoạch du lịch đúng thời điểm cho năm mới dựa theo lịch trình du lịch khắp miền đất nước.
            Du khách đứng tim vì voi chặn đầu xe5 vùng đất Samurai nổi tiếng của Nhật BảnĐồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
        Ảnh
         
                             
            Chùa 've chai' nắm giữ nhiều kỷ lục ở Đà Lạt
            Tên gọi “ve chai” của chùa Linh Phước (Đà Lạt) xuất phát từ việc hầu hết chi tiết trang trí ở đây đều dùng mảnh sành, sứ hay vỏ chai tạo nên.
                                     
                Ngày Tết du ngoạn Thiền viện Trúc Lâm Tây Thiên
                                     
                Những ngôi nhà mái rạ ở 'thủ phủ' gạo tám
        Video
         
            Du khách la hét vì chơi tàu lượn dốc nhất thế giới
            Tàu lượn Takabisha (Nhật Bản) có độ dốc lên tới 121 độ, vận tốc tối đa đạt 160 km/h khiến người chơi như đứng tim khi nhiều đường ray thẳng đứng, tàu như rơi tự do.
                Cả công viên 'nín thở' chờ du khách béo trượt máng
                Cảnh đẹp ngoạn mục trên khắp nẻo đường Na Uy
        Điểm đến yêu thích                
                    Kế hoạch 4 ngày khám phá đảo thiên đường ở Campuchia                
                    Những món ngon nên thử của Phú Quốc                
                    Khách du lịch được mời dùng toilet miễn phí ở Đà Nẵng                
                    Chuyến bay Cần Thơ - Đà Lạt chỉ mất một giờ                
                    Những cây cầu có tên gọi độc đáo ở miền Tây                
		Liên kết du lịch
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
