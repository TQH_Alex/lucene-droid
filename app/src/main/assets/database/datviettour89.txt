url:http://datviettour.com.vn/cong-ty-ky-nghe-soi-tham-quan-nha-trang-thang-112016-55331.html
Tittle :Khách Hàng Thân Thiết | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Khách Hàng Thân Thiết								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Công ty Kỹ Nghệ Sói tham quan Nha Trang, tháng 11/2016                    
					Nha Trang không chỉ sở hữu những hòn đảo đẹp, hoang sơ, bãi tắm trải dài mà còn có vô số dịch vụ giải trí thú vị. Một chuyến du lịch nghỉ dưỡng tại Nha Trang sẽ là hành trình khó quên với du khách. Ngày 11/11 – 13/11/2016, cán bộ công nhân viên công ty TNHH Kỹ Nghệ Sói đã có chuyến du lịch Nha Trang trọn vẹn niềm vui.
Đêm ngày 10/11/2016, xe và hướng dẫn viên Đất Việt Tour đã có mặt tại điểm hẹn, đón đoàn khởi hành đi Nha Trang. Đoàn nghỉ ngơi, thư giãn trên xe.
Sáng ngày đầu tiên có mặt tại xứ sở Trầm Hương, đoàn di chuyển đi dùng cơm sáng. Sau đó, xe đưa đoàn tới khu du lịch Long Phú lên tàu tham quan Hòn Hèo – Suối Hoa Lan. Tại đây, đoàn được khám phá Mê Cung Trận Đồ với những đường đi lắt léo giữa rừng dương xanh thẳm, tắm biển tại bãi biển An Bình, thư giãn cùng chương trình xiếc voi, gấu đặc sắc.
11h00, đoàn ăn trưa tại nhà hàng Hương Lan và ghé thăm đảo Khỉ - khu du lịch sinh thái nổi tiếng ở Nha Trang.
Đoàn tự do tham quan các công trình nghệ thuật ấn tượng như: Vườn Mỹ Nhân Ngư, vườn Thiên Long, Tây Du Ký,...Tàu đưa quý khách trở lại cảng Long Phú, trở về Nha Trang nhận phòng khách sạn nghỉ ngơi.
16h00, đoàn thư giãn tại khu du lịch I – Resort với dịch vụ tắm bùn khoáng nóng, rất tốt cho sức khỏe. Buổi tối, đoàn đến nhà hàng Đèn Lồng Đỏ dùng tiệc gala ấm cúng. Kết thúc chương trình gala, đoàn tự do tản bộ trên đường Trần phú để đến với chợ đêm Nha Trang, mua sắm các món đồ lưu niệm và thưởng thức kem Bốn Mùa.
Ngày thứ hai, đoàn dùng buffet sáng. Xe đưa đoàn khởi hành đi tham quan, tìm hiểu và mua sắm đặc sản yến sào tại nhà yến Nha Trang.
Tiếp tục hành trình, đoàn đến với cảng Vinpearl, trải nghiệm cùng hệ thống cáp treo vượt biển dài nhất thế giới đến với KDL Vinpearl Land. Tại khu du lịch này, đoàn được tham gia các trò chơi trong nhà như: Xe điện đụng, games mini trong nhà, hát karaoke, xem Phim 4D,… và chinh phục các trò chơi ngoài trời như: Đu quay nhào lộn, Tàu lượn siêu tốc, Thuyền hải tặc, thư giãn tại Công Viên Nước,...
Tiếp tục hành trình, đoàn tham quan tại thủy cung Vinpearl Land – nơi có mặt hầu như đầy đủ tất cả các loại sinh vật biển lạ mắt.
Sau khi thưởng thức màn trình diễn ánh sáng tại khu du lịch này, đoàn đi cáp treo về lại đất liền. Xe đưa doàn về lại khách sạn nghỉ ngơi. Buổi tối, đoàn tự do sinh hoạt, dạo biển, thưởng thức kem Bốn Mùa và nghỉ ngơi.
Ngày thứ ba, đoàn dùng điểm tâm sáng, làm thủ tục trả phòng. Xe đưa đoàn mua sắm đặc sản tại TTTM Chợ Đầm.
08h00, đoàn khởi hành về lại Tp.HCM. Trên đường về, đoàn đến Phan Rang mua đặc sản nho, rượu nho,…về làm quà tặng bạn bè người thân.
11h00, đoàn dùng cơm trưa tại nhà hàng và về lại điểm đón ban đầu, kết thúc chuyến tham quan. Hướng dẫn viên chia tay, chúc sức khỏe và hẹn ngày gặp lại đoàn trong những chương trình tham quan sau.
Xem thêm hình ảnh đoàn công ty Kỹ Nghệ Sói tham quan Nha Trang, tháng 11/2016 tại đây: https://goo.gl/IVmnru                
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Khách sạn Majestic tham quan Phú Quốc, tháng 4/2016
                                Công ty TNHH khai thác thủy lợi Dầu Tiếng – Phước Hòa tham quan Vũng Tàu, tháng 03/2014
                                Công ty Phúc Thắng tham quan Vũng Tàu, tháng 5/2016
                                Trung tâm kinh doanh VNPT TP. HCM tham quan Vũng Tàu, tháng 09/2014
                                Công ty Hamyco tham quan Phan Thiết, tháng 9/2013
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
