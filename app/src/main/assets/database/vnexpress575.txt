url:http://dulich.vnexpress.net/photo/anh-video/nha-trinh-tuong-hang-tram-nam-khong-doi-o-lang-son-3459001.html?utm_campaign=boxtracking&utm_medium=box_topic&utm_source=detail
Tittle :Nhà trình tường hàng trăm năm không đổi ở Lạng Sơn - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
		        24h qua
            Du lịch Ảnh 
                                        Du Lịch 
                                        Ảnh 
                                 
                                Việt NamQuốc tếCộng đồngẢnh
                            Thứ ba, 30/8/2016 | 02:08 GMT+7                        
|
                        Nhà trình tường hàng trăm năm không đổi ở Lạng Sơn                                                English
                    Nhà trình tường của người Tày - Nùng ở Lạng Sơn vừa giữ ấm vào mùa đông mà vẫn mát mẻ vào mùa hè.
                                Dân tộc Chăm đẹp bình dị trên báo nước ngoài
                                Những ngôi mộ đá của người dân tộc Chăm
                                             
	Dọc con đường quốc lộ ở Lạng Sơn, du khách dễ bắt gặp những nếp nhà mái ngói âm dương gợi lên nét bình dị vốn có của hàng chục năm trước.
                                                                                 
                                             
	Đồng bào người Tày - Nùng ở vùng biên giới tỉnh Lạng Sơn đã tạo ra những ngôi nhà có tường làm bằng đất sét, đất nện trộn đều vào các khuôn gỗ, dùng chày vồ đập cho các loại đất dính chặt vào nhau, tường dày 50 – 70 cm rất chắc chắn. Công đoạn dựng một ngôi nhà mất nhiều thời gian và sức lực. Nhờ đó, nhà trình tường rất kiên cố, một số có tuổi thọ hàng trăm năm tuổi.
                                                                                 
                                             
	Theo phong thủy của người Tày - Nùng, nhà ở phải theo hướng Nam, có không gian thoáng đãng, gần gũi với thiên nhiên. Tại Bản Khiếng, cửa khẩu Chi Ma, huyện Lộc Bình, nhà trình tường được xây hai tầng, có hàng rào bằng đá bao quanh.
                                                                                 
                                             
	Nhìn từ bên ngoài, các cửa đối xứng nhau. Cửa chính có treo tấm bùa trừ tà hoặc gương bát quát theo phong tục truyền thống. Đặc biệt, những ngôi nhà cổ có đến 10 cửa sổ mang nét đặc trưng riêng về kết cấu ít nơi nào có được.
                                                                                 
                                             
	Tùy từng ngôi nhà sẽ có một cầu thang gỗ dẫn lên tầng trên. Thông thường bàn thờ tổ tiên được đặt ở nơi trang trọng, tầng trên hoặc ở vị trí trung tâm ngôi nhà, với bức "phùng slằn" viết bằng chữ Hán cho biết dòng họ.
                                                                                 
                                             
	Bên trong, 4 góc có 4 cây gỗ to chịu lực cho toàn bộ gian nhà. Trong suốt quá trình dựng nhà, người Tày - Nùng đều tính toán kỹ lưỡng việc lựa chọn loại gỗ tốt, nên hệ thống xà ngang, xà dọc rất kiên cố, không bị mục nát. Ngày nay, nhiều gia đình thay màu tường nhà bằng vôi trắng.
                                                                                 
                                             
	Nhà trình tường còn đẹp bởi những viên ngói âm dương phủ màu rêu phong.
                                                                                 
                                             
	Nếu đến Lạng sơn, du khách có thể ghé các thôn bản vùng cao giáp biên như: Cao Lộc, Lộc Bình, Đình Lập để được chiêm ngưỡng vẻ đẹp cổ kính của những ngôi nhà trình tường thuở xưa. Cao Lộc được xem là nơi giữ những nếp nhà còn nguyên vẹn về kết cấu, kiểu dáng của hàng trăm năm trước mà không bị pha tạp.
                                                                                 
                                             
	Không sặc sỡ như các dân tộc khác, người Tày - Nùng giản dị với màu vải thô nhuộm chàm. Nam giới mặc áo cổ đứng, xẻ ngực có hàng cúc vải và hai túi. Phụ nữ mặc áo năm thân, cài cúc bên nách phải, cổ tay áo thường đính thêm miếng vải khác màu. Ở tuổi trưởng thành người phụ nữ thường bịt một chiếc răng bằng vàng ở hàm trên với quan niệm thể hiện sự sang trọng, nụ cười thêm cuốn hút.
                                                                                 
                                             
	Những năm gần đây, các ngôi nhà trình tường của đồng bào Tày - Nùng được rất nhiều khách du lịch, nhiếp ảnh gia tìm đến tham quan, chụp ảnh. Dạo quanh những ngôi nhà này, du khách sẽ cảm thấy tâm hồn nhẹ bẫng bởi khung cảnh cổ kính, cùng nét văn hóa mang giá trị bản sắc dân tộc từ đời sống sinh hoạt của đồng bào nơi đây.
                                                                                 
	Dương Thanh
	Ảnh: Lưu Minh Dân
                Những kiến trúc độc đáo trên thế giới
                    Bên trong dinh thự 100 triệu USD của Donald Trump ở New York (13/11)
                                             
                    7 dinh thự của vua Bảo Đại dọc miền đất nước (29/8)
                                             
                    Tòa nhà trăm mái ở Hà Nội (26/8)
                                             
                    Cây cầu xoắn kép phục vụ Olympic Bắc Kinh 2022 (20/8)
                                             
                    Du khách đổ xô đến đường hầm bí mật ở Gaza (26/7)
                                             
                Xem thêm
                         
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                        Tags
                                                    Nhà trình tường
                                                    Lạng Sơn
                                                    Biên giới
                                                    Du lịch Lạng Sơn
                                                    Du khách
                                                    Dân tộc
                                                    Người Tày
    Tin Khác
                                                     
                            Diễm My ăn trưa, trồng rau cùng người dân Hội An
                                                             
                                                     
                            Khu ổ chuột đầy rẫy tội phạm một thời ở Hong Kong
                                                             
                                                     
                            Góc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban Nha
                                                             
                            Những bữa tiệc 'nóng mắt' với bong bóng xà phòng
                                                             
                                    Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                                                             
                                    Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
                                                                             
                                    'Vườn thú người' - vết nhơ trong lịch sử châu Âu
                                                                             
                                    Vẻ đẹp của thị trấn nơi William và Kate gặp nhau
                                                                             
                                    Khu cắm trại giữa rừng dương cách Sài Gòn 3 giờ chạy xe
                                                                             
                                    Vẻ đẹp thơ mộng mùa lá đỏ Nhật Bản
                                                                             
                                    Bên trong thị trấn ma lớn nhất thế giới ở quê hương Thành Cát Tư Hãn
                                                                             
                                    Cô gái bị fan cuồng sao y bản chính khi đi du lịch
                                                                             
                                    Quang Vinh khám phá hai mặt đối lập của Sài Gòn
                                                                             
                                    Bảo tàng áo dài tư nhân duy nhất ở Sài Gòn
                                                                             
        Khuyến mãi Du lịch
		  prev
		   next
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
        Xem nhiều nhất
    Góc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban NhaCặp du khách Trung Quốc quan hệ tình dục trong công viênCả công viên 'nín thở' chờ du khách béo trượt mángBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonChàng tiên cá có thật giữa đời thường
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
