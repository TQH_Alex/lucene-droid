url:http://dulich.vnexpress.net/tin-tuc/cong-dong/dau-chan/loi-tam-su-cua-nguoi-lam-nghe-gac-cua-khach-san-3423027.html?utm_campaign=boxtracking&utm_medium=box_tinkhac&utm_source=detail
Tittle :Lời tâm sự của người làm nghề gác cửa khách sạn - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
		        24h qua
            Du lịch Cộng đồng 
                        Dấu chânTư vấnHỏi - Đáp
                                        Du Lịch 
                                        Dấu chân 
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Thứ hai, 20/6/2016 | 20:00 GMT+7
|
						Lời tâm sự của người làm nghề gác cửa khách sạn												
					Doorman là từ để chỉ người làm nghề gác cửa ở khách sạn hạng sang hoặc những tòa nhà lớn. Nhiều người cho đây là công việc nhàm chán nhưng ít ai biết rằng chính họ sẽ đem lại cho khách hàng sự thoải mái ngay từ những phút đầu tiên.
					Nhân viên khách sạn khoanh tay nhìn khách hàng bị bóp cổ  /  Những vị khách khiến nhân viên khách sạn nhớ mãi                    
	Doorman là từ để chỉ người làm nghề gác cửa ở khách sạn hạng sang hoặc những tòa nhà lớn. Công việc của một người gác cửa không “hấp dẫn”, thậm chí còn bị đánh giá là “nhàm chán”. Nhưng ít ai biết được họ chính là nhân tố quan trọng đem lại cho khách hàng sự thoải mái ngay từ những phút đầu tiên.
	Ông Jais Adbul Rahman đã làm việc tại Grand Hyatt Singapore từ năm 1972 và hiện quản lý khu tiền sảnh. “Nhiều người hỏi tôi có thích công việc này không, nhưng nói thật tôi chưa bao giờ chán hay đánh giá thấp nó”, ông nói. "Để làm công việc này, bạn cần ba yếu tố: thân thiện, lịch sự và tốt bụng. Bạn sẽ luôn chào đón khách với nụ cười trên môi cùng những câu nói quen thuộc: Chào buổi sáng, xin chào, chào buổi tối. Bạn giúp đưa ra những lời cảnh báo: Xin ngài hãy mang theo ô, trời có thể sẽ mưa ngày hôm nay. Chỉ là những hành động rất nhỏ, nhưng sẽ quyết định ấn tượng của khách hàng đối với nơi họ nghỉ lại".
					Ông Rahman mở cửa xe đón khách. Ảnh: Lady Ironchef.
	Tại nơi ông làm việc, mỗi ca trực sẽ có hai nhân viên gác cửa luôn duy trì sự có mặt để du khách cảm thấy được hoan nghênh. Khi khách đến, người gác cửa sẽ giúp mở cửa xe và mang hành lý. Họ cũng phải làm việc như người điều tiết giao thông để tránh tắc nghẽn bên ngoài sảnh hoặc vẫy taxi khi khách có nhu cầu rời đi.
	“Đừng nghĩ công việc này chỉ đơn thuần đóng, mở cửa. Chúng tôi còn là nhân viên tư vấn, bác sĩ, người giữ trẻ, dự báo thời tiết, nhân viên kỹ thuật, hoặc rất nhiều việc không tên khác. Khách muốn hỏi đường? Chúng tôi biết. Khách muốn sửa vật dụng? Chúng tôi cũng biết. Và bạn có thể hỏi chúng tôi vào bất cứ thời gian nào, vì chúng tôi luôn đứng đó cả đêm”, Rahman chia sẻ.
					“Lời cảm ơn của khách làm nên một ngày của chúng tôi”. Ảnh: Lady Ironchef.
	Theo nhu cầu tăng cao của khách sạn, số lượng người gác cửa cũng tăng gấp đôi. Họ được yêu cầu phải có kiến thức căn bản về khách sạn và địa phương để cho khách hàng những lời khuyên hữu ích. Thông thường, du khách có xu hướng hỏi người gác cửa về các địa điểm cụ thể, nơi nên thăm quan, dịch vụ vui chơi hay ăn uống. Vì thế, người gác cửa giống như cuốn bách khoa toàn thư hay cuốn sách hướng dẫn bỏ túi để giúp đỡ khách hàng trong thời điểm cần thiết. Đây là yêu cầu cao, cũng là thách thức đối với công việc này. Đôi khi, họ cũng phải đi hàng cây số để làm những việc khách yêu cầu như mua thẻ điện thoại, tìm bến xe buýt hoặc chỗ trú ẩn cho khách khi trời mưa.
	Khi được hỏi điều gì khiến ông Rahman cảm thấy vui nhất, ông trả lời: “Đó chính là lời cảm ơn của khách hàng. Sự cảm ơn chân thành từ phía họ làm nên một ngày của chúng tôi”.
	Xem thêm: Ngôn ngữ bí mật chỉ nhân viên khách sạn mới hiểu                         
                                    Hải Thu
                                                                              |  
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
                Chân dung người làm du lịch
                    Cuộc sống phu khuân vác trên nóc nhà châu Phi được hé lộ (17/9)
                                             
                    Cụ ông 86 tuổi dịch thư ở bưu điện Sài Gòn (18/8)
                                             
                    Người chơi Pokemon Go trở thành hướng dẫn viên du lịch (11/8)
                                             
                    Quán ăn một giá của Tuấn Thăng và hoa hậu tài năng (4/8)
                                             
                    Chuyện về người hầu rượu không biết tiếng Nhật ở Tokyo (27/7)
                                             
                Xem thêm
            Xem nhiều nhất
                            'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                                                             
                                Góc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban Nha
                                                                     
                                Cặp du khách Trung Quốc quan hệ tình dục trong công viên
                                                                     
                                Cả công viên 'nín thở' chờ du khách béo trượt máng
                                                                     
                                Bí mật đáng sợ bên dưới hệ thống tàu điện ngầm London
                                                                     
                 
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                Tags
                                    Khách sạn
                                Doorman
                                Du lịch
                                Mở cửa
                                Lời khuyên
                                Du khách
    Tin Khác
                                                     
                            Diễm My ăn trưa, trồng rau cùng người dân Hội An
                                                             
                            Du khách đứng tim vì voi chặn đầu xe
                                                             
                                                     
                            Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                                             
                            Kỷ niệm hài hước với toilet trên đường du lịch
                                                             
                                    Hành khách phát hiện gián trong bữa ăn chay trên máy bay
                                                                             
                                    Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
                                                                             
                                    Du khách mỏi tay câu cá khổng lồ tại Thái Lan
                                                                             
                                    Cả công viên 'nín thở' chờ du khách béo trượt máng
                                                                             
                                    Cặp du khách Trung Quốc quan hệ tình dục trong công viên
                                                                             
                                    Quang Vinh khám phá hai mặt đối lập của Sài Gòn
                                                                             
                                    Khách Tây nói người Việt bấm còi xe như chơi nhạc
                                                                             
                                    Giây phút kinh hoàng của chàng trai thoát chết trong lễ hội 'bò đuổi'
                                                                             
                                    Cách tiêu tiền của khách Trung Quốc và sự khác biệt về đẳng cấp
                                                                             
                                    Hòn đảo chỉ phục vụ khách VIP ở Philippines
                                                                             
        Khuyến mãi Du lịch
		  prev
		   next
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
        Việt Nam
         
                Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                        Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịchViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnKhông gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
         Gửi bài viết về tòa soạn
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
        Ảnh
         
                             
            Diễm My ăn trưa, trồng rau cùng người dân Hội An
            Diễn viên Diễm My đã có dịp dùng bữa cùng một cặp vợ chồng ở làng Trà Quế, Hội An, trước khi thực hiện bộ ảnh "Vẻ đẹp không tuổi" cùng nhiếp ảnh gia Pháp.
                                     
                Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                     
                Quang Vinh khám phá hai mặt đối lập của Sài Gòn
        Video
         
            Du khách đứng tim vì voi chặn đầu xe
            Đoàn du khách được một phen hú hồn vì gặp đàn voi khi thăm một khu bảo tồn thiên nhiên tại châu Phi.
                Du khách la hét vì chơi tàu lượn dốc nhất thế giới
                Cả công viên 'nín thở' chờ du khách béo trượt máng
        Bình luận hay
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
