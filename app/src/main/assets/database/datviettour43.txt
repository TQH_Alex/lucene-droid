url:http://datviettour.com.vn/tour-du-lich-bien-phan-thiet-mui-ne-2-ngay-1-dem-1542.html
Tittle :Tour Du Lịch Hè 2016 | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Tour du lịch hè 2016								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Tour du lịch biển Phan Thiết – Mũi Né 2 ngày 1 đêm                    
					Hãy đến với vùng đất đầy nắng và gió cùng với Tour du lịch Phan Thiết – Mũi Né để thưởng thức những món ngon miền biển và tận hưởng những phút giây thú vị!
ngày 01: SÀI GÒN - THÀNH PHỐ BIỂN PHAN THIẾT
Xe và Hướng dẫn viên của Du lịch Đất Việt đón quý khách tại điểm hẹn khởi hành đi Phan Thiết. Đoàn nghỉ ngơi, tham quan và dùng điễm tâm sáng tại nhà hàng Hưng Phát (khu vực Dầu Giây). Trên đường đi, quý khách cùng với hướng dẫn viên công ty tham gia các trò chơi hoạt náo vui nhộn như: "Đi tìm ẩn số, Truy tìm báu vật, Địa danh còn lại, Hành trình Đất Việt, Chiếc nón diệu kỳ"… Với các phần quà hấp dẫn.
Trên đường đi đoàn ghé tham quan chinh phục núi Tà Cú (quý khách có thể chiêm ngưỡng Tam Thế Phật, tượng phật nằm dài 49m cao 10m). Đoàn có thể đi cáp treo lên tham quan (tự túc).
Quý khách dùng cơm trưa tại nhà hàng dưới chân núi Tà Cú, xe đưa đoàn vào TP Phan Thiết, tiếp tục di chuyển đến Hòn Rơm – Mũi Né. Trên đường đi, Quý khách sẽ được chiêm ngưỡng Tháp chàm Poshanư, phế tích Lầu Ông Hoàng nơi đã tạo cảm hứng cho thi sỹ Hàn Mặc Tử viết lên những vần thơ trữ tình lãng mạng. Đoàn đến Resort nhận phòng nghỉ ngơi.
Xe đưa Quý khách tham quan và sử dụng dịch vụ tại Trung tâm bùn khoáng Mũi Né (tự túc), hoặc Quý khách sinh hoạt và tắm biển tự do.
Đoàn dùng cơm chiều tại nhà hàng. Quý khách giao lưu và có thể dạo biển về đêm. Nghỉ đêm, du lịch Mũi Né.
ngày 02: TẠM BIỆT THÀNH PHỐ BIỂN PHAN THIẾT
Quý khách khởi hành tham quan Đồi Cát Bay, tại đây đoàn có thể tham gia các trò chơi cảm giác mạnh trên đồi cát bằng cách đi máng trượt, và thưởng thức dừa ba nhát, sau đó xe đưa đoàn đến nhà hàng dùng điểm tâm. Quý khách trở về khách sạn sinh hoạt và tắm biển tự do, quý khách có thể thưởng thức hải sản tươi sống với giá cả rất bình dân như: Ghẹ, mực, sò, ốc các loại… (tự túc).
Đoàn làm thủ tục trả phòng. Xe đưa đoàn đến nhà hàng dùng cơm trưa. Sau đó khởi hành về lại TP.HCM, trên đường về đoàn ghé tham quan và mua sắm đặc sản Phan Thiết tại cơ sở sản xuất như: Bánh cốm, bánh rế, mực khô, cá khô các loại,….
Tiếp tục lộ trình. Xe đưa đoàn về điểm đón ban đầu, kết thúc chương trình tour, Du lịch Đất Việt chia tay quý khách và hẹn găp lại trong những chương trình tham quan sau.
Có gì hấp dẫn?
- Khởi hành sáng thứ 7 hàng tuần
- Tư vấn & giao vé miễn phí
- Cam kết dịch vụ chất lượng
- Quà tặng: Voucher, gối hơi du lịch, móc gắn chìa khóa...
Hotline tư vấn tour du lịch trong nước : 0909 897 499 – 0966 980 945
BẢNG GIÁ TOUR
Lượng kháchKhách sạn
2 saoKhách sạn
3 saoKhách sạn
4 saoNgày khởi hànhKhách Lẻ - Ghép Đoàn
(02 - 08 khách)980.0001.190.0001.520.000Thứ 7 hàng tuầnKhách Đoàn - Tour Riêng
(trên 40 khách)Mọi ngày
Theo yêu cầu
 
GIÁ VÉ BAO GỒM:
Vận chuyển:
- 16 chỗ MerSprinter
- 29 chỗ County
- 35 chỗ Aero Town
- 45 chỗ AeroSpace, Hiclass: Đưa đón, tham quan, tùy theo số lượng khách đăng ký để bố trí xe phù hợp, đưa đón suốt lộ trình… Xe đời mới, máy lạnh. Đạt tiêu chuẩn du lịch.Khách sạn:
- Khách sạn 2 sao: KDL Gành, Năm Châu,…
- Khách sạn 3 sao: Thảo Hà
- Resort 3 sao: Hải Âu, Thái Hòa
- Resort 4 sao: Sunny Beach, Lotus, Mui Ne Bay
- Resort 5 sao: Sealink- Tiện nghi trong phòng: Tivi, hệ thống nước nóng,... Vệ sinh khép kín.
- Phòng từ 02 - 03 khách, đầy đủ tiện nghi, gần biển.Ăn uống:
- Ăn sáng Buffet sáng.
- Ăn trưa, chiều: Gồm 6,7 món ngon, hợp vệ sinh 80.000đ/khách/buổi.Hướng dẫn viên: Đoàn có hướng dẫn viên thuyết minh và phục vụ ăn, nghỉ, tham quan cho Quý khách. Hoạt náo viên tổ chức trò chơi vận động tập thể, sinh hoạt, ca hát.Bảo hiểm: Khách được bảo hiểm du lịch trọn gói, mức bồi thường tối đa 20.000.000đ/vụ. Thuốc y tế thông thường.Qùa tặng: Mỗi vị khách trên đường đi được phục vụ nón du lịch, khăn lạnh, 02 chai nước tinh khiết 0.5l/ngày/người và xổ số vui có thưởng.Tham quan: Giá vé đã bao gồm phí vào cổng tại các điểm tham quan.
GIÁ VÉ KHÔNG BAO GỒM:
Chi phí tham quam tắm bùn (120.000 =>150.000/khách).Vé cáp treo tham quan núi Tà Cú (90.000/người/khứ hồi).Thuế VAT 10%.Các chi phí cá nhân khác ngoài chương trình (ăn hải sản, tắm bùn, giặt ủi, điện thoại, thức uống trong minibar).
GIÁ VÉ TRẺ EM:
Từ 1 - 5 tuổi miễn phí giá tour (2 người lớn chỉ kèm 1 trẻ miễn phí). Từ 6 đến 11 tuổi tính 50% giá vé tour (ngủ ghép cùng gia đình). Từ 12 tuổi trở lên tính như người lớn.                
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Tour du lịch Cần Thơ – Châu Đốc – Hà Tiên 4 ngày 3 đêm
                                Tour du lịch: Du ngoạn đến DuBai
                                Tour du lịch lễ 8/3: Phan Thiết biển xanh
                                Tour du lịch Nha Trang 2 ngày 3 đêm, giá hấp dẫn
                                Tour du lịch Hà Nội – Hòa Bình – Điện Biên Phủ – Sapa
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
