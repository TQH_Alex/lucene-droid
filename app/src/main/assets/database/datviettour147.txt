url:http://datviettour.com.vn/kham-pha-song-nuoc-mien-tay-5-ngay-4-dem-1202.html
Tittle :Tour Du Lịch Trong Nước | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Tour du lịch trong nước								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Tour du lịch sông nước miền Tây – biển đảo Phú Quốc                    
Đến với Tour du lịch miền Tây sông nước - biển đảo Phú Quốc trong 5 ngày 4 đêm để trải nghiệm những điều mới lạ, thú vị, giá hấp dẫn!
NGÀY 01: TP.HCM – MỸ THO – CẦN THƠ
Du Lich Đất Việt, đón khách tại điểm hẹn. Đoàn khởi hành đi Mỹ Tho, cùng chơi các trò chơi vui nhộn sinh hoạt trên xe như: Truy Tìm Ẩn Số, Bí Mật Cuối Cùng, Nhân Tài Đất Việt, Thử Tài Đoán Vật… với những phần quà có nhiều ý nghĩa.
Quý khách dùng điểm tâm tại nhà hàng Trung Lương. Xe đưa đoàn vào trung tâm Thành phố tham quan chùa Vĩnh Tràng. Xe đưa quý khách xuống bến đò 30/04, xuống tàu du ngoạn sông Tiền, quý khách nghe giới thiệu về các cù lao Long - Lân - Quy - Phụng.
Đến Cồn Phụng, quý khách tản bộ tham quan Làng Thủ Công Mỹ Nghệ sản xuất các vật dụng từ cây dừa, tham quan “Giang sơn” của Ông Đạo Dừa - người sáng lập ra “Đạo Bất Tạo Con”.
Tiếp tục khởi hành đi Cồn Thới Sơn, quý khách tiếp tục tham quan Vườn hoa kiểng, Vườn trái cây, Tham quan Lò Nấu Rượu, Cơ Sở thiêu Thủ công , Lò Kẹo Dừa, Lò Bánh Phồng theo con đường làng vào nhà dân địa phương thưởng thức trái cây, trà hoặc rượu pha mật ong, đờn ca tài tử Nam Bộ.
Cùng Đất Việt Tour khám phá >>>> Du lịch miền Tây sông nước
Quý khách dùng cơm trưa tại Cồn Thới Sơn - thưởng thức các món đặc sản miền Tây. Quý khách đi thuyền chèo qua những con lạch nhỏ dười hai hàng dừa xanh mát trên sông, di chuyển ra thuyền lớn sang bờ Mỹ Tho.
Khởi hành đi Cần Thơ. Quý khách ngắm cảnh Cầu Mỹ Thuận . Đến Cần Thơ quý khách nhận phòng nghỉ ngơi tại khách sạn.
Dùng cơm tối, Tối quý khách tự do nghỉ ngơi dạo phố Tây Đô. Nghỉ đêm tại Cần Thơ.
NGÀY 02: CẦN THƠ – RẠCH GIÁ
Ăn sáng. Xe đưa quý khách ra Bến Ninh Kiều, du ngoạn trên Sông Cần Thơ tham quan Cảng Cá, tiếp tục đến tham quan Chợ nổi Cái Răng - tự do thưởng thức đặc sản trái cây Miền Tây (chi phí tự túc).
Quý khách tham quan Khu du lịch Mỹ Khánh, Nhà Cổ - thưởng thức đặc sản trái cây miệt vườn, đờn ca tài tử Nam Bộ. Làm thủ tục trả phòng , dùng cơm trưa khởi hành đi Rạch Giá. Tới Rạch Giá đoàn tham quan.
Đền Thờ Cụ Nguyễn Trung Trực, khu lấn biển, trung tâm thương mại Rạch Giá. Về khách sạn nhận phòng ăn tối. Tự do dạo phố biển Rạch Giá.
NGÀY 03: RẠCH GIÁ - PHÚ QUỐC
Quý khách dùng điểm tâm. Xe đưa đoàn ra bến tàu đi Phú Quốc - ngắm cảnh vùng biển Kiên Giang và Vịnh Thái Lan.
Đến An Thới - Du lịch Phú Quốc, xe đón đoàn và đưa về Thị trấn Dương Đông. Đoàn dùng cơm trưa, nhận phòng khách sạn, xe đón đoàn đi tham quan Vườn tiêu khu tượng (vườn tiêu lớn nhất phú quốc). Tham quan Chùa Sư Muôn hoặc Dinh Cậu (trung tâm sinh hoạt văn hoá tín ngưỡng của người dân Phú Quốc).
Về Dương Đông dùng cơm. Về nhận phòng khách sạn nghỉ ngơi.
Quý khách khởi hành đi Bắc Đảo, tham quan Vườn Tiêu Khu Tượng, xe tiếp tục đưa đoàn đến Bãi Biển Mũi Gành Dầu, ghé thăm quan và dâng hương tại Đền thờ Cụ Nguyễn Trung Trực . Đến bãi biển Gành Dầu đoàn nghỉ ngơi ngắm Hải phận Việt Nam Và Campuchia, thưởng thức dừa tươi, nghe chủ quán ca tài tử Nam bộ.
Dùng cơm tối, Quý khách sinh hoạt tự do hoặc đăng ký chương trình thẻ mực đêm(chi phí tự túc)
NGÀY 04: ĐẢO NGỌC PHÚ QUỐC
Quý khách dùng điểm tâm. Đoàn lên tàu đi câu cá, tham quan làng Chài Hàm Ninh, biển Hàm Ninh. Trên đường về ghé Suối Tranh (có thể tắm suối, thưởng thức cua, ghẹ,. . . tự túc), Tự do tắm biển tại Bãi Sao. Tham quan Dinh Cậu, bãi đá Gành Dầu. Quý khách dùng cơm trưa.
Trở về khách sạn, trên đường về ghé tham quan cơ sở nuôi cấy và sản xuất ngọc trai, tham quan cơ sở sản xuất nước mắm Phú Quốc nổi tiếng. Đoàn đi chợ Dương Đông mua đặc sản địa phương. Quý khách nghỉ ngơi.
Quý khách dùng cơm chiều. Quý khách tự do khám phá du lich Phu Quoc.
Xem Tour du lịch Phú Quốc giá tốt  => TẠI ĐÂY
NGÀY 05: PHÚ QUỐC – TP.HCM
Đoàn dùng điểm tâm sáng. Tự do tắm biển Làm thủ tục trả phòng. Xe đưa đoàn ra cảng An Thới khởi hành về lại Rạch Giá. Quý khách dùng cơm trưa. Khởi hành về Tp.HCM .
Về đến TP.HCM, HDV cty du lich Đất Việt chào tạm biệt khách và hẹn gạp lại.
 
 
Có gì hấp dẫn?
- Tư vấn nhiệt tình, miễn phí
- Lịch trình, ngày khởi hành theo yêu cầu
- Cam kết dịch vụ chất lượng cao
- Quà tặng: Voucher du lịch, nón, ly sứ, ba lô, quà tặng trò chơi
- Du lịch Teambuilding: vận động trên biển, gala dinner
Hotline: 0976 046 046 – 0989 120 120
BẢNG GIÁ TOUR
Lượng kháchKhách sạn
2 saoKhách sạn
3 saoKhách sạn
4 saoNgày khởi hànhKhách Lẻ - Ghép Đoàn
(02 - 08 khách)Thứ 3, 5, 7
hàng tuầnKhách Đoàn - Tour Riêng
(trên 40 khách)4.360.000 Mọi ngày
Theo yêu cầu
 
GIÁ VÉ BAO GỒM:
Phương tiện: Xe Aero Space (45 chỗ) đời mới, ti vi, băng đôi, ghế bật, máy lạnh. Đạt tiêu chuẩn du lịchKhách sạn: Tiêu chuẩn Khách sạn 2 sao trung tâm thành phố. Tiện nghi trong phòng: máy lạnh, tivi, điện thoại, hệ thống nước nóng, vệ sinh khép kín Phòng từ 2 –3 khách.Ăn uống: Ăn sáng: hủ tíu, bánh mì ôpla, . . . giải khát cafê, nước ngọt, . .
Ăn trưa, chiều: gồm 06 ->07 món ngon, hợp vệ sinh (Có đặc sản địa phương). Đặc biệt có một buổi tiệc liên hoan.
Ăn trưa, chiều: 06 – 07 món ngon, hợp vệ sinh
Đặc biệt : có 01 bữa tiệc liên hoan đoàn.Qùa tặng: Mỗi vị khách trên đường đi được phục vụ nón du lịch, khăn lạnh, 02 chai nước tinh khiết 0.5l / ngày / người.
Thẻ VIP tặng trưởng đoàn, phó đoàn và nhiều phần quà ý nghĩa tặng cho đoàn.Tham quan: Giá vé đã bao gồm phí vào cổng tại các điểm tham quan.
Vé tàu khứ hồi Rạch Giá Phú Quốc.
GIÁ VÉ KHÔNG BAO GỒM:
Thuế VAT 10%Các chi phí cá nhân khác ngoài chương trình (ăn hải sản, giặt ủi, điện thoại, thức uống trong minibar…)Tiền Típ cho tài xế và HDV (nếu có)
GIÁ VÉ TRẺ EM:
Từ 1t-5t miễn phí (hai người lớn chỉ kèm 1 trẻ miễn phí).Từ 6 đến 11 tính 60% giá vé (ngủ ghép cùng gia đình).
Bạn đọc nên xem
>>>  5 hoạt động cực thú vị, hấp dẫn chỉ có khi du lịch Đà Lạt
>>> Những đặc sản biển thơm ngon níu chân khách du lịch Phan Thiết
 
 
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Tour du lịch Bình Châu – Hồ Cốc – Vũng Tàu
                                Tour du lịch: Bình Châu – Hồ Tràm, giá ưu đãi!
                                Tour du lịch Hà Nội – Hải Phòng – Hạ Long – Yên Tử – Team building
                                Tour Du Lịch: Côn Đảo Huyền Thoại 2 Ngày 1 Đêm
                                Tour du lịch Madagui 2 ngày 1 đêm
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
