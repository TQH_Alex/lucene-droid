url:http://datviettour.com.vn/dich-vu-visa-nhat-ban-3601.html
Tittle :Dịch Vụ Visa – Passport | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Dịch vụ Visa – Passport								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Dịch vụ Visa Nhật Bản                    
 I. Visa ngắn hạn (thăm thân…gia đình, họ hàng: có quan hệ 3 đời)
Tài liệu người xin Visa chuẩn bị
(1) Hộ chiếu gốc còn hạn trên 6 tháng
(2) Tờ khai xin cấp Visa 1 tờ ( Có mẫu khi liên hệ trực tiếp)
(3) 01ảnh 4,5cm x 4,5cm
(4)Tài liệu chứng minh mối quan hệ họ hàng:
+ Giấy khai sinh
+ Giấy chứng nhận kết hôn
+ Bản sao hộ khẩu
(5)Tài liệu chứng minh khả năng chi trả kinh phí cho chuyến đi:
+ Giấy chứng nhận thu nhập do cơ quan có thẩm quyền cấp
+ Giấy chứng nhận số dư tiền gửi ngân hàng
Tài liệu phía Nhật Bản chuẩn bị
(1) Giấy lý do mời ( Có mẫu khi liên hệ trực tiếp)
(2) Bản sao hộ tịch (Trường hợp người mời hoặc vợ / chồng là người Nhật)
* Trường hợp người bảo lãnh chịu chi phí ở mục (5) phía trên, hãy xuất trình các tài liệu từ mục (3) - (5) sau:
(3) Giấy chứng nhận bảo lãnh ( mẫu kèm theo)
(4) Một trong những tài liệu sau liên quan đến người bảo lãnh như sau :
+ Giấy chứng nhận thu nhập
+ Giấy chứng nhận số dư tiền gửi ngân hàng
+ Bản lưu giấy đăng ký nộp thuế (Bản sao)
+ Giấy chứng nhận nộp thuế (bản ghi rõ tổng thu nhập)
(5) Phiếu công dân (Bản có ghi quan hệ của các thành viên trong gia đình)
* Trường hợp người mời hoặc người bảo lãnh là người nước ngoài, xuất trình “Giấy chứng nhận có ghi rõ các hạng mục đăng ký người nước ngoài” và copy hộ chiếu thay cho “Phiếu công dân”
II. Visa ngắn hạn (thăm người quen hoặc bạn bè/ du lịch)
Tài liệu người xin Visa chuẩn bị
(1) Hộ chiếu gốc còn hạn 6 tháng
(2) Tờ khai xin cấp Visa 1 tờ ( Có mẫu khi liên hệ trực tiếp)
(3) 01 ảnh 4,5cmx 4,5cm
(4) Tài liệu chứng minh mối quan hệ bạn bè (Trừ trường hợp du lịch)
+ Ảnh chụp chung
+ Thư từ, email
+ Bản kê chi tiết các cuộc gọi điện thoại quốc tế
(5) Tài liệu chứng minh khả năng chi trả kinh phí cho chuyến đi:
+ Giấy chứng nhận thu nhập do cơ quan có thẩm quyền cấp
+ Giấy chứng nhận số dư tiền gửi ngân hàng
Tài liệu phía Nhật Bản chuẩn bị
(1) Giấy lý do mời (Có mẫu khi liên hệ trực tiếp)
(2) Lịch trình ở Nhật (Có mẫu khi liên hệ trực tiếp)
* Trường hợp thăm bạn bè, người bảo lãnh chịu chi phí ở mục (5) phía trên, hãy xuất trình các tài liệu từ mục (3)-(5) sau:
(3) Giấy chứng nhận bảo lãnh (Có mẫu khi liên hệ trực tiếp)
(4) Một trong những tài liệu sau liên quan đến người bảo lãnh
+ Giấy chứng nhận thu nhập
+ Giấy chứng nhận số dư tiền gửi ngân hàng
+ Bản lưu giấy đăng ký nộp thuế (Bản sao)
+ Giấy chứng nhận nộp thuế( bản ghi rõ tổng thu nhập)
(5) Phiếu công dân (Bản có ghi quan hệ của các thành viên trong gia đình)
* Trường hợp người mời hoặc người bảo lãnh là người nước ngoài, xuất trình “Giấy chứng nhận có ghi rõ các hạng mục đăng ký người nước ngoài” và copy hộ chiếu thay cho “Phiếu công dân”
III. Visa ngắn hạn (ví dụ: thương mại ngắn hạn…)
+ Tham dự hội nghị
+ Thương mại ( liên hệ công tác, đàm phán, ký kết hợp đồng, dịch vụ hậu mãi, quảng cáo, điều tra thị trường)
Tài liệu người xin Visa chuẩn bị
(1) Hộ chiếu gốc còn hạn trên 6 tháng
(2) Tờ khai xin cấp visa 1 tờ ( Có mẫu khi liên hệ trực tiếp )
(3) 01 ảnh 4,5cm x 4,5cm
(4) Giấy chứng nhận đang làm việc
(5) Tài liệu chứng minh khả năng chi trả kinh phí cho chuyến đi
+ Quyết định cử đi công tác của cơ quan cấp
+ Giấy yêu cầu đi công tác
+ Văn bản tương đương
Tài liệu do cơ quan phía Nhật Bản chuẩn bị
(1) Một trong những tài liệu nêu rõ các hoạt động ở Nhật như sau:
+ Giấy lý do mời (Có mẫu khi liên hệ trực tiếp)
+ Hợp đồng giao dịch giữa hai bên
+ Tư liệu hội nghị
+ Tư liệu về hàng hóa giao dịch
(2) Lịch trình ở Nhật ( Có mẫu khi liên hệ trực tiếp)
* Trường hợp người mời chịu chi phí ở mục (5) phía trên, hãy xuất trình các tài liệu mục (3) - (4) sau:
(3) Giấy chứng nhận bảo lãnh ( Có mẫu khi liên hệ trực tiếp)
(4) Bản sao đăng ký pháp nhân hoặc tài liệu giới thiệu khái quát về cơ quan đoàn thể
* Những công ty có thương hiệu chỉ cần xuất trình bản copy báo cáo theo quý (SHIKIHO), không cần xuất trình bản sao đăng ký pháp nhân hoặc tài liệu giới thiệu về cơ quan, đoàn thể.
* Đối với trường hợp cá nhân mời, xuất trình “Giấy chứng nhận làm việc tại Nhật” thay cho bản sao đăng ký pháp nhân hoặc tài liệu giới thiệu khái quát về cơ quan, đoàn thể
IV. Visa dài hạn (du học, đi học tiếng, vợ / chồng người Nhật , Visa lao động...)
Trường hợp ở Nhật quá 90 ngày hoặc làm những công việc với mục đích sinh lợi, đề nghị trước hết phải xin giấy chứng nhận đủ tư cách lưu trú ở Nhật Bản tại Cục quản lý xuất nhập cảnh địa phương Bộ tư pháp Nhật nơi gần nhất (Số điện thoại Cục quản lý xuất nhập cảnh Bộ tư pháp Nhật: 03-3580-4111).
(1) Hộ chiếu gốc còn hạn trên 6 tháng
(2) Tờ khai xin cấp Visa 1 tờ ( mẫu kèm theo)
(3) 01 ảnh 4,5cm x 4,5cm
(4) Giấy chứng nhận đủ tư cách lưu trú tại Nhật
(5) Tài liệu xác nhận chính xác bản thân (01 bản)
+ Trường hợp đi học tiếng, du học: Giấy phép nhập học
+ Trường hợp đi lao động kỹ thuật, kỹ năng: Bản hợp đồng lao động, giấy thông báo tuyển dụng...
+ Trường hợp đi tu nghiệp: Giấy tiếp nhận tu nghiệp...
+ Trường hợp vợ/chồng người Nhật: Bản sao hộ tịch sau khi đã nhập hộ khẩu hoặc giấy chứng nhận kết hôn do chính phủ Việt Nam cấp
+ Trường hợp vợ/ chồng người vĩnh trú ở Nhật lâu dài: Giấy chứng nhận đã nộp đăng ký kết hôn hoặc giấy khai sinh do chính phủ Việt Nam cấp
+ Trường hợp người định cư ở Nhật: Giấy khai sinh hoặc giấy chứng nhận kết hôn do chính phủ Việt Nam cấp
Trường hợp tư cách lưu trú khác, hãy hỏi để được giải đáp cụ thể
Ngoài những hồ sơ nêu trên, tùy trường hợp có thể Đại Sứ Quán hoặc Bộ Ngoại Giao sẽ yêu cầu xuất trình thêm giấy tờ khác. Xin lưu ý, trường hợp không xuất trình thêm những giấy tờ được yêu cầu có thể sẽ không được tiếp nhận hồ sơ Visa hoặc chậm cấp Visa.
V. Thời gian làm việc của bộ phận cấp Visa du lịch Nhật Bản:
(1) Thời gian tiếp nhận hồ sơ: Tất cả các ngày trong tuần từ thứ Hai đến thứ Sáu (trừ những ngày nghỉ lễ của Sứ Quán)
Buổi sáng : từ 8h30 đến 11h30
(2) Thời gian trả kết quả Visa: Tất cả các ngày trong tuần từ thứ Hai đến thứ Sáu (trừ những ngày nghỉ lễ của Sứ Quán)
Buổi chiều : từ 1h30 đến 4h45
VI. Thời gian cần thiết
5 ngày kể từ ngày nộp đơn xin (có trường hợp cần thời gian xem xét nhiều hơn 5 ngày)
Ví dụ : - Nộp đơn xin cấp Visa sáng thứ Hai tuần này, trả kết quả vào chiều thứ Hai tuần tiếp theo
- Nộp đơn xin cấp Visa vào sang thứ Ba tuần này, trả kết quả vào chiều thứ Ba tuần tiếp theo                
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Mẫu hộ chiếu
                                Dịch vụ Visa Hong Kong
                                Thủ tục xin visa du lịch Úc
                                Thủ tục xin visa công tác Úc
                                Dịch vụ gia hạn visa
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
