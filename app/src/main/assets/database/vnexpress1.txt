url:http://dulich.vnexpress.net/tin-tuc/chu-de/deal/tour-hai-nam-trung-quoc-5-ngay-gia-tu-7-99-trieu-dong-3485753.html
Tittle :Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
		        24h qua
            Du lịch Chủ đề 
                        Deal
                                        Du Lịch 
                                        Deal 
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Thứ tư, 19/10/2016 | 09:00 GMT+7
|
						Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng												
					Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
	Giá gốc: 9.990.000 đồng
	Giá khuyến mãi: từ 7.990.000 đồng
	Áp dụng từ ngày 19/10 đến 7/12
	Vietrantour được hãng hàng không Hainan Airlines lựa chọn tiên phong triển khai tour khám phá thiên đường tình yêu Hải Nam, nơi được xem là “chân trời góc biển” của các cặp uyên ương.
	Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng. Đặc biệt, khi đăng ký tour từ nay đến hết ngày 31/10, khách hàng nhận ngay quà tặng từ Vietrantour gồm: một USB Hong Kong, một mũ du lịch và gói chăm sóc da mặt công nghệ cao trị giá đến 9 triệu đồng.
	Theo hành trình khám phá thiên đường tình yêu Hải Nam của Vietrantour, du khách sẽ tận hưởng chốn hẹn hò lý tưởng tại đệ nhất vịnh Á Long với những khu nghỉ dưỡng đẳng cấp, chiêm ngưỡng vẻ đẹp của nơi được mệnh danh là “chân trời góc biển”. Ngoài ra, du khách còn có dịp hòa mình cùng trong bầu không khí của những truyền kỳ tình yêu trọn đời, trọn kiếp ở nơi được xem là “Jeju của Trung Quốc” này.
	Vịnh Á Long - thiên đường hò hẹn
	Khi đặt chân đến Tam Á, những đôi tình nhân đang kiếm tìm khoảng không yên bình, riêng tư sẽ thấy đây là chốn hò hẹn lý tưởng. Tắc đường, khói bụi đến những thanh âm ồn ào của cuộc sống xô bồ ngoài kia đều là khái niệm xa lạ với người dân nơi này. Thay vào đó là một vịnh Á Long, đệ nhất vịnh của Trung Quốc trải dài trên diện tích 19km2 với những khu nghỉ dưỡng cao cấp hàng đầu, nhiều dải cát vàng mịn tựa mây trời, biển xanh ngắt phản chiếu nắng vàng, vô số hàng dừa lả lơi trong gió… đủ nên thơ để chắp cánh cho biết bao câu chuyện tình lãng mạn.
	Ngay cả vào mùa đông, vịnh Á Long cũng sở hữu nhiệt độ nước biển thấp nhất 22 độ C để du khách tham gia các hoạt động thể thao dưới nước thú vị như: lặn biển ngắm san hô, dù lượn, lướt sóng, chèo thuyền chuối, motor nước, xe đạp nước… Vùng biển Tam Á còn là nơi sinh sống của hơn 1.064 loài cá, 350 loại tôm, 325 loại cua và 700 loại động vật thân mềm. Tại đây, các cặp đôi đừng nên bỏ lỡ cơ hội trải nghiệm một ngày làm ngư dân và nếm thử hương vị thơm ngon của hải sản tươi sống.
	Bãi biển "Chân trời góc biển" - thiên đường đám cưới
	Cách trung tâm thành phố Tam Á, tỉnh Hải Nam về phía Tây khoảng 23km là một vùng lưng tựa vào núi Mã Lĩnh, mặt hướng ra biển, cảnh quan hữu tình có tên “Thiên Nhai Hải Giác” (chân trời góc biển). Từ khi mở cửa năm 1988, nơi đây thu hút lượng lớn khách du lịch trong và ngoài nước đến tham quan. Tương truyền xưa kia, nhiều vị quan thời nhà Đường, Tống từng bị lưu đày đến đây. Đứng trước đại dương mênh mông, ngoái sau lưng là cố hương mờ mịt, quá buồn tủi cho thân phận nên mới cảm thán đặt cho nơi cùng trời cuối đất tên gọi "Chân trời góc biển".
	Tản bộ dọc theo bờ biển, du khách sẽ bắt gặp một hòn đá hình chóp khắc bốn chữ “Nam thiên nhất trụ”, gắn liền với huyền thoại về cuộc chiến đấu sinh tử giữa con người và biển cả. Cách đó không xa (khoảng 500 mét) còn có 2 hòn đá, dẫu trải qua lớp bụi vô hình của thời gian, vẫn sừng sững tồn tại như minh chứng cho tình yêu vĩnh cửu của chàng Romeo và nàng Juliet Trung Quốc.
	Chuyện kể rằng có hai người trẻ tuổi yêu nhau say đắm nhưng buộc phải chia tay vì mối hận thù gia đình. Thật không may, trong một lần chạy trốn đến vùng đất này, họ đã bị gia đình hai bên phát hiện và chờ sẵn. Không còn lựa chọn nào khác, hai người đã nắm chặt tay, nhảy xuống biển để được ở bên nhau mãi mãi. Bất chợt họ bị sét đánh và hóa thành hai tảng đá lớn cùng với các tác nhân tạo nên nhiều hòn đá nhỏ hơn xung quanh.
	Quá cảm động trước chuyện tình lãng mạn, thế hệ sau đã khắc lên đó bốn chữ “Thiên Nhai Hải Giác” để biểu hiện tình yêu của họ sẽ không bao giờ chết dù ở nơi tận cùng của thế giới. Vì thế, nơi đây thu hút hàng nghìn cặp uyên ương từ khắp nơi trên thế giới đến tổ chức đám cưới mỗi năm.
	Đỉnh Lộc Hồi Đầu Sơn - thiên đường trăng mật
	Nằm trên diện tích gần 83 hecta, chủ yếu là đất rừng, trải dài trên 5 ngọn đồi san sát là thắng cảnh nổi tiếng ở Tam Á: Lộc Hồi Đầu Sơn. Nơi đây gắn với giai thoại về tình yêu bất diệt của đôi tình nhân tộc Lê. Trong một lần đi vào rừng, chàng thợ săn bắt gặp một con hươu lướt qua bèn đuổi theo đến tận vách đá ven biển.
	Bị dồn tới đường cùng, ngay lúc anh chàng giương cung chuẩn bị bắn thì một ánh chớp lóe lên, con hươu bỗng hóa thành một thiếu nữ xinh đẹp tuyệt trần. Hai người nảy sinh tình cảm, cùng nhau vượt qua nhiều thử thách và sống hạnh phúc đến cuối đời. Vì lẽ đó, người ta đã dựng lên bức tượng đá, có điêu khắc hình đôi nam nữ đứng hai bên một con hươu đang quay đầu cao 15m trên đỉnh Lộc Hồi Đầu Sơn, nhằm trân trọng khoảnh khắc hạnh phúc của tình yêu.
	Câu chuyện tình này được tái hiện sinh động trong show diễn “Tam Á thiên cổ tình” kéo dài hơn 1 giờ tại Romance Park - công viên chủ đề nổi tiếng ở thành phố Tam Á. Du khách sẽ phải lặng người thán phục đạo diễn Trương Nghệ Mưu khi ông đã kết hợp nhuần nhuyễn giữa hiệu ứng ánh sáng 3D, laser hiện đại với hát, múa và đặc biệt là xiếc uốn dẻo - môn nghệ thuật mà Trung Quốc thường đoạt huy chương vàng trong các liên hoan xiếc thế giới.
	Ánh hoàng hôn nhuộm vàng ruộm đất trời Tam Á sẽ là ký ức ngọt ngào khó quên khi những cặp đôi tựa vào nhau thủ thỉ tâm tình trên đỉnh Lộc Hồi Đầu Sơn. Tại đây, các cặp đôi có thể phóng tầm mắt xa hơn thu trọn toàn cảnh thành phố, với một bên là những con thuyền đánh cá nép mình bên vịnh nước bình yên và bên kia là vô số khu nghỉ dưỡng sang trọng, cùng biết bao khối nhà chọc trời hay đại lộ rộng thênh thang.
	Theo hành trình của Vietrantour, du khách còn đến với khu văn hóa Phật giáo Nam Sơn - nơi bắt nguồn lời chúc quen thuộc “Phúc như Đông Hải, thọ tỷ Nam Sơn”. Tại đây, du khách có thể chiêm ngưỡng bức tượng Nam Sơn Hải Thượng Quán Âm cao 108 m, đứng trên tòa sen 108 cánh với 3 mặt nhìn ra 3 hướng tượng trưng cho lòng từ bi, bình an và trí tuệ; tham quan nhà hát Vương Miện - nơi diễn ra đêm chung kết các cuộc thi hoa hậu thế giới lần thứ 53 - 57.
	Ngoài ra, du khách còn có thể khám phá những nét văn hóa độc đáo tại làng Miêu - Lê; vườn thực vật nhiệt đới Hưng Long rộng 600 mẫu với hơn 2.300 loài thực vật; phố cổ Hải Khẩu - Kỳ Lâu…
	Văn phòng Vietrantour, 33 Tràng Thi, Hoàn Kiếm, Hà Nội. Tel: 04 73056789. Hotline, Viber, Zalo: 093599 6789. Đặt tour trực tuyến tại đây. Xem thêm tại đây.
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
				Tin liên quan
							Trung Quốc đông đúc đến mức nào  (18/9/2016)
							Ngôi làng toàn đồ giả ở Trung Quốc  (1/9/2016)
							10 món Trung Quốc làm du khách mê mẩn  (4/9/2016)
							Trung Quốc mở cửa công viên toàn 'đồ nhái'  (11/10/2016)
            Xem nhiều nhất
                            Tour thu vàng nước Nga 7 ngày giá 39,9 triệu đồng
                                Góc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban Nha
                                                                     
                                Cặp du khách Trung Quốc quan hệ tình dục trong công viên
                                                                     
                                Cả công viên 'nín thở' chờ du khách béo trượt máng
                                                                     
                                Bí mật đáng sợ bên dưới hệ thống tàu điện ngầm London
                                                                     
                 
                Tags
                                    Vietrantour
                                Hải Nam
                                Trung Quốc
    Tin Khác
                            Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                            Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                            Nhiều tour nước ngoài giảm giá trong tháng khuyến mãi
                            Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                    Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                    Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                    Giảm 3 triệu đồng tour Cửu Trại Câu mùa đẹp nhất trong năm
                                    Giảm 3 triệu đồng tour du lịch Hong Kong 4 ngày
                                    Tour châu Âu mùa thu tiết kiệm 5 triệu đồng
                                    Giảm một triệu đồng tour Boracay - núi lửa Taal - Manila
                                    Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                    Giảm 3 triệu đồng tour Seoul - Nami - Everland 5 ngày
                                    Tour du lịch mùa vàng châu Âu với giá tiết kiệm
                                    Hành hương đất Phật Myanmar 4 ngày từ 9,9 triệu đồng
        Khuyến mãi Du lịch
		  prev
		   next
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
         Gửi bài viết về tòa soạn
        Xem nhiều nhất
    Anh Tây đi bộ khắp thế giới mê mẩn núi rừng Việt NamViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnThủ tướng chỉ ra 3 giải pháp để phát triển du lịchNhững món đồ không thể thiếu khi đi du lịch cuối nămVị đậm đà tinh tế trong món gà sốt tiêu tỏi
        Có thể bạn quan tâm
         
	     
                    Khách sạn Thùy Vân Vũng Tàu
	Khách sạn Thùy Vân tọa lạc tại giao lộ của Thùy Vân và Hoàng Hoa Thám, dọc theo bãi sau, Thành phố Vũng Tàu. Khách sạn có quy mô 54                 
                    Nhà hàng Crystal Jade Palace - JW Marriott Hà Nội
	Crystal Jade Palace là thương hiệu nhà hàng nổi tiếng được vinh danh với các giải thưởng danh giá, chuyên phục vụ các món ẩm thực Quảng Đông truyền thống.                 
                    Kỳ thú hồ nước phun Australia
	Ghé thành phố Kiama chiêm ngưỡng hồ nước phun Kiama nổi tiếng sẽ là ấn tượng khó quên cho bạn vào Giáng sinh năm nay. Được hình thành từ những                 
                    Du lịch xem Shanghai Rolex Masters 2013
	Dù đã bước sang thu song tháng 9 dường như chưa hề hạ nhiệt bởi sức nóng của các trận tranh hùng trong làng banh nỉ tại giải Shanghai Rolex                 
                    Khách sạn Bảo Thy 1 Vũng Tàu
	Khách sạn Bảo Thy 1 nằm trên số 249 đường Lê Hồng Phong, phường 8, thành phố Vũng Tàu với địa điểm lý tưởng khách sạn là sự lựa chọn                 
                    Bà Ba
	Nằm trên đường Lý Thường Kiệt, quán bánh bèo bì Bà Ba đã khiến nhiều thực khách ghé ăn thử và đâm ra “ghiền” các món bánh ướt, bánh tằm                 
                    Đón Giáng Sinh miền giáo đường Bùi Chu
	Xứ đạo Bùi Chu là giáo xứ có diện tích nhỏ nhưng giáo dân đông trong Giáo hội Việt Nam. Đến đây, du khách ghé thăm một xứ đạo lâu                 
                    Chợ phiên Sapa
		Chợ phiên của Sa Pa họp vào ngày thứ bảy và chủ nhật. Người dân vùng xa thường phải đi từ ngày hôm trước. Vào tối thứ bảy, có nhiều                 
                    Khách sạn Alagon Sài Gòn
	Khách sạn Alagon Sài Gòn tọa lạc ngay trung tâm thành phố Hồ Chí Minh là sự lựa chọn tốt nhất dành cho khách du lịch hay các khách đi                 
                    Nhà hàng Rules
	Nhà hàng Rules phục vụ các món ăn truyền thống của người Anh. Nơi đây có phong cách sang trọng, giá cả khá cao, cung cách phục vụ làm hài                 
                    Du ngoạn Hương Sơn, Trung Quốc
	Công viên Hương Sơn là nơi đầu tiên đón thu ở Trung Quốc, khi đó thông, phong, gỗ thích, hoa khói đồng loạt đổi màu thay lá, trở thành điểm                 
                    Phố cổ Hội An
	Phố cổ Hội An với trục phố chính Trần Phú và các trục phố phụ nằm cắt ngang dọc với hàng chục cửa hàng bán đồ lớn nhỏ. Tại đây                 
                    Khách sạn Pullman Saigon Centre
	Khách sạn Pullman Saigon Centre là khách sạn đẳng cấp 5 sao quốc tế có 306 phòng nghỉ rộng rãi, kết hợp thiết kế tinh tế sang trọng với mọi                 
                    Nhà hàng Lá thông
	Lá Thông là nhà hàng phảng phất một chút phong cách Nhật, một chút phong cách châu Âu nhưng lại rất đậm chất Việt Nam. Trong khuôn viên rộng nhuộm màu                 
                    Đón Giáng sinh trên đất thánh La Vang
	Tòa thánh La Vang được xây dựng gần ba cây đa, nơi họ tin là Đức Mẹ đã hiện ra vào năm 1798. Người ta vẫn ghi nhớ rằng dưới                 
                    Chợ đêm Hội An
	Từ 17giờ đến 23giờ hằng ngày, trên tuyến phố dài gần 300 mét này sẽ có 40 gian hàng trưng bày, mua bán các loại hàng thủ công mỹ nghệ,                 
                    Khách sạn Hà Nội Friendly
	Khách sạn Hà Nội Friendly cung cấp một dịch vụ chất lượng cao với các thiết bị và tiện nghi hiện đại nhắm mang đến một sự hài lòng tuyệt                 
                    Nhà hàng Vườn thiên đàng
	Nhà hàng Vườn Thiên Đàng tọa lạc ở trung tâm thành phố Huế, là một địa điểm lý tưởng để du khách thư giãn, ngắm nhìn Sông Hương, cầu Tràng                 
                    Đón năm mới tại Malta, Địa Trung Hải
	Nằm lọt giữa Địa Trung Hải, quần đảo Malta xinh đẹp mang dáng vẻ thanh thoát, yên bình, với những ngôi đền huyền thoại, những vịnh nước xanh biếc đầy                 
                    Chợ Hội An
	Chợ Hội An là nơi buôn bán các mặt hàng dịch vụ từ đồ ăn thức uống của toàn thành phố Hội An. Chợ được chia thành nhiều khu khác                 
			Liên kết ngân hàng
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
