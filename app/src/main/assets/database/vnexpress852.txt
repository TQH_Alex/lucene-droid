url:http://dulich.vnexpress.net/tin-tuc/quoc-te/nguoi-ve-tu-coi-chet-o-bali-3477709.html?utm_campaign=boxtracking&utm_medium=box_topic&utm_source=detail
Tittle :Người về từ cõi chết ở Bali - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
                Các địa danh khác
		        24h qua
            Du lịch Quốc tế 
                                        Du Lịch 
                                        Quốc tế 
                                        Các địa danh
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Thứ ba, 4/10/2016 | 12:00 GMT+7
|
						Người về từ cõi chết ở Bali												
					Trong một buổi ngồi thiền quá lâu, người Ida trở nên lạnh cóng, da trắng toát, tim ngừng đập, ngừng thở và phải sau 6 tiếng, cô mới tỉnh lại rồi trở thành một thầy cúng và luôn tìm cách mang hạnh phúc đến cho mọi người.
					Chợ nghệ thuật đáng đến nhất ở Bali  /  Du khách bị nghi giết cảnh sát ở Bali                    
	Sau khi tác phẩm Eat, Pray & Love (Ăn, Cầu nguyện và Yêu) của Elizabeth Gilbert xuất bản và nổi tiếng khắp thế giới năm 2006, Ketut Liyer - người thầy thuốc mà tác giả nhắc tới trong truyện - cũng nổi tiếng theo. Du khách đua nhau tới Bali - Indonesia ghé thăm ngôi nhà của Liyer ở Ubud để gặp ông. Tuy vậy, Liyer đã qua đời vào tháng 6 năm nay.
	Người tiếp nối "sự nghiệp" của Liyer sau khi ông ra đi là Ida Panditha MpU Budha Maharsi Alit Parama Daksa, một thầy cúng Hindu nổi tiếng.
					Ida Daksa (ở giữa) được mệnh danh Revenant, người trở về từ cái chết hay bóng ma hiện về, do đã chết đi sống lại. Ảnh: News.
	"Tôi gặp Ida khoảng 4 hay 5 lần, tại những giai đoạn bản thân vấp phải vấn đề trong cuộc sống hay cảm xúc cá nhân. Vào chuyến thăm cuối cùng, tôi đã có thể tha thứ cho chính mình nhờ tìm thấy mục đích rõ ràng của cuộc đời. Cô ấy đã thay đổi cuộc sống của tôi", Uday Rao, Tổng giám đốc của Four Seasons Resort ở thị trấn Ubud kể khi đề nghị nhà báo Ian Neubauer viết về Ida.
	Rao là người theo đạo Hindu. Ông kể rằng Ida giúp mọi người không phải vì niềm tin vào tôn giáo hay quyền lực nào đó từ trên trời. Thay vào đó, cô truyền đến mọi người hy vọng và giúp bạn tìm thấy sự bình an trong tâm hồn. Đây cũng là điều mà người thầy thuốc Liyer đã giúp Gilberts và được nhắc đến trong cuốn tiểu thuyết thuộc top bán chạy nhất do tờ The New York Times thống kê suốt 187 tuần.
	"Đó là một câu chuyện dài", Ida nói khi được nhà báo Ian phỏng vấn. "Khi tôi còn trẻ, tất cả những gì tôi muốn là được học hành để có sự nghiệp. Nhưng sau khi tốt nghiệp và tới Bintan (đặc khu kinh tế gần Singapore) tìm việc, tôi chẳng kiếm được cơ hội nào. Vài tháng sau thì hết tiền, tôi phải trở lại Bali để tìm việc nhưng vẫn gặp thất bại", Ida kể về quãng thời gian tuyệt vọng.
	Sau đó, Ida bắt đầu làm quen với thiền và cơ thể cảm thấy có điều kỳ lạ mà ban đầu là đau bụng. Cô cũng tụng kinh bằng một ngôn ngữ bản thân không biết, lặp lại những điệp khúc đó mà chẳng thể nhớ mình đã nói gì. Nhưng đây là lần đầu tiên kể từ khi trưởng thành, Ida thấy bình tĩnh, cảm giác rất thoải mái, đầu óc lúc nào cũng bận rộn nhưng thật yên bình.
					Ida trong một buổi làm lễ cho du khách. Ảnh: facebook NV.
	Trong một lần thiền, Ida đã dành thời gian tĩnh tại rất lâu. Gia đình cô lo lắng và khi kiểm tra cơ thể con gái, họ thấy cô lạnh toát, da chuyển màu trắng, tim ngừng đập. Ai cũng nghĩ Ida qua đời bởi không tìm thấy nhịp thở. Riêng người chú lại nói rằng sẽ để cô làm thầy cúng nếu tỉnh lại.
	Sau 5-6 tiếng ở trạng thái như bị thôi miên, những ngón tay của Ida bắt đầu cử động và dần dần mở mắt nhưng đầu óc không nhớ chút gì về những điều đã trải qua. Những ngày sau đó, cô từ chối việc chấp nhận mình là người trở về từ cõi chết hay thậm chí là cả danh hiệu vị thánh.Hai tuần sau, người chú đưa cô gái đến gặp Hội đồng quốc gia thụ chức của Hiệp hội Phật pháp ở Bali và trải qua quá trình kiểm tra tinh thần. Một số nhà sư đã phản đối Ida vì cho rằng cô quá trẻ. Tuy vậy, hầu hết người trong Hiệp hội ủng hộ và bắt đầu đào tạo Ida với thời gian một năm.Khi được hỏi làm thế nào các thầy cúng có thể đọc nỗi đau của người khác, Ida cho biết lúc đó chỉ cần tập trung và không thể có bất kỳ cảm xúc nào. "Tôi đơn giản chỉ làm nhiệm vụ của mình", cô bày tỏ.Sống trong một ngôi đền, xung quanh Ida là những cánh đồng lúa xanh ngắt bên chân núi Agung, cách Ubud một giờ lái xe. Nơi đây thường có hàng chục, thậm chí hàng trăm du khách chờ đợi, xếp hàng để có thể tham gia vào buổi lễ thanh lọc cơ thể nhằm tẩy trần, thường bắt đầu vào 10h sáng. Chi phí khoảng 30 USD mỗi người.
	Các du khách được nữ thầy cúng này vẩy nước lên đầu và nói: "Hãy cảm thấy tự do để bày tỏ cảm xúc của mình. Mở rộng trái tim và tâm trí bạn. Hãy đi sâu vào trong con người mình, bạn sẽ thấy hạnh phúc, niềm từ bi và cả khiêm nhường ẩn trong bản thân".
	Ở buổi lễ đó, nhà báo Ian thấy một số người cười, một số lại khóc. Một du khách đến từ Australia nói rằng cô không cảm thấy gì dù đã để tâm trí mình cởi mở. Lúc đó, thầy cúng Ida bật cười như trẻ thơ và nói với giọng mềm mại tựa hoa anh đào trôi trong gió: "Hạnh phúc hay nỗi buồn không quan trọng. Hạnh phúc như gió, nó đến rồi đi. Tôi cũng thế, vẫn có những khó chịu của riêng mình. Điều quan trọng là bạn phải biết chấp nhận, chấp nhận bản thân và thế giới xung quanh ta".
					Ubud - thị trấn được biết đến đẹp như trong cổ tích nằm ở vùng trung nguyên với những ngôi đền của đạo Hindu và Phật giáo, nơi được coi là linh hồn ở Bali.
					Trên thực tế, Liyer là một người bán hàng rong và trở nên giàu có nhờ bán bùa yêu cũng như xem bói bằng cách nhìn vào lòng bàn tay người khác. Theo không ít du khách, thứ bùa yêu mà Liyer bán là "nhảm nhí" và nhiều người dân Ubud cũng gọi Liyer là "kẻ nói dối" (cách chơi chữ giữa hai từ Liyer và Liar). Liyer tự nhận thuộc thế hệ thứ 9 của một thầy thuốc và đã qua đời vào tháng 6 năm nay.
				Những người từng gặp nhân vật nổi tiếng này cho biết họ cũng có cảm giác như mình đang bị lừa: "Ông ta giả vờ nhìn vào lòng bàn tay tôi. Nhiều người từng được Ketut bói trong những năm trước và họ biết rằng ông ta nói toàn những thứ nhảm nhí. Ông ấy bị mù, vậy thì làm sao ông ấy có thể nhìn thấy bàn tay của người khác để mà nói về tương lai hay mọi thứ được nhỉ?".
	xem thêm Người đàn ông gác cổng tới 'thiên đường'                        
                                    Anh Minh
                                                                              |  
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
                                                Khách sạn45Nhà hàng29Tham quan459Giải trí34Mua sắm30                        
                Những điểm đến được bao phủ bởi huyền thoại
                    Bí mật đáng sợ bên dưới hệ thống tàu điện ngầm London (19/11)
                                             
                    Bí ẩn tượng đá biết hát bên bờ sông Nile (8/7)
                                             
                    Thành phố của những mụ phù thủy ở Mỹ (12/5)
                                             
                    Những câu chuyện bí ẩn về Vạn Lý Trường Thành (10/5)
                                             
                    Ngôi làng phù thủy bị nguyền rủa hàng trăm năm (1/5)
                                             
                Xem thêm
            Xem nhiều nhất
                            Cây 'đi tiểu' như người tại Đức
                                                             
                                Du khách thoát chết khi ngã ngựa ngay trước mặt sư tử
                                                                     
                                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                                                                     
                                Ngã xuống hồ nước nóng, du khách chết mất xác
                                                                     
                                Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúa
                                                                     
                 
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                Tags
                                    Ăn
                                Cầu Nguyện
                                Yêu
                                Eat
                                Pray
                                Love
                                Du khách
                                Bali
                                Indonesia
                                Du lịch
    Tin Khác
                            Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                                                             
                            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúa
                                                             
                            Bí mật đáng sợ bên dưới hệ thống tàu điện ngầm London
                                                             
                            Ngã xuống hồ nước nóng, du khách chết mất xác
                                                             
                                    Pokemon khổng lồ xuất hiện ở sân bay Changi
                                                                             
                                    Những bữa tiệc 'nóng mắt' với bong bóng xà phòng
                                                                             
                                    Du khách giết vợ để cặp kè gái bán hoa ở Thái Lan bị bỏ tù
                                                                             
                                    Cô gái phải nộp phạt 30.000 USD sau khi bị cưỡng bức
                                                                             
                                    Du khách la hét vì chơi tàu lượn dốc nhất thế giới
                                                                             
                                    Nhà hàng cấm cửa thực khách vì bình luận không tốt
                                                                             
                                    Ngôi nhà không thích đàn ông ở Mỹ
                                                                             
                                    Cây 'đi tiểu' như người tại Đức
                                                                             
                                    Du khách thoát chết khi ngã ngựa ngay trước mặt sư tử
                                                                             
                                    Du khách trộm 2.500 bông hoa ở Disneyland Trung Quốc để bán
                                                                             
        Khuyến mãi Du lịch
		  prev
		   next
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
        Việt Nam
         
                Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                        Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịchViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnKhông gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
         Gửi bài viết về tòa soạn
        Ẩm thực
         
            Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
            Bà chủ quán rất vui vẻ chào đón nhóm của nữ ca sĩ ngay cả khi quán kín chỗ ngồi, dù vừa mở cửa.
                'Vua đầu bếp' để khách tự sáng tạo món ăn
                Vị đậm đà tinh tế trong món gà sốt tiêu tỏi
        Cộng đồng
         
                'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                        Hãy lên kế hoạch du lịch đúng thời điểm cho năm mới dựa theo lịch trình du lịch khắp miền đất nước.
            Du khách đứng tim vì voi chặn đầu xe5 vùng đất Samurai nổi tiếng của Nhật BảnĐồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
        Ảnh
         
                             
            Cô gái bị fan cuồng sao y bản chính khi đi du lịch
            Hai blogger du lịch nổi tiếng trên Instagram Jack Morris và Lauren Bullen đã rất sốc khi phát hiện ra được một "fan cuồng" bắt chước mọi hành động của họ ở những nơi từng đến du lịch.
                                     
                Hãng hàng không xử lý đồ ăn thế nào khi máy bay trễ chuyến
                                     
                10 sân bay có đồ ăn ngon nhất thế giới
        Video
         
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúa
            Những đứa trẻ tại làng Gauriganj được làm quen với rắn từ khi lên hai để chúng có thể biểu diễn thuật thôi miên rắn chuyên nghiệp sau 10 năm.
                Du khách la hét vì chơi tàu lượn dốc nhất thế giới
                Cả công viên 'nín thở' chờ du khách béo trượt máng
        Điểm đến yêu thích                
                    Nhật Bản mở dịch vụ tắm mì ramen để cải thiện làn da                
                    Ngôi làng không điện, nước nổi tiếng ở Uruguay                
                    Du khách bị hải cẩu cắn vì mải tạo dáng với cá                
                    Phụ nữ Armenia làm say mê du khách Mỹ                
                    Du khách thoát chết nhờ đấm liên tục vào mặt cá mập                
		Liên kết du lịch
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
