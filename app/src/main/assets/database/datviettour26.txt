url:http://datviettour.com.vn/tour-du-lich-han-quoc-seoul-dao-nami-27277.html
Tittle :Danh Sách Lịch Khởi Hành | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Danh sách Lịch khởi hành								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Tour du lịch Hàn Quốc: Seoul – Nami – Everland, giá hấp dẫn!                    
Bạn không cần phải tưởng tượng về xứ sở Kim Chi thơ mộng qua những bộ phim tình cảm lãng mạn. Đăng kí tour du lịch Hàn Quốc 5 ngày 4 đêm tại Đất Việt Tour là bạn đã có thể thỏa sức khám phá những cảnh quan thiên nhiên tuyệt đẹp, những hòn đảo thơ mộng và có những kỉ niệm khó quên.
NGÀY 1: TP.HỒ CHÍ MINH – SEOUL
23:45 Quý khách tập trung tại sân bay Tân Sơn Nhất ga đi quốc tế, hướng dẫn viên đón đoàn và hướng dẫn làm thủ tục đáp chuyến bay VJ862 đi Incheon- Hàn Quốc khởi hành lúc 02:30. Quý khách nghỉ đêm trên máy bay.
Xem thêm : Những hoạt động ở Seoul khiến khách du lịch Hàn Quốc mê mẩn
NGÀY 2: THAM QUAN SEOUL (Ăn sáng trên máy bay)
09:30 Máy bay hạ cánh xuống sân bay Incheon – Hàn Quốc. Đoàn làm thủ tục nhập cảnh, và lấy hành lý.
10:15 Xe và hướng dẫn viên địa phương đón đoàn vào Seoul, tham quan Thủ Đô Hàn Quốc với:
Cung điện Kyeongbok (Cảnh Phúc Cung) – cung điện hoàng gia nằm ở phía bắc của thủ đô Seoul, được xây dựng vào năm 1395 dưới triều vua Taejo thuộc triều đại Joseon.Viện bảo tàng truyền thống dân gian Quốc Gia Triều Tiên – Nơi lưu giữ và bảo tồn các di sản văn hóa của Triều Tiên với trên 10.000 mẫu vật phản ánh các nghi lễ, tôn giáo, cách bài trí nhà cửa và các đồ vật trong gia đình Triều Tiên truyền thống.
Ăn trưa tại nhà hàng. Tiếp tục tham quan:
Blue House (hay còn gọi là Nhà Xanh - Phủ Tổng Thống Hàn Quốc) – tọa lạc tại trung tâm thủ đô Seoul. Đây là nơi sinh sống và làm việc của Tổng Thống đương nhiệm Hàn Quốc. Nhà Xanh được xây dựng trên một khuôn viên rộng lớn với kiến trức truyền thống Hàn Quốc kết hợp yếu tố hiện đại (chụp hình bên ngoài).
Quảng Trường Gwanghwamun – chính quyền Seoul quy hoạch nơi này thành điểm nhấn chính cho thành phố, giống như đại lộ Champ-Élysées ở Paris hay quảng trường Thiên An Môn ở Bắc Kinh. Quảng trường Gwanghwamun tọa lạc trên mảnh đất rộng 34, dài 557m với các công trình đa dạng như đài phun nước, thảm hoa, cây xanh ... Đây là một trong những niềm tự hào của người dân Seoul.Suối Cheonggyecheon – con suối trong mát dài 5.8 km giữa lòng thủ đô Seoul.Tháp Namsan Seoul – tọa lạc trên núi Namsan, một biểu tượng của Seoul, nơi các đôi tình nhân gắn những ổ khóa để thề non hẹn biển với nhau. Tại khu vực này Quý khách có thể ngắm nhìn toàn cảnh của thành phố (Không bao gồm phí thang máy lên tháp).
Ăn tối, xe đưa đoàn về khách sạn nhận phòng tự do nghỉ ngơi sau 1 ngày hoạt động tích cực.
Tham khảo >>> 7 lưu ý cần biết khi bạn du lịch đến Hàn Quốc
NGÀY 3:   SEOUL – ĐẢO NAMI
Sau khi ăn sáng, xe và HDV đưa đoàn tham quan:
Đảo Nami – Nổi tiếng với những tán lá phong đỏ rực một góc trời cùng hàng ngân hạnh thẳng tắp đã từng xuất hiện trong bộ phim “Bản tình ca mùa Đông”.
Ăn trưa với món gà nướng trên đảo.
Quay về lại Seoul, tham quan:
Mua sắm tại cửa hàng sâm chính phủ, cửa hàng mỹ phẩm nổi tiếng của Hàn Quốc.Quý khách mua sắm tại cửa hàng miễn thuế, cửa hàng dầu thông đỏ.Tham gia lớp học Kim Chi và mặc trang phục Hanbok truyền thống của Hàn Quốc để chụp ảnh.Tự do mua sắm tại chợ Dongdaemun – khu mua sắm sầm uất tại Seoul.
Ăn tối tại nhà hàng địa phương. Về lại khách sạn tự do nghỉ ngơi hoặc dạo phố
NGÀY 4: SEOUL – EVERLAND – DRUM CAT SHOW
Sau khi ăn sáng, Xe đưa Quý khách tham quan công viên giải trí Everland – Một trong mười công viên lớn nhất thế giới - nổi tiếng với nhiều trò chơi & các màn trình diễn hấp dẫn. Với nhiều trò chơi cảm giác mạnh, khu vườn thú hoang dã Safari, Quý khách sẽ thả mình vui chơi trong những vườn hoa sặc sỡ, cùng tham gia diễu hành với đoàn vũ công của công viên ...
Ăn trưa tại nhà hàng. Tham quan và mua sắm tại:
Trung tâm thực phẩm bổ trợ gan.Cửa hàng sâm tươi Hàn Quốc, một món quà quý giá và ý nghĩa mà quý khách có thể mang về cho người thân và bạn bè.
Ăn tối tại nhà hàng địa phương, thưởng thức chương trình biểu diễn Drum Cat Show vui nhộn và hoành tráng. Xe đưa đoàn về lại khách sạn, tự do nghỉ ngơi.
Tham khảo >>> Tour du lịch Hàn Quốc
NGÀY 5: SEOUL - TP. HỒ CHÍ MINH
Sau khi ăn sáng, đoàn tự do sinh hoạt đến giờ khởi hành ra sân bay quốc tế Incheon, làm thủ tục đáp chuyến bay VJ863 về lại TP.HCM khởi hành lúc 11:40.
15:10 Đến phi trường Tân Sơn Nhất, hướng dẫn viên hỗ trợ quý khách làm thủ tục nhập cảnh và lấy hành lý. Về đến điểm đón ban đầu, hướng dẫn viên chào tạm biệt và hẹn gặp lại quý khách trên các chuyến tham quan sau.
Có gì hấp dẫn?
- Khởi hành ngày:
24/11
 01, 08, 15, 22/12
30/12/2016 (Tết Dương lịch)
- Tư vấn & giao vé miễn phí
- Cam kết chất lượng
- Quà tặng: Túi xách du lịch, bao da hộ chiếu
Hotline: 0902 393 734 - 0981 814 070
BẢNG GIÁ TOUR 
Lượng kháchKhách sạn
2 sao Khách sạn      3 saoKhách sạn
4 saoNgày khởi hànhKhách Lẻ - Ghép Đoàn
(02 - 08 khách) 13.900.00024/11
01, 08, 15, 22/12
15.900.00030/12/2016
(Tết Dương lịch)Khách Đoàn - Tour Riêng
(đoàn 40 khách)Mọi ngày
Theo yêu cầu
***Lưu ý:
Khi đăng ký đi tour quý khách sẽ đặt cọc 8.000.000 vnđ cho công ty.Đối với những quý khách không được cấp visa, sẽ chịu phí 1.200.000 vnđ/khách (Lệ phí LSQ + dịch thuật)
1. Giá tour trẻ em:
Khởi hành ngày 24/11 + 01, 08, 15, 22/12/2016
Trẻ em từ 02 -11 tuổi: 12.590.000 vnđ/kháchTrẻ em dưới 02 tuổi: 4.190.000 vnđ/khách
Khởi hành ngày 30/12/2016 (Tết Dương lịch)
Trẻ em từ 02 -11 tuổi: 14.300.000 vnđ/kháchTrẻ em dưới 02 tuổi: 4.770.000 vnđ/khách
2. Phụ thu phòng đơn: 6.300.000 vnđ/khách
GIÁ TOUR BAO GỒM
Phương tiện: Vé máy bay Sài Gòn – Seoul  – Sài Gòn. Vận chuyển bằng xe máy lạnh đời mới theo chương trình.Visa nhập cảnh Hàn Quốc.Thuế an ninh, thuế xăng dầu, thuế phi trường 2 nước Hàn Quốc và Việt Nam.Khách sạn: 3 sao trung tâm Seoul, tiêu chuẩn 2 người/phòng.Ăn uống: Các bữa ăn theo chương trình.Tham quan: Các chi phí tham quan theo chương trình.Hướng dẫn viên: Đoàn có hướng dẫn viên tiếng Việt chuyên nghiệp, nhiệt tình suốt tuyến.Bảo hiểm: Bảo hiểm du lịch toàn cầu với mức bồi thường cao nhất là 220.000.000 đồng/trường hợp.Quà tặng: Nón, balo du lịch, vỏ hộ chiếu.
GIÁ TOUR KHÔNG BAO GỒM
Hộ chiếu: còn hạn trên 06 tháng tính đến ngày khởi hànhChi phí cá nhân, hành lý quá cước, bồi dưỡng cho bellboy khách sạn.Tiền tip cho hướng dẫn viên và tài xế địa phương (mức đề nghị 6USD/ khách /ngày).Phụ thu phòng đơn (nếu có): 6.300.000 đ/khách/toàn lộ trìnhCác chi phí không được đề cập trong mục bao gồm.
GIÁ TOUR TRẺ EM
Trẻ nhỏ dưới 2 tuổi: 30% giá tour người lớn (sử dụng giường chung với người lớn).Trẻ em từ 2 tuổi đến dưới 12 tuổi: 90% giá tour người lớn (Không có chế độ giường riêng).Trẻ em đủ 12 tuổi trở lên: 100% giá tour người lớn.
ĐIỀU KIỆN HỦY TOUR (Không tính thứ bảy, chủ nhật và ngày lễ)
Hoãn hoặc hủy sau khi đăng ký đặt cọc sẽ không được hoàn lại tiền cọc.Hoãn hoặc hủy trước 30 ngày so với ngày khởi hành (trừ thứ bảy & chủ nhật): 75% giá trị tour.Hoãn hoặc hủy trong vòng 29 ngày so với ngày khởi hành (trừ thứ bảy & chủ nhật): 100% giá trị tour.
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Tour du lịch: Khám phá Singapore, 3 ngày 2 đêm
                                Tour du lịch Tết 2017: Đảo ngọc Phú Quốc 3 ngày 2 đêm
                                Tour du lịch Tết 2017: Du ngoạn Singapore 4 ngày 3 đêm
                                Tour du lịch Tết 2017: Hong Kong – Freeday 4 ngày 3 đêm
                                Tour du lịch Tết 2017: Thái Lan 5 ngày 4 đêm
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
