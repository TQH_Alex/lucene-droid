url:http://datviettour.com.vn/tour-du-lich-campuchia-sihanouk-ville-bokor-41889.html
Tittle :Danh Sách Lịch Khởi Hành | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Danh sách Lịch khởi hành								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Tour du lịch Tết 2017: Sihanouk Ville – Bokor – Koh Rong                    
Tour du lịch Tết 2017: Sihanouk Ville - Bokor - Koh Rong có sự kết hợp thú vị giữa cao nguyên Bokor huyền ảo và biển Sihanoukville mát lành. Hứa hẹn sẽ đem đến cho du khách những kỉ niệm thật đẹp và đáng nhớ.
NGÀY 1: TP.HCM - PHNÔM PÊNH - SIHANOUKVILLE (Ăn: Sáng - Trưa - Tối)
Xe và hướng dẫn viên đón Quý khách tại điểm hẹn khởi hành đi Phnôm Pênh.
Đón tại Công ty du lịch Đất Việt – 198 Phan Văn Trị, quận Gò Vấp.Đón tại Nhà văn hóa Thanh niên – 02 Phạm Ngọc Thạch, Quận 1.Đón tại Trung tâm.Grand Palace Cộng Hòa – 142/18 Cộng Hòa, Quận Tân Bình.
Qúy khách dùng điểm tâm tại nhà hàng với món đặc sản bánh canh Trảng Bàng – Tây Ninh.
Qúy khách làm thủ tục xuất nhập cảnh của khẩu Mộc Bài – Bavet.
Ăn trưa tại nhà hàng, thủ đô Phnôm Pênh, khởi hành đi Sihanoukville.
Qúy khách đến nơi, nhận phòng khách sạn. Quý khách tự do tắm biển, đắm mình trong làn nước trong vắt, phẳng lặng bên bờ cát trắng mịn trải dài của hòn đảo yên tĩnh và đẹp nhất Sihanouk Ville,…Quý khách có thể tham dự các trò chơi trên biển: Môtô nước, xuồng chuối,… (chi phí tự túc). Về lại khách sạn nghỉ ngơi, quý khách chụp hình lưu niệm tại tượng sư tử vàng,…Đoàn nghỉ đêm tại khách sạn.
NGÀY 2: SIHANOUKVILLE – KOH RONG SALOEM ISLAND (Ăn: Sáng - Trưa - Tối)
 Sáng: Quý khách dùng điểm tâm tại khách sạn, đi chợ Phsar Thmei (theo từng nhóm nhỏ) mua: Tôm tít, cua, ghẹ, mực... để mang ra đảo (phí tự túc).
08h30: Xe đưa quý khách ra cảng, khởi hành chuyến tham quan trên vịnh Thái Lan. Tàu cập vịnh Saracen – còn gọi là đảo Rong Samloem – vịnh có hình trái tim với bãi biển cực đẹp, một bên biển xanh trong vắt, một bên là rừng nguyên sinh. Quý khách tự túc ăn trưa.
16h00: Tàu đưa đoàn về khách sạn ăn tối, tự do khám phá Sihanoukville về đêm.
Tham khảo bài viết : Du lịch Tết tại xứ sở chùa tháp Campuchia
NGÀY 3 : SIHANOUK VILLE – BOKOR
07h00: Qúy khách ăn sáng, trả phòng, tự do tắm biển hoặc trekking xuyên rừng nguyên sinh.
10h00: Đoàn trả phòng, lên tàu về lại đất liền, khới hành đi cao nguyên Bokor.
12h30: Qúy khách ăn trưa tại Sihanouk, chinh phục cao nguyên Bokor:
- Viếng tượng Sơn Thần – khối đá tự nhiên, hình mặt người, cao 4m.
-  Viếng Wat Samprov Pram (Chùa Năm Thuyền) - nơi lưu giữ những bức tranh tôn giáo tuyệt đẹp với tuổi thọ gần 90 năm, phóng tầm mắt nhìn đảo Phú Quốc và Vịnh Thái Lan từ độ cao 1080m.
-  Tham quan nhà thờ Pháp, dinh thự hoàng gia, casino cổ được người Pháp xây dưng những năm đầu thế kỷ XX.
17h00: Qúy khách nhận phòng khách sạn.
18h00: Đoàn dùng tiệc buffet, tự do tham gia các hoạt động vui chơi giải trí về đêm tại Thansur Bokor.
Xem thêm: Du lịch Campuchia mùa cao điểm cần chú ý điều gì?
NGÀY 4: BOKOR - TP.HCM (Ăn: Sáng - Trưa - Tối)
08h00: Qúy khách trả phòng, ăn sáng, xuống núi.
Đoàn tiếp tục về Phnôm Pênh qua tỉnh Kampong Speu.
12h30: Qúy khách ăn trưa lẩu băng chuyền SouSou, khởi hành về TP.HCM qua các tỉnh Kandal, PreyVeng, Svayrieng.
Đoàn làm thủ tục xuất nhập cảnh ở Bavet - Mộc Bài.
19h30: Về đến TP.HCM, kết thúc chuyến tham quan, chia tay và hẹn gặp lại quý khách trên mọi nẻo đường quê hương!
Có gì hấp dẫn?
- Khởi hành ngày 30/01/2017 (Mùng 3 Tết Âm lịch)
- Tư vấn & giao vé miễn phí
- Cam kết dịch vụ chất lượng
- Quà tặng: Lì xì đầu năm, nón du lịch, bao da hộ chiếu
Hotline: 0909 108 369 - 0966 980 936 
 
BẢNG GIÁ TOUR 
Lượng kháchKhách sạn
3,4 saoKhách sạn
4 saoKhách sạn
5 saoNgày khởi hànhKhách Lẻ - Ghép Đoàn
(02 - 08 khách) 4.990.00030/01/2017
(Mùng 3 Tết Âm lịch)Khách Đoàn - Tour Riêng
(trên 40 khách)Mọi ngày
Theo yêu cầu
 
Lưu ý:
Giá tour trẻ em:
Trẻ em từ 05 - 11 tuổi: 3.790.000 vnđ/kháchTrẻ em dưới 05 tuổi: 2.490.000 vnđ/khách
Phụ thu phòng đơn: 2.900.000 vnđ/khách
GIÁ TOUR BAO GỒM
Phương tiện: Xe máy lạnh đưa đón tham quan theo chương trình.Khách sạn: Phòng 2 người (trường hợp lẻ nam, lẻ nữ ngủ phòng 3).
+ Shihanouk: 3 sao (local )
+ Bokor: 5 sao (local)
Ăn uống: 
- Ăn sáng: Buffet tại khách sạn.
- Ăn trưa, chiều: nhà hàng địa phương tiêu chuẩn, hợp vệ sinh.
Chi phí ăn uống theo chương trình.Hướng dẫn viên: Đoàn có hướng dẫn viên tiếng Việt thuyết minh và phục vụ đoàn tham quan suốt tuyến.Bảo hiểm: Bảo hiểm du lịch suốt tuyến theo quy định của bảo hiểm Bảo Minh.Tham quan: Giá tour đã bao gồm phí vào cổng tại các điểm tham quan.Qùa tặng: Nón du lịch, bao da hộ chiếu.
GIÁ TOUR KHÔNG BAO GỒM
Tiền TIP cho hướng dẫn viên + tài xế địa phương (3 USD/khách/ngày)Hộ chiếu còn giá trị trên 06 tháng tính từ ngày về.Chi phí cá nhân ngoài chương trình: giặt ủi, điện thoại, minibar…Visa tái nhập vào Việt Nam cho người nước ngoài hoặc khách Việt Kiều 890.000 vnđ/Visa nhận tại cửa khẩu và có giá trị vào Việt Nam 1 lần - trong 1 tháng đối với Việt kiều.Riêng quốc tịch Mỹ là 3.600.000 vnđ/khách – theo quy định mới loại tái nhập 1 năm nhiều lần.% Thuế VAT
ĐIỀU KIỆN HỦY TOUR CAMPUCHIA: (Không tính thứ bảy, chủ nhật và ngày lễ)
1. Đặt cọc 2.500.000 vnđ/khách ngay khi đăng ký tour.
2. Hủy tour sau khi đăng ký: mất tiền cọc tour.
Trước ngày đi 20 ngày: 50% giá tourTrước ngày đi 10 - 15 ngày: 75% giá tourTrước ngày đi 7 ngày : 100% giá tour
Trường hợp quý khách có yếu tố khách quan về cá nhân vui lòng báo sớm trước 5 ngày không tính thứ 7, chủ nhật & ngày lễ, để công ty hỗ trợ giải quyết.
*ĐIỀU KIỆN & HỘ CHIẾU KHI ĐĂNG KÝ TOUR:
Hộ chiếu còn 6 tháng tính đến ngày đi tour về.Hộ chiếu đảm bảo các yếu tố sau: hình ảnh không bị hư hỏng, mờ nhòe, thông tin đầy đủ, dù còn hạn sử dụng nhưng nếu hình ảnh bị mờ nhòe, vẫn không được xuất hay nhập cảnh ....trường hợp vào ngày khởi hành, Quý khách không được xuất hoặc nhập cảnh trong nước và ngoài nước công ty sẽ không chịu trách nhiệm và không hoàn tiền các khoản chi phí liên quan khác.Khách quốc tịch nước ngoài/Việt kiều, vui lòng kiểm tra visa của khách vào Việt Nam nhiều lần hay 1 lần,Khách hàng làm visa tái nhập, ngày đi tour mang theo 2 tấm hình 4 x 6 và mang theo visa vào Việt Nam khi xuất , nhập cảnh.Khi đăng ký tour, vui lòng cung cấp thông tin cá nhân trên hộ chiếu: Họ & tên chính xác, ngày cấp, ngày hết hạn hộ chiếu, số điện thoại liên lạc, địa chỉ liên lạc,...
Tham khảo >>> Tour du lịch Campuchia Tết 2017                
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Tour du lịch: Khám phá Singapore, 3 ngày 2 đêm
                                Tour du lịch Tết 2017: Malaysia – Singapore, 6 ngày 5 đêm
                                Tour du lịch Tết 2017: Du ngoạn Phú Quốc 3 ngày 3 đêm
                                Tour du lịch Tết 2017: Ninh Chữ – vịnh Vĩnh Hy 3 ngày
                                Tour du lịch Tết 2017: Seoul – Yangji Pine – Everland, 5 ngày 4 đêm
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
