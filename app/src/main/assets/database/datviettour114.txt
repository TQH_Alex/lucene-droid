url:http://datviettour.com.vn/tour-du-lich-tet-2016-thai-lan-5-ngay-4-dem-bay-toi-39976.html
Tittle :Danh Sách Lịch Khởi Hành | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Danh sách Lịch khởi hành								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Tour du lịch Tết 2017: Thái Lan 5 ngày 4 đêm                    
Đón mừng năm mới trên xứ sở chùa Vàng sẽ mang đến cho du khách nhiều điều thú vị, được khám phá thủ đô Bangkok hoa lệ, thiên đường biển Pattaya,...Tour du lịch Thái Lan tết khởi hành mùng 3, 4 Tết giá tiết kiệm chỉ 9.990.000. Đăng ký ngay hôm nay!
NGÀY 01: TP.HCM - BANGKOK - PATTAYA (Ăn: Trưa - Tối)
Trưởng đoàn của công ty Đất Việt sẽ có mặt tại sân bay Tân Sơn Nhất, giúp quý khách làm thủ tục cho đoàn đáp chuyến bay đi Thái Lan. Đến sân bay Thái Lan, xe đưa Quý khách khởi hành đi Pattaya (thành phố Ma Quỷ). Trên đường đi, quý khách dùng chân tham quan Công viên Sriracha Tiger Zoo. Tại đây, Quý khách sẽ được xem những màn biểu diễn thật hấp dẫn như: Show cá sấu, show cọp, heo,…
Sau đó, xe đi thẳng về thành phố biển Pattaya. Khi đến nơi, Quý khách dùng bữa tối với món ăn tự chọn BBQ hải sản Hàn Quốc.
Quý khách về khách sạn nhận phòng nghỉ ngơi hoặc tham gia các show về đêm tại Pataya (chi phí tự túc),…
NGÀY 02: PATTAYA – ĐẢO CORAL – COLOSSEUM SHOW (Ăn: Sáng - Trưa - Tối)
Sáng: Báo thức, Quý khách dùng điểm tâm sáng buffet tại khách sạn. Xe đưa quý khách tiếp tục tham quan:
- Đảo San Hô (Coral) bằng tàu cao tốc, tại đây Quý khách có thể tắm biển hay tham gia các trò chơi trên biển như: Dù kéo, lướt ván, lái canô, thám hiểm dưới đáy biển…(chi phí tự túc).
Quý khách quay về đất liền dùng cơm trưa tại nhà hàng và khởi hành đi tham quan xưởng chế tác đá quý (Một trung tâm trưng bày đá quý đạt tiêu chuẩn ISO 9001) Thái Lan được biết đến là một quốc gia nổi tiếng khai thác về đá quý.
Tiếp đến, đoàn tham quan Trân Bảo Phật Sơn - núi Phật được khắc bằng vàng 24k, cao 140 mét được chiếu bằng tia laze khắc lên vách núi để tặng cho vua Rama IX nhân dịp 50 năm trị vì vương quốc Thái Lan.
Quý khách đến Trung tâm Mimosa – đó là 01 khu trung tâm mua sắm phức hợp đầy màu sắc tại Pattaya. Với tên gọi là City Of Love mô phỏng theo thành phố tình yêu Colmar của Pháp...
Sau đó tham quan đồi vọng cảnh Phra Tamak. Tại đây, du khách thỏa sức ngắm cảnh trên biển Pattaya và chiêm ngưỡng vẻ đẹp lộng lẫy của toàn cảnh thành phố nhộn nhịp này. Ngoài ra, du khách còn được tham gia chương trình đốt pháo cầu phúc cầu may cho gia đình tại đài tưởng niệm và để tỏ lòng tôn kính Krom Luang Chumporn, cha đẻ của ngành hải quân hoàng gia Thái Lan.
Buổi tối: Xe đưa quý khách đến chương trình ca nhạc tạp kỹ Colosseum Show hoành tráng, là một trong những chương trình dàn dựng hoành tráng và chuyên nghiệp, các hiệu ứng ánh sáng, âm thanh.
Sau đó, quý khách được tặng 01 xuất massage Thái Cổ Truyền giúp lưu thông khí huyết, đánh thức tiềm năng cơ thể.
Tham khảo >>> Đảo san hô Coral, sự lựa chọn tuyệt vời khi du lịch Thái Lan
NGÀY 03: PATTAYA – BANGKOK (Ăn: Sáng - Trưa - Tối)
Sáng: Báo thức, Quý khách dùng điểm tâm sáng Buffet tại khách sạn. Trả phòng khách sạn, khởi hành về Bangkok. Xe đưa đoàn tham quan:
Trung tâm nghiên cứu rắn độc.Ghé tham quan cửa hàng trưng bày các loại đồ da như da cá sấu, cá đuối….
Đặc biệt, quý khách dùng buffet tại tòa nhà cao Bayoke Sky 86 tầng. Sau đó, đoàn khởi hành tham quan Hoàng Cung - được vua Rama V xây dựng vào năm 1901, và từ đó đến nay vẫn luôn được bảo tồn cẩn thận để quảng bá sự huy hoàng và thịnh vượng của một thời kì hoàng kim. Đây là tòa nhà bằng gỗ Teak màu vàng đẹp và lớn nhất thế giới, trong Hoàng Cung trưng bày các tác phẩm như: Ngà voi, thủy tinh, đồ vật bằng bạc, đồ sành sứ, đồ cổ…. Tại đây, Quý khách có thể hiểu rõ hơn về cách sinh hoạt của hoàng thất.
Qúy khách dùng cơm tối tại nhà hàng, tự do tham quan Bangkok về đêm.
Xem thêm: Kinh nghiệm cho một chuyến du lịch Pattaya hoàn hảo
NGÀY 04: BANGKOK - TỰ DO MUA SẮM (Ăn: Sáng - Trưa)
Sáng: Qúy khách dùng điểm tâm, xe đưa quý khách đi ra bến thuyền, dạo thuyền trên dòng sông Chaopharaya huyền thoại, xem hiện tượng cá nổi trên sông…
Qúy khách tham quan chùa Phật Vàng lớn nhất thế giới: cao 3 mét và nặng hơn 5 tấn. Tượng được đúc theo phong cách Sukhothai tĩnh lặng và được khám phá một cách tình cờ vào thập niên 1950. Người địa phương cho rằng bức tượng lớn nhất thế giới này biểu thị cho sự thịnh vượng và thuần khiết cũng như sức mạnh và quyền năng.
Quý khách tự do tham quan, mua sắm tại các siêu thị lớn như: World Trade Centre, Baiyoke Sky, Pratunam Market, Platinum, Robinson Silom, Rachada Sago Market. Buổi tối quý khách tự túc ăn tối.
NGÀY 5: BANGKOK - TP. HCM (Ăn: Sáng)
Sáng: Báo thức, quý khách dùng điểm tâm sáng và làm thủ tục trả phòng, khởi hành đi tham quan chùa Thuyền - Wat Yan Nawa - ngôi chùa độc nhất vô nhị, với hình dáng thuyền rồng nằm bên cạnh dòng sông vào đời vua Rama III – chùa có lịch sữ lâu đời linh thiêng ở địa phương, cùng với kiến trúc Thái là các Chedi cao vút mang đậm phong cách thời Ayuthaya.
Quý khách tự do tham quan mua sắm tại BIG C. Đến giờ hẹn, xe đưa Quý khách ra sân bay Thái Lan đáp chuyến bay trở về TP.HCM,… Về đến sân bay Tân Sơn Nhất, hướng dẫn viên chia tay đoàn kết thúc chuyến tham quan.
Có gì hấp dẫn?
- Khởi hành ngày 29, 30, 31/01/2017 (Mùng 2, 3, 4 Tết Âm lịch)
- Giá cực kỳ ưu đãi
- Tư vấn và giao vé miễn phí
- Quà tặng: Lì xì đầu năm, nón du lịch, bao da hộ chiếu
Hotline: 0909 108 369 – 0966 980 936 
BẢNG GIÁ TOUR 
Lượng kháchKhách sạn
3 sao Khách sạn      4 saoKhách sạn
5 saoNgày khởi hànhKhách Lẻ - Ghép Đoàn
(02 - 08 khách) 10.990.00029, 30/01/2017
(Mùng 2, 3 Tết Âm lịch)
 9.990.00031/01/2017
(Mùng 4 Tết Âm lịch)Khách Đoàn - Tour Riêng
(đoàn 40 khách)Mọi ngày
Theo yêu cầu
 Chú ý:
Giá tour trẻ em:
Từ 02 - 11 tuổi: 9.490.000 vnđ/kháchTrẻ em dưới 2 tuổi: 2.990.000 vnđ/khách
Phụ thu phòng đơn: 3.290.000 vnđ/khách
GIÁ TOUR DU LỊCH THÁI LAN BAO GỒM
Phương tiện: Vé máy bay khứ hồi và thuế phí của hãng hàng không Vietjet. Vé máy bay không hoàn không đổi (do chuyến bay phụ thuộc vào các hãng hàng không nên trong một số trường hợp giờ bay có thể thay đổi mà hãng hàng không không báo truớc, khi công ty nhận được thông tin sẽ thông báo đến quý khách sớm nhất). Hành lí: 7 kg hành lí xách tay và 20 kg hành lí kí gửi.
Xe máy lạnh đưa đón tham quan theo chương trình.Khách sạn: Khách sạn 3 sao. Tiêu chuẩn 2 khách/phòng (trường hợp 3 khách vì lý do giới tính).Ăn uống: Ăn sáng: Buffet tại khách sạn. Ăn trưa, chiều tại nhà hàng địa phương tiêu chuẩn, hợp vệ sinh.Hướng dẫn viên: Đoàn có hướng dẫn viên tiếng Việt thuyết minh và phục vụ đoàn tham quan suốt tuyến.Bảo hiểm: Bảo hiểm du lịch theo quy định của bảo hiểm Bảo Minh.Tham quan: Giá tour đã bao gồm phí vào cổng tại các điểm tham quan theo chương trình.Qùa tặng: Nón du lịch, bao hộ chiếu Đất Việt Tour.
GIÁ TOUR KHÔNG BAO GỒM
Tiền TIP cho hướng dẫn viên + Tài xế địa phương 3$~68.000 vnđ/ngày/người.Hộ chiếu còn giá trị trên 06 tháng tính từ ngày về.Chi phí cá nhân ngoài chương trình: giặt ủi, điện thoại, minibar…Visa tái nhập cảnh Việt Nam cho khách quốc tịch nước ngoài 890.000 VNĐ/khách.Riêng quốc tịch Mỹ là 3.600.000 VNĐ/khách – theo quy định mới loại tái nhập 1 năm nhiều lần.% Thuế VAT.
ĐIỀU KIỆN HỦY TOUR: (Không tính thứ bảy, chủ nhật và ngày lễ)
1. Đặt cọc 6.000.000VND/khách ngay khi đăng ký tour.
2. Hủy tour sau khi đăng ký: mất tiền cọc tour.
Trước ngày đi 20 Ngày: 50% giá tourTrước ngày đi 15 Ngày: 70% giá tourTrước ngày đi 10-15 ngày: 100% giá tour
- Trường hợp quý khách có yếu tố khách quan về cá nhân vui lòng báo sớm trước 5 ngày không tính thứ 7, chủ nhật & ngày lễ, để công ty hỗ trợ giải quyết.
ĐIỀU KIỆN & HỘ CHIẾU KHI ĐĂNG KÝ TOUR
Hộ chiếu còn 6 tháng tính đến ngày đi tour về.Hộ chiếu đảm bảo các yếu tố sau: hình ảnh không bị hư hỏng, mờ nhòe, thông tin đầy đủ, dù còn hạn sử dụng nhưng nếu hình ảnh bị mờ nhòe, vẫn không được xuất hay nhập cảnh,...Trường hợp vào ngày khởi hành, Quý khách không được xuất hoặc nhập cảnh trong nước và ngoài nước công ty sẽ không chịu trách nhiệm và không hoàn tiền các khoản chi phí liên quan khác.Khách quốc tịch nước ngoài/Việt kiều, vui lòng kiểm tra visa của khách vào việt nam nhiều lần hay 1 lần.Khách hàng làm visa tái nhập, ngày đi tour mang theo 2 tấm hình 4x6 , và mang theo visa vào việt nam khi xuất , nhập cảnh.Khi đăng ký tour, vui lòng cung cấp thông tin cá nhân trên hộ chiếu: Họ & tên chính xác, ngày cấp, ngày hết hạn hộ chiếu, số điện thoại liên lạc ,địa chỉ liên lạc,...
Bài viết liên quan:
- Tour du lịch Tết 2017: Khám phá xứ sở chùa Vàng Thái Lan 5 ngày
- Du lịch Tết Thái Lan                
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Tour du lịch Tết 2017: Đà Lạt ngàn thông 3 ngày 3 đêm
                                Tour du lịch Tết 2017: Ninh Chữ – vịnh Vĩnh Hy 3 ngày
                                Tour du lịch Tết 2017: Du ngoạn Phú Quốc 3 ngày 3 đêm
                                Tour du lịch: Khám phá đất nước Myanmar, 4 ngày 3 đêm
                                Tour du lịch Tết 2017: Trà Sư – Bà Lụa 2 ngày 2 đêm
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
