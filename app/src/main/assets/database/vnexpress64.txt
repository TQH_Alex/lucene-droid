url:http://dulich.vnexpress.net/tin-tuc/viet-nam/ha-noi/hoang-tu-anh-an-dem-ben-ho-guom-3501011.html
Tittle :Hoàng tử Anh ăn đêm bên Hồ Gươm - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
                Các địa danh khác
		        24h qua
            Du lịch Việt Nam 
                        Hà Nội
                                        Du Lịch 
                                        Hà Nội 
                                        Các địa danh
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Thứ sáu, 18/11/2016 | 15:32 GMT+7
|
						Hoàng tử Anh ăn đêm bên Hồ Gươm												
					Công tước xứ Cambridge dùng tôm rang muối tỏi, nem rán hải sản, đậu chiên sả và chiêm ngưỡng toàn cảnh Hồ Gươm, tại một nhà hàng ở trung tâm Thủ đô.
					Hoàng tử Anh William ngồi vỉa hè uống cà phê phố cổ Hà Nội  /  Hoàng tử Anh ca ngợi Việt Nam nỗ lực chống buôn bán động vật hoang dã  /  Hồng Nhung, Phan Anh ấn tượng với sự chân tình của Hoàng tử William                    
	Ông Lương Tuấn Phương - Giám đốc điều hành Cầu Gỗ Vietnamese Cuisine Restaurant cho biết, bàn của Hoàng tử Anh được đặt ở góc sảnh. Từ đây có thể thu gọn trong tầm mắt cả lòng hồ rộng lớn, với cầu Thê Húc, đền Ngọc Sơn, Tháp Rùa cổ kính.
					Hoàng tử William chụp ảnh lưu niệm cùng thực khách và nhân viên nhà hàng.
	"Trước đó, tôi chỉ biết có khách VIP tới dùng bữa vì thấy nhiều vệ sĩ đến khảo sát chỗ ngồi nhưng không ngờ đó là Hoàng tử William", ông Phương chia sẻ.
	Hậu duệ Hoàng gia Anh trong mắt chủ và thực khách của nhà hàng rất tươi tắn và giản dị. Sau một ngày bận rộn, Hoàng tử cởi bỏ cà vạt, nói cười thoải mái với mọi người và thư thái tận hưởng không gian đêm cuối thu Hà Nội.
	Vừa dự tiệc chiêu đãi tại Đại sứ quán Anh, đoàn khách vẫn gọi chút đồ ăn nhẹ để khám phá ẩm thực Hà Nội như tôm rang muối tỏi, nem rán hải sản và đậu chiên sả. Đây đều là các món riêng của nhà hàng, 100% nguyên liệu và gia vị Việt.
					Sảnh tầng 6 nhà hàng Cầu Gỗ Vietnamese Cuisine Restaurant giúp thực khách cảm nhận hơi nước Hồ Gươm mát rượi, cùng hương đêm Hà Nội.
	Trước khi về, Hoàng tử bắt tay thân thiện với mọi người. Công tước xứ Cambridge không quên nói lời cảm ơn đầu bếp cùng đội ngũ nhân viên nhà hàng.
	Trước Hoàng tử William, nơi đây cũng được nhiều đại sứ quán, quan khách quốc tế và người nổi tiếng lựa chọn để thưởng thức ẩm thực Việt.
	Chuyến công du của Hoàng tử William kéo dài 2 ngày, 16 và 17/11 tại Hà Nội. Hoàng tử đến Việt Nam lần này để đồng chủ trì Hội nghị Hà Nội về phòng, chống buôn bán trái phép các loài động vật, thực vật hoang dã do Bộ Nông nghiệp và Phát triển Nông thôn tổ chức, với sự hỗ trợ của Chính phủ Anh.
					Món ngon thuần Việt trong thực đơn phục vụ đoàn khách Hoàng gia Anh.
	Trong buổi đầu tiên có mặt tại Hà Nội, chiều 16/11, Hoàng tử đã khám phá phố cổ, thưởng thức cà phê vỉa hè. Đêm khuya cùng ngày, Hoàng tử chọn Cầu Gỗ Vietnamese Cuisine Restaurant để lưu lại trong mắt toàn cảnh Hồ Gươm, tháp Rùa - một Hà Nội thanh lịch và bình yên.
	Hưng Thịnh
	Cầu Gỗ Vietnamese Cuisine Restaurant tọa lạc trên 2 tầng cao nhất tòa nhà Hàm Cá Mập (5 và 6) tại số 1, 3, 5, 7 Đinh Tiên Hoàng, phường Hàng Bạc, quận Hoàn Kiếm, Hà Nội. Nhà hàng có một mặt hướng ra phố cổ, một mặt bao quát toàn bộ không gian Hồ Gươm - trái tim của thủ đô Hà Nội. Trên diện tích 250m2 mỗi tầng, nhà hàng được thiết kế thành những góc nhỏ đậm chất Hà Nội sang trọng và xưa cũ, với nhiều đồ nội thất cổ như cánh cửa gỗ chớp, quạt cổ, máy hát, đèn dầu. Nơi đây dành phần lớn diện tích để mở sảnh hướng ra Hồ Gươm, giúp thực khách có thể tận hưởng cảm giác thư thái theo tầm nhìn trải rộng khắp hồ. Điện thoại: 043.9260808.                        
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
                                                Khách sạn168Nhà hàng22Tham quan135Giải trí10Mua sắm11                        
				Tin liên quan
							Quán bar nhỏ nhất nước Anh  (21/10/2016)
							Anh Tây đi bộ khắp thế giới mê mẩn núi rừng Việt Nam  (17/11/2016)
							Nhà hàng cấm cửa thực khách vì bình luận không tốt  (18/11/2016)
							Trộm ví tài xế, du khách Anh bị bắt tại Thái Lan  (29/10/2016)
            Xem nhiều nhất
                            Anh Tây đi bộ khắp thế giới mê mẩn núi rừng Việt Nam
                                                             
                                Việt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật Bản
                                                                     
                                Thủ tướng chỉ ra 3 giải pháp để phát triển du lịch
                                                                     
                                Những món đồ không thể thiếu khi đi du lịch cuối năm
                                Vị đậm đà tinh tế trong món gà sốt tiêu tỏi
                 
                Tags
                                    Cầu Gỗ Vietnamese Cuisine Restaurant
                                Hồ Gươm
                                Công tước xứ Cambridge
                                Hoàng tử Anh
                                Hoàng tử William
    Tin Khác
                            'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                                                             
                            Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
                                                             
                            'Vua đầu bếp' để khách tự sáng tạo món ăn
                                                             
                            Những quán ăn không biển hiệu vẫn đông khách ở Hà Nội
                                                             
                                    Giới trẻ Hà Nội được miễn phí vé vào cửa festival Ẩm thực Italy
                                                                             
                                    Ăn ở vỉa hè Hà Nội, khách Tây thấy mình như người khổng lồ
                                                                             
                                    Tam giác mạch khoe sắc bên bờ hồ Hà Nội
                                                                             
                                    Khách Tây 'quậy' hết mình trong lễ hội âm nhạc ở Hà Nội
                                                                             
                                    Bát mì nấu gần sân Hàng Đẫy ấm bụng ngày trở gió
                                                                             
                                    Bên trong ngôi chùa đẹp bậc nhất thế giới ở Hà Nội
                                                                             
                                    Nhiều ưu đãi trong ngày hội tư vấn du lịch ở Hà Nội
                                                                             
                                    Những nơi được check in khi Hà Nội đón gió lạnh đầu mùa
                                                                             
                                    Bỏ qua bún chửi, Hà Nội còn nhiều món bún ngon nên thử
                                                                             
                                    Lễ hội Oktoberfest với nhiều ưu đãi hấp dẫn tại Vuvuzela
        Khuyến mãi Du lịch
		  prev
		   next
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
         Gửi bài viết về tòa soạn
        Ẩm thực
         
                             
            Quán bún măng vịt mỗi ngày chỉ bán một tiếng
            Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
                Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
                'Vua đầu bếp' để khách tự sáng tạo món ăn
        Cộng đồng
         
                'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                        Hãy lên kế hoạch du lịch đúng thời điểm cho năm mới dựa theo lịch trình du lịch khắp miền đất nước.
            Du khách đứng tim vì voi chặn đầu xe5 vùng đất Samurai nổi tiếng của Nhật BảnĐồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
        Ảnh
         
                             
            Khách Tây 'quậy' hết mình trong lễ hội âm nhạc ở Hà Nội
            Suốt 3 ngày diễn ra Quest Festival tại Sơn Tinh Camp (Ba Vì, Hà Nội), hàng nghìn tín đồ âm nhạc trong và ngoài nước đổ về đây để sống trong bầu không khí sôi động của âm nhạc và lễ hội.
                                     
                Bên trong ngôi chùa đẹp bậc nhất thế giới ở Hà Nội
                                     
                Mùa bắt châu chấu ở ngoại thành Hà Nội
        Video
         
            Du khách la hét vì chơi tàu lượn dốc nhất thế giới
            Tàu lượn Takabisha (Nhật Bản) có độ dốc lên tới 121 độ, vận tốc tối đa đạt 160 km/h khiến người chơi như đứng tim khi nhiều đường ray thẳng đứng, tàu như rơi tự do.
                Cả công viên 'nín thở' chờ du khách béo trượt máng
                Cảnh đẹp ngoạn mục trên khắp nẻo đường Na Uy
        Điểm đến yêu thích                
                    Kế hoạch 4 ngày khám phá đảo thiên đường ở Campuchia                
                    Những món ngon nên thử của Phú Quốc                
                    Khách du lịch được mời dùng toilet miễn phí ở Đà Nẵng                
                    Chuyến bay Cần Thơ - Đà Lạt chỉ mất một giờ                
                    Những cây cầu có tên gọi độc đáo ở miền Tây                
		Liên kết du lịch
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
