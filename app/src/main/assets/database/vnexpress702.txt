url:http://dulich.vnexpress.net/tin-tuc/cong-dong/dau-chan/cuu-trai-cau-thien-duong-co-that-3496033.html
Tittle :Cửu Trại Câu - Thiên đường có thật - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
		        24h qua
            Du lịch Cộng đồng 
                        Dấu chânTư vấnHỏi - Đáp
                                        Du Lịch 
                                        Dấu chân 
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Thứ sáu, 11/11/2016 | 02:07 GMT+7
|
						Cửu Trại Câu - Thiên đường có thật												
					Phong cảnh Cửu Trại Câu (Trung Quốc) vào tháng 10, 11 toát lên vẻ đẹp cuốn hút mọi du khách.
					Cô gái Việt chỉ ra 8 định kiến sai lầm khi đến Cửu Trại Câu  /  Có một 'tiểu Cửu Trại Câu' ngay ở Việt Nam                    
					Cửu Trại Câu hay còn gọi là "thung lũng chín làng" nằm ở phía bắc tỉnh Tứ Xuyên - Trung Quốc. Với những hồ nước đa sắc cùng các ngọn núi phủ tuyết và cánh rừng vàng ruộm mỗi khi thu về, nơi đây trở thành địa điểm du lịch mang vẻ đẹp khó cưỡng đối với mọi du khách.
					Tò mò về nơi được mệnh danh là thiên đường hạ giới, nhóm chúng tôi gồm 14 người đã quyết định phải đến được Cửu Trại Câu vào mùa thu này. Thông qua một người bạn đang làm việc tại Trung Quốc, chúng tôi mất chừng một tháng để lên chương trình.
					Nhằm giảm bớt chi phí và tăng thêm trải nghiệm, các thành viên thống nhất mua vé tàu từ ga Gia Lâm đến ga Nam Ninh, rồi mới từ ga Nam Ninh bắt xe tới sân bay Nam Ninh rồi bay tới Thành Đô. Với chi phí khoảng 25 triệu/ người, hành trình của chúng tôi dài 8 ngày từ ngày 20 đến 28/10.
					Ngoài Cửu Trại Câu, chuyến đi này còn cho chúng tôi cơ hội khám phá cả 5 địa điểm du lịch nổi tiếng khác của tỉnh Tứ Xuyên là: Hoàng Long, Nga My, Lạc Sơn, công viên Panda và phố cổ Cẩm Lý.
					Chúng tôi may mắn vì toàn bộ khách sạn và tàu xe đi lại thậm chí là vé thắng cảnh của đều được người bạn bên Trung Quốc tính toán hợp lý nhằm đáp ứng tiêu chí: đàng hoàng nhưng tiết kiệm.
					Nói là tiết kiệm nhưng toàn hành trình chúng tôi vẫn luôn được ở khách sạn 2 hoặc 3 sao. Việc ăn uống cũng khá thoải mái trừ một vài bữa trưa trên đường di chuyển. Để việc trải nghiệm được trọn vẹn, bạn tôi còn bố trí cho đoàn ở một đêm homestay trong làng của người Tây Tạng. Nhờ đó, chúng tôi đã có thêm những trải nghiệm thú vị hơn cả những gì mọi người mong đợi.
					Mục đích chính của chuyến đi là khám phá Cửu Trại Câu nên chúng tôi có kế hoạch dành hẳn hai ngày cho địa điểm này. Và thực tế thì hai ngày vẫn còn chưa đủ để khám phá hết vẻ đẹp của thiên nhiên kỳ ảo không nơi nào có được của Cửu Trại Câu. 
					Có một lưu ý nhỏ cho những ai muốn khám phá Cửu Trại Câu trong hai ngày là vé tham quan bán theo ngày. Nếu bạn muốn ở đây hai ngày sẽ phải mua hai lần vé. Mặc dù ở lại làng người Tạng, tức là ở bên trong khu vực thắng cảnh chứ chưa ra khởi cổng kiểm soát nhưng hôm sau chúng tôi vẫn phải mua vé bổ sung. Một số đoàn không biết quy định này nên khi bị kiểm tra, họ bị coi là trốn vé và thanh tra đưa lên xe ra ngoài cổng, chưa kể số tiền nộp phạt tiền khá nặng.
					Vé thắng cảnh Cửu Trại Câu khá đắt. Hơn 300 tệ (gần 1 triệu/người). Do tính chất công việc, chúng tôi đã liên hệ để được làm thẻ sinh viên quốc tế và chiếc thẻ này đã giúp chúng tôi được giảm giá khá nhiều khi mua vé tham quan Cửu Trại Câu cũng như một số nơi khác.
	Xem thêm: Kinh nghiệm du ngoạn Cửu Trại Câu của phượt thủ 9x
	Trần Yến
	Ảnh: Phạm Ngọc Minh                        
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
                Trung Quốc huyền bí
                    Bên trong thị trấn ma lớn nhất thế giới ở quê hương Thành Cát Tư Hãn (16/11)
                                             
                    Cầu 'nút thắt may mắn' ở Trung Quốc (4/11)
                                             
                    Thành phố 'quỷ khóc ma gào' ở Trung Quốc (3/11)
                                             
                    Mai Chi lạc bước ở Phượng Hoàng cổ trấn (27/10)
                                             
                    Bóng ma hút du khách trong phủ Hòa Thân (15/10)
                                             
                Xem thêm
            Xem nhiều nhất
                            'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                                                             
                                Góc tối của lễ hội phụ nữ khoe ngực trần tại Tây Ban Nha
                                                                     
                                Cặp du khách Trung Quốc quan hệ tình dục trong công viên
                                                                     
                                Cả công viên 'nín thở' chờ du khách béo trượt máng
                                                                     
                                Bí mật đáng sợ bên dưới hệ thống tàu điện ngầm London
                                                                     
                 
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                Tags
                                    Cửu Trại Câu
                                Du lịch Trung Quốc
                                Thiên đường hạ giới
                                Du lịch
                                Du khách
                                Di sản
    Tin Khác
                                                     
                            Diễm My ăn trưa, trồng rau cùng người dân Hội An
                                                             
                            Du khách đứng tim vì voi chặn đầu xe
                                                             
                                                     
                            Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                                             
                            Kỷ niệm hài hước với toilet trên đường du lịch
                                                             
                                    Hành khách phát hiện gián trong bữa ăn chay trên máy bay
                                                                             
                                    Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
                                                                             
                                    Du khách mỏi tay câu cá khổng lồ tại Thái Lan
                                                                             
                                    Cả công viên 'nín thở' chờ du khách béo trượt máng
                                                                             
                                    Cặp du khách Trung Quốc quan hệ tình dục trong công viên
                                                                             
                                    Quang Vinh khám phá hai mặt đối lập của Sài Gòn
                                                                             
                                    Khách Tây nói người Việt bấm còi xe như chơi nhạc
                                                                             
                                    Giây phút kinh hoàng của chàng trai thoát chết trong lễ hội 'bò đuổi'
                                                                             
                                    Cách tiêu tiền của khách Trung Quốc và sự khác biệt về đẳng cấp
                                                                             
                                    Hòn đảo chỉ phục vụ khách VIP ở Philippines
                                                                             
        Khuyến mãi Du lịch
		  prev
		   next
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
        Việt Nam
         
                Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                        Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịchViệt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật BảnKhông gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
         Gửi bài viết về tòa soạn
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
        Ảnh
         
                             
            Diễm My ăn trưa, trồng rau cùng người dân Hội An
            Diễn viên Diễm My đã có dịp dùng bữa cùng một cặp vợ chồng ở làng Trà Quế, Hội An, trước khi thực hiện bộ ảnh "Vẻ đẹp không tuổi" cùng nhiếp ảnh gia Pháp.
                                     
                Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                     
                Quang Vinh khám phá hai mặt đối lập của Sài Gòn
        Video
         
            Du khách đứng tim vì voi chặn đầu xe
            Đoàn du khách được một phen hú hồn vì gặp đàn voi khi thăm một khu bảo tồn thiên nhiên tại châu Phi.
                Du khách la hét vì chơi tàu lượn dốc nhất thế giới
                Cả công viên 'nín thở' chờ du khách béo trượt máng
        Bình luận hay
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
