url:http://haidangtravel.com/Tour-Dao-Diep-Son-Nha-Trang-Tet-Am-Lich-2017
Tittle :Tour Đảo Điệp Sơn, Nha Trang, Tết Âm Lịch 2017
content :Toggle navigation
                             Trang Chủ
                              Tour Du Lịch
                         Tour Trong Nước Tour Nước Ngoài
                              Tổ Chức Sự Kiện
                             Thuê Xe Du Lịch
                         Thuê Xe 4 Chổ Thuê Xe 7 Chổ Thuê Xe 16 Chổ Thuê Xe 29 Chổ Thuê Xe 35 Chổ Thuê Xe 45 Chổ
                             Khách Sạn
                              Cẩm nang du lịch
                              Tin tức du lịch
                              Bảng Giá Tour
                     1900 2011
                    08.3849.3838 - 0948.99.1080
                    Đăng Ký
                                            ×
                                            ĐĂNG KÝ
                                                        Tên đăng nhập (bắt buộc)
                                                        Nhập số điện thoại ( bắt buôt )
                                                        Nhập họ tên của bạn
                                                        Nhập mail của bạn
                                                        Nhập mật khẩu ( bắt buột )
                                                        Nhập lại mật khẩu ( bắt buột )
                                                        NamNữKhác
                                                        Địa chỉ nhà bạn
                                                        Ngày sinh của bạn
                                                 Nhận mail từ Hải Đăng Travel
                                                Đăng Ký
                        Đăng nhập
                                            ×
                                            ĐĂNG NHẬP
                                        Quên mật khẩu ?
                                        Đăng Ký
                                                Ghi nhớ mật khẩu
                                                Đăng nhập
                    Trang chủ
                 ›
                    Tour trong nước
                 ›
                    Tour Đảo Điệp Sơn, Nha Trang, Tết Âm Lịch 2017
                 Photo
                                Hổ Trợ Trực Tuyến
                             SKYPE YAHOO914353773
                             Tour Đảo Điệp Sơn, Nha Trang, Tết Âm Lịch 2017
                                     2 ngày 2 đêm
                                      Ô Tô 
                                      2 sao 
                                     1,750,000 đ
                                            Giá gốc: 2,400,000 đ
                                                 Lịch khởi hành
                                                                                                        29/01/2017
                                 DỊCH VỤ ĐÍNH KÈM
                                  Vé tham quan  Hướng dẫn viên  Ăn uống  Phương tiện   Bảo hiểm  Khách sạn
                                 Cơ hội nhân quà hấp khi đăng ký tour tại www.haidangtravel.com xem quà tặng tại đây 
                                 MUA TOUR
                                 GIỚI THIỆU Tour Đảo Điệp Sơn, Nha Trang, Tết Âm Lịch 2017
                                         Đánh Giá Tour
                                    0.0
                                                                                                                                    Đăng nhập để đánh giá hay điền các thông tin sau :
                                                        Họ tên (bắt buộc)
                                                        Nhập mail của bạn (bắt buộc)
                                                    Nhập nhận xét của bạn (bắt buộc)
                                                     Đánh giá :
                                                                           Rất Tốt  Tốt  khá  Trung Bình  Bình Thường
                                                Rất Tốt 0
                                                Tốt  0
                                                Khá 0
                                                Tạm Được  0
                                                Bình Thường  0
                                            Nhận xét (0)
                                                                                                                                                                                Hết
                        LỊCH TRÌNH TOUR
                                 NGÀY 1 : SÀI GÒN-NHA TRANG-ĐẢO ĐIỆP SƠN 
                                    07:30: Quý khách dùng bữa sáng tại nhà hàng thuộc địa phận Ninh Hòa. Sau đó tiếp tục hành trình đến với xã Vạn Giã.
09:00: Quý khách lên tàu khám phá “Đảo Điệp Sơn” - Điệp Sơn là một dãy gồm 3 hòn đảo nhỏ, nằm chơi vơi trong vùng biển thuộc vịnh Vân Phong, tỉnh Khánh Hòa. Hành trình khám phá hòn đảo bắt đầu từ thị trấn Vạn Giã, sau khoảng một giờ đồng hồ lênh đênh trên biển, quần đảo Điệp Sơn xinh đẹp dần xuất hiện, cắt hình rõ nét trên đường chân trời.
Cho tàu neo đậu gần hòn đảo nằm giữa, bạn có thể bước xuống tàu và bắt đầu hành trình chinh phục con đường mòn trên biển vô cùng độc đáo và thú vị dài gần 700 mét, nối liền hòn đảo giữa với đảo Điệp Sơn lớn. Con đường uốn lượn, rộng khoảng 1 mét và nằm sâu dưới mặt nước biển trong xanh chưa đến nửa mét.
Đi bộ trên con đường này đem lại cảm giác vô cùng thích thú, xen lẫn hồi hộp và một chút sợ hãi khi bạn phải bước giữa đại dương bao la. Bạn có thể thỏa thích ngắm nhìn những đàn cá nhỏ tung tăng bơi lội hai bên, chốc chốc lại thấy những đàn cá nhảy tung lên khỏi mặt nước, hay giơ tay vẫy chào con thuyền đang chạy lướt qua.
Đến với Điệp Sơn,du khách sẽ trầm trồ trước vẻ đẹp của những bãi biển hoang sơ cát trắng, nước biển trong xanh, và những cánh đồng cỏ lau tuyệt đẹp.
Điệp Sơn là một quần đảo còn xa lạ với nhiều người, nơi đây vẫn còn giữ nét đẹp hoang sơ, trong xanh của biển, người dân hiền hòa, hiếu khách.
11:00: Quý khách dùng buổi trưa tại Đảo với các  món miền biển cùng với những người dân nhiệt tình mến khách
13h00: Đoàn lên tàu trở về đất liền, tạm biệt hòn đảo xinh đẹp quyến rũ.
Xe đưa đoàn đến với Nha Trang, nhận phòng nghỉ ngơi
15h00 : Xe đưa đoàn tham quan chùa Long Sơn, Chùa còn được gọi là chùa Phật Trắng hoặc Đăng Long tự. Đây là điểm đến hành hương, vãn cảnh yêu thích của người địa phương cũng như khách thập phương và là ngôi chùa nổi tiếng nhất thành phố Nha Trang bởi lịch sử lâu đời với bức tượng Kim Thân Phật tổ nằm trên đỉnh núi.
Xây dựng từ năm 1963, đã đưa ngôi chùa vào danh sách kỷ lục Việt Nam: “Ngôi chùa có tượng Phật ngoài trời lớn nhất Việt Nam”.
Đoàn tham quan tắm khoáng tháp Bà để thư giãn bằng hơi nước và nước khoáng thiên nhiên, và là nơi phục hồi sức khỏe bằng phương pháp trị liệu với nước khoáng mang lại tinh thần thư thái cho con người.
Buổi chiều: Quý khách tự do khám phá đặc sản của thành phố biển.
 
 
 
                                 NGÀY 2 : NHA TRANG – NHÀ YẾN-TPHCM 
                                     
 
7h00:Quý khách dùng bữa sáng tại  nhà hàng và thỏa thích tắm biển
8h30: Đoàn trả phòng, sau đó quý khách tham quan mua sắm Yến sào Nha Trang tốt nhất, giá cả rẻ nhất ; được dùng trà Yến miễn phí và nếm vị Yến sào Nha Trang chính hiệu. Kế đến, Quý khách sẽ tận mắt nhìn thấy các quy trình sơ chế tổ Yến, được phổ biển tỉ mỉ kỹ thuật xây dựng nhà chim và nghệ thuật dẫn dụ chim Yến vào nhà. Quý khách sẽ được nghe thuyết minh sâu sắc vòng đời chim Yến... Và, hấp dẫnnhất là được vào  bên trong nhà chim, được sờ thấy những chiếc tổ Yến và nhìn thấy chim Yến “cư ngụ” trong môi trường nhà phố.
Dùng buổi trưa tại Nha Trang
Ghé mua đặc sản tại Nha Trang, Phan Rang và Phan Thiết.
19h00: Về Đến Tp.Hồ Chí Minh. Kết thúc chương trình và hẹn gặp lại.
 
                    G.Thiệu
                                        Ngày 1
                                        Ngày 2
                     ĐỊA ĐIỂM ĐÓN KHÁCH
                                                    QUÝ KHÁCH ĐI TOUR KHỞI HÀNH BẰNG Ô TÔ VỚI NHỮNG ĐIỂM ĐÓN SAU :
                             Đón khách tại Công ty Du lịch Hải Đăng, 225 Bàu Cát, P12, Quận Tân Bình
                             Đón khách tại Nhà Văn Hóa Thanh Niên, 04 Phạm Ngọc Thạch, Quận 1
                             Đón khách tại Cây xăng Comeco, Ngã 4 Hàng Xanh, Quận Bình Thạnh
                             Đón khách tại Ngã 4 Thủ Đức, Quận 9
                             Đón khách tại Siêu thị Lotte Mart, Ngã 4 Amata, Biên Hòa, Đồng Nai
                            QUÝ KHÁCH ĐI TOUR TUYẾN MIỀN TÂY NHỮNG ĐIỂM ĐÓN SAU :
                             Đón khách tại Công viên Bàu Cát, 141 Đồng Đen, P11, Quận Tân Bình 
                             Đón khách tại Nhà Văn Hóa Thanh Niên, 04 Phạm Ngọc Thạch, Quận 1
                             Bến xe miền tây , Q 6 ( bệnh viện Triều An )
                            QUÝ KHÁCH ĐI TOUR KHỞI HÀNH BẰNG MÁY BAY VỚI NHỮNG ĐIỂM ĐÓN SAU :
                             Quý khách vui lòng tập trung tại nhà ga đối với những khách đi tour khởi hành bằng tàu
                             Quý khách vui lòng tập trung tại sân bay hoặc các phòng chờ của các hãng bay đối với những khách đi tour khởi hành bằng máy bay
                         QUY ĐỊNH TOUR
                                             GIÁ TOUR BAO GỒM 
                                                  Lưu Trú
                                                Khách sạn 2 sao: Cường Long, Phương Nhung …..
Máy lạnh, tivi, hoặc khách sạn tương đương,phòng 3-4 khách.
Phụ thu phòng đơn: 1 phòng 1 khách: 300.000 đ/khách/2 đêm dịp lễ, 200.000 đ/khách/2 đêm ngày thường
Phụ thu phòng đôi:   75.000 đ/Khách/đêm (Ngày thường)
Phụ thu phòng đôi: 125.000 đ/Khách/đêm (Ngày lễ)
Trường hợp có khách đi lẻ, Hải Đăng ghép cho khách và khách không phải đóng phụ thu.
. *** Nếu quý khách có nhu cầu ở khách sạn 3-4-5 sao vui lòng liên hệ HAIDANGTRAVEL để được sắp xếp.
                                                 Vé Tham Quan
                                                    Vé vào cổng: Tàu đi Đảo Điệp Sơn
                                                  Ăn Uống
                                                Điểm tâm: 2 buổi 
2 set Menu: 40.000 đ/suất (1tô bún, phở, hủ tiếu + 1 ly nước ngọt, sữa, café)
Ăn chính: 2 buổi
1 buổi: 90.000đ/suất
1 buổi trên trên đảo: 150.000 đ/suất
                                                   Nón Nước - Khăn Lạnh
                                                Nước suối: 1 chai 500ml/ngày/khách
Khăn lạnh: 1 cái/ngày/khách
Nón du lịch: 1 nón HAIDANGTRAVEL/khách
                                                 Phương Tiện
                                                Xe Giường Nằm đời mới máy lạnh đón khách tại điểm hẹn,XE DU LỊCH PHỤC VỤ RIÊNG của đoàn,Hải Đăng không sử dụng ghép khách với xe chay tuyến. Nên quý khách được báo cụ thể số giường xe ngay khi đăng kí tour
Mỗi đợt tối thiểu 35 khách/ đoàn. Nếu slk từ 20- dưới 35 công ty sẽ set up xe ghế ngồi để vẫn đảm bảo lịch khởi hành cho quý khách.Trường hợp không đủ slk thì được dời sang lịch khởi hành tiếp theo.
Quý khách qua đảo bằng tàu : - thời gian di chuyển 1 giờ
Nếu quý khách đi cano qua đảo vui lòng phụ thu : + 130.000/khách (ngày 15.4)  + 150.000 /khách (ngày 29.4) -  thời gian di chuyển 20 phút
                                             GIÁ TOUR KHÔNG BAO GỒM 
                                                   Chi Phí Cá Nhân
                                                Chi phí cá nhân: Giặt ủi, điện thoại,ăn uống ngoài chương trình …..Điểm tham quan: Các điểm ngoài chương trình,lặn ngắm san hô, tắm khoáng...Thuế VAT : Quý khách có nhu cầy xuất hóa hơn VAT vui lòng đóng thêm 10%
                                                  Thuế VAT
                                                 10% thuế VAT
                                             QUY ĐỊNH TRẺ EM 
                                                Trẻ em dưới 5 tuổi
Cha, mẹ hoặc người thân đi kèm tự lo các chi phí ăn, ngủ, tham quan (nếu có) cho bé. Hai người lớn chỉ kèm 1 trẻ em dưới 5 tuổi, em thứ 2 trở lên phải mua ½ vé tour. Ghế ngồi cho bé 40% nếu ba mẹ có nhu cầu mua
Từ 6-10 tuổi
60% vé tour:Bao gồm các dịch vụ ăn uống, ghế ngồi trên xe và ngủ chung với gia đình. Hai người lớn chỉ được kèm 1 trẻ em từ 5 đến 10 tuổi, em thứ 2 trở lên phải mua suất người lớn.
Trẻ em trên 10 tuổi
Vé người lớn, tiêu chuẩn như người lớn
                                             QUY ĐỊNH MUA VÀ HỦY TOUR 
                                                Sau khi đăng ký, thanh toán ít nhất 50% tiền cọc và đóng hết 100% trước khởi hành 7 ngày.
Dời ngày khởi hành trước 10 ngày không mất phí, sau 8 ngày mất 20%.( Không tính ngày lễ và chủ nhật), chỉ được dời 1 lần.
Sau khi đăng ký huỷ tour mất 10% giá tour
Từ 10 đến trước 8 ngày trước ngày khởi hành chịu phí 30% giá tour.( Không tính ngày lễ và chủ nhật)
Từ 8 đến 6 ngày trước ngày khởi hành chịu phí 50% giá tour.( Không tính ngày lễ và chủ nhật)
Từ 3-5 ngày trước ngày khởi hành chịu phí 70% giá tour.( Không tính ngày lễ và chủ nhật)
Từ 2 ngày trước ngày khởi hành chịu phí 100% giá tour.( Không tính ngày lễ và chủ nhật)
Các quy định trên không áp dụng cho các dịp lễ và tết.
Các ngày lễ tết việc dời ngày và hủy tour trước 15 ngày khởi hành mất 50% giá tour. Sau 15 ngày so với ngày khởi hành mất 100% giá tour.(Không tính ngày chủ nhật.)
Sau khi hủy tour, du khách vui lòng đến công ty nhận tiền trong vòng 2 tuần kể từ ngày đăng ký tour. Chúng tôi chỉ thanh toán trong thời gian 14 ngày nói trên.
Trường hợp hủy tour do sự cố khách quan như thiên tai, dịch bệnh hoặc do tàu thủy, xe lửa, máy bay hoãn/hủy chuyến, Hải Đăng Travel sẽ không chịu trách nhiệm bồi thường thêm bất kỳ chi phí nào khác ngoài việc hoàn trả chi phí những dịch vụ chưa được sử dụng của tour đó.
                                             HÌNH THỨC THANH TOÁN 
                                                Thanh toán bằng tiền mặt tại Trụ sở chính, chi nhánh của Hải Đăng Travel hoặc công ty hỗ trợ thu tận nơi ( nếu khách có yêu cầu)
Chuyển khoản qua số tài khoản
Số tài khoản: 711A00686521.
Chủ tài khoản: Dương Phan Quốc Văn – Vietinbank TP.HCM.
Nội dung: Tour Đảo Điệp Sơn-Nha Trang 2n2đ. Ngày khởi hành:…, Tên khách:…, Số lượng:…
Sử dụng Voucher, cung cấp mã số Voucher, đóng thêm phụ thu (nếu có). Du khách giữ Voucher và nộp lại cho công ty.
Thanh toán bằng thẻ Visa ( thẻ từ, thẻ chip,…)
Thẻ giảm giá, thẻ ưu đãi xuất trình khi đăng ký tour.
                     ĐIỂM THAM QUAN 
                                                  Kiểm Tra Giá & Lịch Khởi Hành 
                                                                                            KS 2 Sao
                            Khởi Hành
                                                                                                                                                                    29/01/2017
                            = 
                            Người lớn
                            = 
                            Trẻ em
                            = 
                            Tổng cộng
                                = 
                         Mua Tour
                TOUR LIÊN QUAN
                                              4 ngày 4 đêm |  Ô Tô |  2 sao
                                            Xem
                                    Tour Ninh Chữ, Đà Lạt,Tết Âm Lịch 2017
                                         2,890,000 đ
                                                Giá gốc: 2,900,000 đ
                                                     Lịch khởi hành
                                                        29/01/201730/01/2017
                                              4 ngày 4 đêm |  Ô Tô |  2 sao
                                            Xem
                                    Tour Nha Trang, Đà Lạt, Tết Âm Lịch 2017
                                         2,890,000 đ
                                                Giá gốc: 2,940,000 đ
                                                     Lịch khởi hành
                                                        29/01/201730/01/201731/01/2017
                                              3 ngày 3 đêm |  Ô Tô |  2 sao
                                            Xem
                                    Tour Nha Trang Tiệc Buffet, Bãi Tắm Nhũ Tiên,Tết Âm Lịch 2017
                                         2,090,000 đ
                                                Giá gốc: 2,290,000 đ
                                                     Lịch khởi hành
                                                        29/01/201730/01/201731/01/201702/02/2017
                                              3 ngày 3 đêm |  Ô Tô |  2 sao
                                            Xem
                                    Tour Nha Trang, Bar Trên Biển,Đón Tết Âm Lịch 2017
                                         1,990,000 đ
                                                Giá gốc: 1,890,000 đ
                                                     Lịch khởi hành
                                                        29/01/201730/01/201731/01/201702/02/2017
                                              2 ngày 2 đêm |  ô Tô |  3 sao
                                            Xem
                                    Tour Ninh Chữ - Vĩnh Hy Resort 3sao, Tết Âm Lịch 2017
                                         1,750,000 đ
                                                Giá gốc: 1,999,000 đ
                                                     Lịch khởi hành
                                                        29/01/201730/01/2017
                                              5 ngày 5 đêm |  Máy bay |  3 sao
                                            Xem
                                    Tour Đà Nẵng, Sơn Trà, Hội An , Huế,Quảng Bình, Tết Âm Lịch 2017
                                         3,750,000 đ
                                                Giá gốc: 4,189,999 đ
                                                     Lịch khởi hành
                                                        29/01/201730/01/201731/01/2017
                    TRỤ SỞ CHÍNH
                         225 Bàu Cát, P12, Q.Tân Bình, HCM
                         19002011 - 08 3849 3838 
                        08 3849 2999   
                          www.haidangtravel.com  
                         info@haidangtravel.com 
                        CSKH: 0948.99.1080
                    CHI NHÁNH
                         Quận 1 : Lầu 7, Tòa Nhà Viễn Đông, 14 Phan Tôn
                        19002011(Ext-107) -  0838204589  
                         Quận 12 : 256 TX25, Thạnh Xuân, Q12 
                        19002011(Ext-111)
                         Biên Hòa : 51A2, KP11, P.Tân Phong 
                        19002011(Ext-108) -  0613999392  
                    TRỢ GIÚP
                    Liên hệ & giới thiệuĐiều khoản chungHình thức thanh toánHướng dẫn mua tourChính sách bảo mật
                        GIỜ LÀM VIỆC
                            Thứ 2 đến 6
                                Sáng: 08:00-12:00Chiều:13:30-17:30
                            Thứ 7 Sáng: 08:00-12:00Nghỉ chủ nhật, lễ, tết
                    Được tìm kiếm nhiều
                        Tour Nha Trang
                        Tour Đà Lạt
                        Tour Cà Mau
                        Tour Phú Yên
                        Tour Ninh Chữ
                        Tour Côn Đảo
                        Tour Miền Bắc
                        Tour Phú Quốc
                        Tour Đà Nẵng
                        Tour Phan Thiết
                        Tour Bình Ba
                        Tour Nam Du
                        Tour Vũng Tàu
                        Tour Bà Lụa
                THÀNH TÍCH
            2015 © All Rights Reserved. Công ty cổ phần xây dựng du lịch Hải Đăng
        Your browser doesn't support canvas. Please download IE9+ on
            IE Test Drive
            +
                Có thể bạn quan tâm ?
                                Tour Đảo Hòn Sơn Khám Phá Đỉnh Ma Thiên Lãnh 2N2Đ
                                 1,390,000 đ
                                Tour Côn Đảo, Trọn Gói Vé Máy Bay 2N1Đ
                                 3,999,000 đ
                                Tour Ninh Chữ - Vĩnh Hy - Cổ Thạch đồng giá 2N2Đ
                                 999,000 đ
                                Tour Ninh Chữ, Vĩnh Hy Hang Rái Siêu Tiết Kiệm 2n2d
                                 850,000 đ
                                Tour Côn Đảo, Khám Phá Lịch Sử Văn Hóa Côn Sơn 2N1Đ
                                 4,590,000 đ
                                Tour Ninh Chữ - Vĩnh Hy Khám Phá Bãi Đá Bảy Màu Resort 3sao 2N2Đ
                                 1,250,000 đ
                                Tour du lịch Vũng Tàu, Long Hải, Suối nước nóng Bình Châu 2N1Đ
                                 789,000 đ
                                Tour Đảo Phú Quý, Khám Phá Hòn Đảo Mang Vẻ Đẹp Hoang Sơ 3N3Đ
                                 2,250,000 đ
                                Tour Đà Lạt Phototrip Khám Phá Những Cung Đường Hoa 3N3Đ
                                 1,250,000 đ
                                Tour Đảo Bình Hưng, Hang Rái, Vĩnh Hy 2n2d
                                 1,111,000 đ
                        ×
