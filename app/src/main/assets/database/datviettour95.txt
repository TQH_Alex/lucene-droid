url:http://datviettour.com.vn/truong-dai-hoc-cong-nghe-tp-hcm-tham-quan-phan-thiet-thang-112016-55362.html
Tittle :Khách Hàng Thân Thiết | Công ty Đất Việt Tour
content :Trụ sở Tp.HCM:(08) 38 944 888CN Bình Dương:(0650) 3 775 775CN Đồng Nai:(061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
																											Khách Hàng Thân Thiết								
										Chọn chuyên mụcTrang chủTour 30-4Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingVisa passport 
                        Trường Đại học Công nghệ TP.HCM tham quan Phan Thiết, tháng 11/2016                    
					Nhằm tạo điều kiện để quý thầy cô và các bạn sinh viên được trải nghiệm thực tế, vui chơi, thư giãn sau những giờ học căng thẳng ngày 07 – 09/11/2016 vừa qua trường đại học Công Nghệ TP.Hồ Chí Minh đã phối hợp cùng Đất Việt Tour tổ chức chuyến du lịch Phan Thiết kết hợp thực tập thực tế cho sinh của trường. Trong chuyến đi này, đoàn được khám phá nhiều địa danh du lịch nổi tiếng, thưởng thức đặc sản Phan Thiết hấp dẫn và trải nghiệm cùng chương trình team building kỳ thú.
Sáng ngày 07/11/2016, xe và hướng dẫn viên Đất Việt Tour đón đoàn tại trường, khởi hành đến Phan Thiết.
Trên đường đi, đoàn dùng điểm tâm sáng tại nhà hàng Cao Phát, Long Khánh, Đồng Nai sau đó tiếp tục lộ trình đến Phan Thiết. Trên xe, hướng dẫn viên tổ chức nhiều trò chơi vui nhộn như:  Đi tìm ẩn số, Truy tìm báu vật, Bí mật cuối cùng, Địa danh còn lại, Hành trình Đất Việt, Chiếc nón lạ kỳ,…Nhiều thành viên may mắn đã nhận được những phần quà bất ngờ từ ban tổ chức.
Đến Phan Thiết, đoàn tham quan dinh Vạn Thủy Tú - nơi trưng bày bộ xương cá ông lớn nhất Đông Nam Á. Đoàn tiếp tục đến Mũi Né. Trên đường đi đoàn được chiêm ngưỡng những vườn thanh long bạt ngàn.
Sau bữa cơm trưa tại nhà hàng Đại Dương, Phan Thiết đoàn nghỉ ngơi. Buổi chiều, đoàn tham quan lâu đài rượu vang. Tại đây, đoàn được thưởng thức những lý vang hảo hạng miễn phí.
Đoàn nhận phòng khách sạn Sealinks lưu trú, sau đó tham quan resort Sunny và Mường Thanh. Đoàn dùng cơm tối tại nhà hàng và nghỉ đêm tại khách sạn Sealinks.
Ngày thứ hai, đoàn học cách chuẩn bị tiệc buffet dưới sự hướng dẫn của các trưởng bộ phận và dùng buffet tại nhà hàng Sealinks.
Từ 07h00 – 11h00, sinh viên tập trung chia thành bốn nhóm. Nhóm 1 học và thực hành nghiệp vụ lễ tân. Các trưởng bộ phận lễ tân giới thiệu vai trò, công việc, trách nhiệm của bộ phận lễ tân đồng thời hướng dẫn quy trình làm thủ tục nhận phòng (check–in) và trả phòng (check out).
Nhóm 2 học và thực hành nghiệp vụ buồng/phòng. Bộ phận buồng/phòng giới thiệu về quy trình phục vụ buồng phòng. Sinh viên được quan sát và thực hành cách trải ga giường, tìm hiểu về quy trình dọn phòng và kiểm tra phòng khi khách trả phòng.
Nhóm 03 học và thực hành nghiệp vụ pha chế. Sinh viên được trưởng bộ phận bar, nhân viên bartender của nhà hàng  giới thiệu các loại rượu, cách pha chế nước ngọt, cocktail,…
Nhóm 04 học và thực hành nghiệp vụ nhà hàng. Bộ phận nhà hàng giới thiệu về các nhà hàng tại resort và hướng dẫn tham quan khu vực bếp, khu vực nhà hàng, khu vực phòng tiệc, hướng dẫn cách set up một số kiểu bàn ăn thông dụng. Đoàn dùng cơm trưa tại nhà hàng Sealinks, làm thủ tục trả phòng.
Buổi chiều, các bạn sinh viên tiếp tục làm thủ tục nhận phòng khách sạn thực tập tại các bộ phận.
16h30, đoàn tập trung ra bãi biển tham gia chương trình team building với nhiều trò chơi tập thể mang tinh thần đoàn kết, đồng đội cao.
Buổi tối, đoàn di chuyển đến nhà hàng Cánh Buồm Vàng, tham gia chương trình gala dinner Tổng Kết Hành Trình Tour.
Ngày thứ ba, đoàn dùng bữa sáng tại nhà hàng Sealinks, sau đó tự do tắm biển và nghỉ ngơi. 07h30, đoàn tiếp tục thực hành tại khách sạn.10h00, đoàn làm thủ tục trả phòng. Xe đưa đoàn đi tham quan trường Dục Thanh – bảo tàng Hồ Chí Minh.
Đoàn dùng cơm trưa tại nhà hàng Bình Minh và khởi hành về trường. Trên đường về, đoàn dừng chân tham quan và mua sắm đặc sản Phan Thiết tại những cơ sở sản xuất.
18h00, đoàn về đến trường, hướng dẫn viên Đất Việt Tour chia tay đoàn, kính chào và hẹn gặp lại thầy cô và các bạn sinh viên trong những chương trình tham quan sau.
Xem thêm hình ảnh đoàn trường Đại học Công nghệ TP.HCM tham quan Phan Thiết, tháng 11/2016 tại đây: https://goo.gl/TaC0Dq                
				Liên hệ đặt tour
				Trụ sở Tp.HCM: (08) 38 944 888Chi nhánh Bình Dương: (0650) 3844 690Chi nhánh Đồng Nai: (061) 3 889 888Hotline: 0989 120 120 - 0976 046 046
                            Tour khác
                                Chuyến tham quan của công ty Eurowindow
                                Công ty Thái Sơn SP tham quan Đà Lạt, tháng 10/2016
                                Công ty Toàn Phú tham quan Nha Trang tháng 11/2013
                                Công ty điện tử Asti tham quan Nha Trang, tháng 4/2015
                                Công ty Cảng Quốc tế Tân Cảng – Cái Mép tham quan Phan Thiết 8/2013
                        Trang chủGiới thiệuKhuyến mãiQuà tặngCẩm nang du lịchKhách hàngLiên hệ
                        Tour trong nướcTour nước ngoàiTour vé lẻTeambuildingCho thuê xeVisa - PassportVé máy bay
                        Về đầu trang
						Công ty Cổ phần ĐT TM DV Du lịch Đất Việt
						Trụ sở Tp.HCM: 198 Phan Văn Trị, P.10, Quận Gò Vấp, TP.HCM
						ĐT:(08) 38 944 888 - 39 897 562
						Chi nhánh Bình Dương: 401 Đại lộ Bình Dương, P.Phú Cường, Tp.Thủ Dầu Một, Bình Dương
						ĐT: (0650) 3 775 775
						Chi nhánh Đồng Nai: 200 đường 30 Tháng Tư, P. Thanh Bình, Tp. Biên Hòa
						ĐT:(061) 3 889 888 - 3 810 091
                        Email sales@datviettour.com.vn
