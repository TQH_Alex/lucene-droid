url:http://haidangtravel.com/tin-tuc/Banh-dau-xanh-Hoi-An-mon-ngon-ngay-Tet.html
Tittle :Bánh đậu xanh Hội An món ngon ngày Tết
content :Toggle navigation
                             Trang Chủ
                              Tour Du Lịch
                         Tour Trong Nước Tour Nước Ngoài
                              Tổ Chức Sự Kiện
                             Thuê Xe Du Lịch
                         Thuê Xe 4 Chổ Thuê Xe 7 Chổ Thuê Xe 16 Chổ Thuê Xe 29 Chổ Thuê Xe 35 Chổ Thuê Xe 45 Chổ
                             Khách Sạn
                              Cẩm nang du lịch
                              Tin tức du lịch
                              Bảng Giá Tour
                     1900 2011
                    08.3849.3838 - 0948.99.1080
                    Đăng Ký
                                            ×
                                            ĐĂNG KÝ
                                                        Tên đăng nhập (bắt buộc)
                                                        Nhập số điện thoại ( bắt buôt )
                                                        Nhập họ tên của bạn
                                                        Nhập mail của bạn
                                                        Nhập mật khẩu ( bắt buột )
                                                        Nhập lại mật khẩu ( bắt buột )
                                                        NamNữKhác
                                                        Địa chỉ nhà bạn
                                                        Ngày sinh của bạn
                                                 Nhận mail từ Hải Đăng Travel
                                                Đăng Ký
                        Đăng nhập
                                            ×
                                            ĐĂNG NHẬP
                                        Quên mật khẩu ?
                                        Đăng Ký
                                                Ghi nhớ mật khẩu
                                                Đăng nhập
                    Trang chủ
                 ›
                    Bánh đậu xanh Hội An món ngon ngày Tết
        Cẩm nang du lịch
                        KHÁM PHÁ
                    An GiangBắc CạnBắc GiangBạc LiêuBắc NinhBến TreBình DươngBình ĐịnhBình PhướcCà MauCần ThơCao BằngChâu ĐôcCôn ĐảoĐà LạtĐà NẵngĐăk LakĐăk NôngĐảo Bà LụaĐảo Bình BaĐảo Bình HưngĐảo Điệp SơnĐảo Nam DuĐiện BiênĐồng NaiĐồng ThápGia LaiHà GiangHà NộiHà TiênHà TĩnhHải PhòngHậu GiangHồ Chí MinhHòa BìnhKiên GiangKon TumLai ChâuLạng SơnLào CaiLong AnNam ĐịnhNghệ AnNha TrangNinh BìnhNinh ChữPhan ThiếtPhú QuốcPhú ThọPhú YênQuảng BìnhQuảng NamQuảng NgãiQuảng NinhQuảng TrịQuy NhơnSóc TrăngSơn LaTây NinhThái BìnhThái NguyênThanh HóaThừa Thiên HuếTiền GiangTrà VinhTuyên QuangVĩnh LongVĩnh PhúcVũng TàuYên Bái
                            Kinh nghiệm
                                                                            Du lịch trong nướcDu lịch nước ngoàiKinh Nghiệm Đi Biển Đảo
                            Ẩm Thực
                            Thư giản
                                                                            Truyện cườiTop du lịchẢnh đẹpChuyện lạHình chếPhượt viết
                            Du Lịch & Thời Trang
                            Tôi Viết
                                    Go
                                    Bánh đậu xanh Hội An món ngon ngày Tết
                                        19/11/2016Quảng Nam
                                    Chỉ còn mấy ngày nữa là sẽ đến Tết. Trong cái gió heo may tràn qua phố Hội, không khí tại các hộ gia đình sản xuất bánh đậu xanh càng thêm hối hả.
                                     
Những ngày tết, bánh đậu xanh luôn có mặt trong các gia đình tại Hội An. Ảnh: T.Ly
Những ngày này, rong ruổi qua con phố nhỏ, nơi các hộ gia đình đang làm bánh, ai cũng cảm nhận được mùi hương thơm thoảng thoảng của đậu xanh.
Nghề làm bánh đậu xanh ở Hội An có từ lâu đời, trước thế kỉ thứ 18. Loại bánh này đã từng được cư dân dùng để tiến vua. Nối tiếp truyền thống của cha ông để lại, bằng cái tâm với nghề các hộ gia đình đã biến những chiếc bánh đậu xanh bình dị trở thành đặc sản nổi tiếng trong và ngoài nước.
Đến thăm lò bánh của gia đình bà Trinh ở đường Lê Lợi, TP Hội An (Quảng Nam), lò bánh nổi tiếng nhất nơi phố cổ, không khí tết càng hiển hiện rõ. Hơn 50 năm giữ lửa nghề, chị Trần Thị Liễu – con dâu bà Trinh cho biết ngày nào gia đình bà cũng cho ra đời hàng trăm chiếc bánh để phục vụ du khách và người dân Hội An.
Gần tết khách hàng đặt mua bánh hơn gấp nhiều lần ngày thường nên lò phải thuê thêm nhiều lao động mới đủ lượng hàng để bán.
Trời vừa hửng sáng, các tay thợ đã vội vàng bắt đầu công đoạn làm bánh. Theo mô hình kinh tế hộ, cũng như các lò bánh khác, gia đình bà Trinh trực tiếp quán xuyến tất cả các công đoạn. Mỗi công đoạn đều đòi hỏi người thợ phải khẻo léo, tỉ mẩn. Từ chọn nguyên liệu đến sấy khô, đóng gói cũng không hề dễ dàng.
Để đậu xanh cho ráo trước khi đem xay nhuyễn. Ảnh: T.Ly
Cho đậu xanh vào nồi nấu chín thật mềm. Ảnh: T.Ly
Đầu tiên, đậu xanh phải chọn cho được loại hạt nhỏ, ruột vàng. Đậu mang đi ngâm, vo sửa sạch trước khi bắt lên luộc trên bếp lửa. Đậu chín tiếp tục cho vào cối hoặc máy nghiền nát thành bột.
Tiếp tục ngào bột với nước đường rồi nhồi trộn bột từ từ từng ít một, để bột vừa đủ độ ẩm, không được ướt hoặc khô quá vì như vậy bánh sẽ không chắc và khó kết dính. Bột sau khi ngào xong tiếp tục ủ qua một đêm để bột khô hơn.
Riêng bánh đậu xanh mặn có thêm phần nhưn bánh làm bằng mỡ heo. Mỡ được rán vừa độ lửa sao cho thật khéo léo, nếu mỡ rán quá già, bánh sẽ có mùi khét; nếu mỡ quá non bánh sẽ mất mùi thơm và có vị ngậy.
Tuy nhiên, để có được chiếc bánh đậu xanh “đặc hạng” như ở Hội An, phần nhưn bánh còn trộn thêm gia vị đường, muối, tiêu… theo tỉ lệ hợp lí.
Cuối cùng là in bánh. Thợ in bánh trên khuôn đồng dạng tròn hoặc vuông. Cho bột vào đầy các lỗ khuôn, thêm nhưn (nếu là bánh mặn) ở giữa, ém chặt bột, úp ngược mặt khuôn xuống và gõ nhẹ đáy khuôn để lấy bánh ra.
Đậu xanh nấu chín cho vào máy xay nhuyễn thành bột. Ảnh: T.Ly
Bột được cho vào khuôn để in thành bánh. Ảnh: T.Ly
Trước khi cho vào phong bao, đem bánh sấy chín. Ảnh: T.Ly
Trước khi cho vào phong bao, bánh đem sấy chín lần nữa cho có độ giòn và thơm. Chỉ cần vài ba động tác khéo léo từ đôi bàn tay người thợ đã cho ra đời những chiếc bánh đậu xanh vừa thơm lựng lại đẹp mắt.
Có lẽ không mấy ai còn lạ lẫm với bánh đậu xanh nhưng bánh đậu xanh Hội An thì chắc không phải ai cũng có dịp được thưởng thức.
Vẫn chỉ là những nguyên liệu vườn quê như bao nơi khác, nhưng bánh đậu xanh Hội An lại mang một phong vị, một đặc trưng riêng của vùng đất xứ Quảng nhỏ bé hiền hậu. Thế nên cứ mỗi độ tết đến xuân về, người con phố Hội đi xa vẫn cứ nhớ hoài mùi hương đậu xanh phảng phất.
Và mỗi du khách khi đến Hội An, cũng tìm cách để được thưởng thức hay tận mắt chứng kiến cách làm những chiếc bánh nhỏ nhắnđể lại hăm hở mua để về làm quà cho người thân.
In hạn sử dụng cho bánh. Ảnh: T.Ly
Du khách tìm mua để thưởng thức hoặc làm quà gửi cho người thân. Ảnh: T.Ly
        Đọc Nhiều Nhát
                Kỷ Niệm Đà Lạt 
                Chương trình khuyến mãi tour du lịch tết 2016
                Bảng Giá Tour Lễ 30 Tháng 4 Và Giỗ Tổ
        Bài viết mới nhất
                    Biển Của Người Xứ Nẫu
                    Quy Nhơn Thiên Đường Của Biển
                    Ôi Nha Trang
        Cẩm Nang Du Lịch
         Cẩm nang Ninh Chữ Cẩm nang Nha Trang Cẩm nang Đà Lạt Cẩm nang Phú Yên Cẩm nang Phú Quốc Cẩm nang Quảng Ngãi Cẩm nang Quảng Nam Cẩm nang Phú Thọ Cẩm nang Phan Thiết Cẩm nang Quảng Bình Cẩm nang Ninh Bình Cẩm nang Lạng Sơn
        Kinh nghiệmThư giản
                    Du lịch trong nướcDu lịch nước ngoàiKinh Nghiệm Đi Biển Đảo
                    Phượt viếtHình chếChuyện lạ
                    Ảnh đẹpTop du lịchTruyện cười
    Ảnh Khách Hàng
                    TRỤ SỞ CHÍNH
                         225 Bàu Cát, P12, Q.Tân Bình, HCM
                         19002011 - 08 3849 3838 
                        08 3849 2999   
                          www.haidangtravel.com  
                         info@haidangtravel.com 
                        CSKH: 0948.99.1080
                    CHI NHÁNH
                         Quận 1 : Lầu 7, Tòa Nhà Viễn Đông, 14 Phan Tôn
                        19002011(Ext-107) -  0838204589  
                         Quận 12 : 256 TX25, Thạnh Xuân, Q12 
                        19002011(Ext-111)
                         Biên Hòa : 51A2, KP11, P.Tân Phong 
                        19002011(Ext-108) -  0613999392  
                    TRỢ GIÚP
                    Liên hệ & giới thiệuĐiều khoản chungHình thức thanh toánHướng dẫn mua tourChính sách bảo mật
                        GIỜ LÀM VIỆC
                            Thứ 2 đến 6
                                Sáng: 08:00-12:00Chiều:13:30-17:30
                            Thứ 7 Sáng: 08:00-12:00Nghỉ chủ nhật, lễ, tết
                    Được tìm kiếm nhiều
                        Tour Nha Trang
                        Tour Đà Lạt
                        Tour Cà Mau
                        Tour Phú Yên
                        Tour Ninh Chữ
                        Tour Côn Đảo
                        Tour Miền Bắc
                        Tour Phú Quốc
                        Tour Đà Nẵng
                        Tour Phan Thiết
                        Tour Bình Ba
                        Tour Nam Du
                        Tour Vũng Tàu
                        Tour Bà Lụa
                THÀNH TÍCH
            2015 © All Rights Reserved. Công ty cổ phần xây dựng du lịch Hải Đăng
        Your browser doesn't support canvas. Please download IE9+ on
            IE Test Drive
            +
                Có thể bạn quan tâm ?
                                Tour Đảo Hòn Sơn Khám Phá Đỉnh Ma Thiên Lãnh 2N2Đ
                                 1,390,000 đ
                                Tour Ninh Chữ - Vĩnh Hy - Cổ Thạch đồng giá 2N2Đ
                                 999,000 đ
                                Tour Côn Đảo, Khám Phá Lịch Sử Văn Hóa Côn Sơn 2N1Đ
                                 4,590,000 đ
                                Tour Đà Lạt Phototrip Khám Phá Những Cung Đường Hoa 3N3Đ
                                 1,250,000 đ
                                Tour Ninh Chữ - Vĩnh Hy Khám Phá Bãi Đá Bảy Màu Resort 3sao 2N2Đ
                                 1,250,000 đ
                                Tour du lịch Vũng Tàu, Long Hải, Suối nước nóng Bình Châu 2N1Đ
                                 789,000 đ
                                Tour Côn Đảo, Trọn Gói Vé Máy Bay 2N1Đ
                                 3,999,000 đ
                                Tour Ninh Chữ, Vĩnh Hy Hang Rái Siêu Tiết Kiệm 2n2d
                                 850,000 đ
                                Tour Đảo Phú Quý, Khám Phá Hòn Đảo Mang Vẻ Đẹp Hoang Sơ 3N3Đ
                                 2,250,000 đ
                                Tour Đảo Bình Hưng, Hang Rái, Vĩnh Hy 2n2d
                                 1,111,000 đ
