url:http://dulich.vnexpress.net/tin-tuc/viet-nam/anh-tay-di-bo-khap-the-gioi-me-man-nui-rung-viet-nam-3500086.html
Tittle :Anh Tây đi bộ khắp thế giới mê mẩn núi rừng Việt Nam - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
                Các địa danh khác
		        24h qua
            Du lịch Việt Nam 
                                        Du Lịch 
                                        Việt Nam 
                                        Các địa danh
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Thứ năm, 17/11/2016 | 08:38 GMT+7
|
						Anh Tây đi bộ khắp thế giới mê mẩn núi rừng Việt Nam												
					Khám phá những rặng núi đá vôi hùng vĩ ở miền tây Quảng Bình, Meigo Mark, chàng trai Estonia đi bộ du lịch qua 19 nước cho hay anh bị mê mẩn với cảnh đẹp nơi đây.
	Meigo 26 tuổi, đến từ đất nước Bắc Âu Estonia. Anh tự giới thiệu quê hương chỉ bằng 1/7 diện tích Việt Nam và có hơn một triệu dân. Đặt chân vào Việt Nam từ đầu tháng 11/2016 qua cửa khẩu quốc tế Lao Bảo (Quảng Trị), Meigo cho hay đây là quốc gia thứ 19 anh đặt chân đến.
					Tấm bản đồ giấy đầu tiên của Meigo Mark và hành trình hiện tại ở Việt Nam. Ảnh: Hoàng Táo
	Du lịch bằng cách đi bộ
	Điều đặc biệt khiến nhiều người ngạc nhiên là chàng trai này khám phá thế giới hoàn toàn bằng… đi bộ. “Tôi đi bộ được 12.530 km suốt 2 năm 6 tháng qua. Việc du lịch của tôi rất rẻ. Tôi hoàn toàn đi bộ, không sử dụng hay đi nhờ xe máy, xe ôtô hay phương tiện khác…, ngoại trừ đi thuyền”, Meigo nói.
	Khởi hành vào ngày 11/5/2014 tại thủ đô Tallinn của Estonia, Meigo lên kế hoạch vòng quanh thế giới bằng đi bộ trước đó 6 tháng. Mang theo một ba lô nhỏ với 2 áo mưa, một lều ngủ chống thấm, một máy ảnh bỏ túi nhỏ, một máy ghi âm cùng vài bộ áo quần, Meigo lên đường khám phá thế giới.
	Một năm sau, Meigo đã bán căn nhà nhỏ của mình để có kinh phí tiếp tục chuyến đi. Anh cũng tổ chức quyên góp trên mạng để có thêm kinh phí thực hiện ước mơ.
					Anh dự định viết sách sau khi kết thúc hành trình 9 năm, khám phá 45 nước và vượt quãng đường 40.000 km bằng đi bộ. Ảnh: Hoàng Táo
	Việt Nam với những ấn tượng ban đầu
	Meigo dự định khám phá Việt Nam trong 2 tháng. Từ cửa khẩu Lao Bảo, anh theo quốc lộ 9 về thăm biển Cửa Việt ở Quảng Trị. Tại đây, Meigo mua tờ bản đồ giấy đầu tiên. “Nhiều người Việt Nam không biết Estonia nên tôi mua bản đồ và vẽ lại hành trình của mình”, Meigo thông tin trước kia chỉ dùng điện thoại thông minh để dò đường.
	Đặt chân đến biển Cửa Việt, Meigo thốt lên vì không tin rằng đã đặt chân đến Thái Bình Dương từ Bắc Âu chỉ bằng việc đi bộ. Sau đó, anh theo quốc lộ 1A ra TP Đồng Hới (Quảng Bình).
	Khám phá Phong Nha trong 2 ngày, Meigo đã đi thuyền trên sông Son, thăm động Phong Nha, làng quê và những rặng núi đá vôi.
	Meigo cho hay mê mẩn với cảnh núi rừng bình yên và hùng vĩ tại đây. Khám phá Việt Nam trong 2 tuần qua, anh đã kịp làm quen với một số người bạn, được họ mời về nhà và thưởng thức các món ăn bản địa. Meigo khá ấn tượng với món bún, ăn canh và nhớ khá kỹ về một món nấm.
	Anh chia sẻ mỗi ngày thường đi 25-50 km, trong 5-11 tiếng, và ngồi nghỉ vài phút khi mệt. Đôi khi, chàng trai ghé thăm các gia đình bản địa, đề nghị họ hát tặng, hoặc kể một câu chuyện rồi ghi âm lại. “Khi gặp những người trẻ biết tiếng Anh, tôi nhờ họ dịch lại các câu chuyện trên”, Meigo nói.
	Thú nhận giao thông ở quốc lộ 1A “rất hỗn loạn” với quá nhiều phương tiện khác nhau, Meigo đã lựa chọn đường mòn Hồ Chí Minh để tiếp tục hành trình ra Hà Nội.
	Ước mơ chinh phục 45 nước
	Hai quốc gia gần nhất trước khi anh đặt chân vào Việt Nam là Lào và Campuchia với thời gian 6 tuần. Ấn Độ là đất nước khiến chàng trai tiêu tốn nhiều thời gian nhất đến nay, với một năm 22 ngày, vượt qua chặng đường 4.700 km.
	Meigo nói anh cắm trại nghỉ đêm dọc đường, hòa mình vào thiên nhiên. Tại một tỉnh ở Ấn Độ, anh đã nghe được tiếng gầm gừ của một con báo khi ngủ đêm giữa rừng. Cũng không ít lần, anh ngủ giữa trời tuyết.
	“Du lịch hiện nay thật dễ dàng với một thiết bị thông minh có GPS. Điều này quả khó khăn nếu thực hiện vào 20 năm trước”, anh nói.
	Ở mỗi đất nước đặt chân đến, Meigo kết bạn và được nhiều người hỗ trợ việc ăn nghỉ. “Nhiều người địa phương rất yêu mến tôi, họ mời thức ăn và chỗ nghỉ”, anh cung cấp thêm đã có 140 gia đình giúp anh trong hành trình dài đã qua. Meigo cũng kịp làm quen với 15 người khác từng du lịch vòng quanh thế giới bằng nhiều cách.
					Chàng trai bảo rất yêu phong cảnh bình yên của làng quê và núi rừng Việt Nam. Ảnh: Hoàng Táo
	Đi qua nhiều quốc gia, anh chưa gặp khó khăn gì nhiều trong giấy tờ để nhập cảnh vào quốc gia tiếp theo, dù mỗi nơi có những thủ tục khác nhau.
	Đất nước tiếp theo trong hành trình của Meigo là Trung Quốc, với thời gian ít nhất khoảng 6 tháng. “Sau đó, tôi đi tàu thủy sang Hàn Quốc, Nhật Bản, Canada, khám phá nhiều quốc gia ở Nam Mỹ, tới Nam Phi và đi theo hướng bắc để trở về Estonia”, Meigo chia sẻ, “Hành trình này sẽ còn kéo dài ít nhất 7 năm nữa”.
	Hành trình kéo dài và không ngắt quãng, mẹ và chị gái của Meigo đã bay đến Thổ Nhĩ Kỳ, Nepal và Hy Lạp để thăm anh. Meigo cũng giữ liên hệ với gia đình qua email và mạng xã hội.
	“Gia đình tôi cũng yêu du lịch, họ hỗ trợ và khuyến khích tôi thực hiện chuyến đi”, Meigo tâm sự. Đặt mục tiêu chinh phục 45 nước với 40.000 km trong 9 năm, chàng trai nói sẽ viết sách sau khi kết thúc hành trình.
	“Hy vọng khi đó tôi sẽ quay lại Việt Nam, nhưng bằng phương tiện khác, chứ đi bộ thì phải rất lâu”, Meigo cười nói.
	Xem thêm: Chàng trai đi qua 96 quốc gia đến Việt Nam
	Hoàng Táo                        
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
                                                Khách sạn509Nhà hàng63Tham quan365Giải trí82Mua sắm27                        
                Những chuyến du lịch vòng quanh thế giới
                    Chuyện tình du khách: Gặp một lần, yêu cả đời (5/8)
                                             
                    Du khách bay khắp thế giới bằng khinh khí cầu lập kỷ lục mới (28/7)
                                             
                    Người đàn ông 60 tuổi đạp xe quanh thế giới chưa biết ngày về (8/5)
                                             
                    Những điểm đến ngày càng nổi tiếng trên thế giới (15/12)
                                             
                    Du lịch sang chảnh như hoàng tử Anh Harry (29/11)
                                             
                Xem thêm
            Xem nhiều nhất
                            Việt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật Bản
                                                             
                                Thủ tướng chỉ ra 3 giải pháp để phát triển du lịch
                                                                     
                                Những món đồ không thể thiếu khi đi du lịch cuối năm
                                Vị đậm đà tinh tế trong món gà sốt tiêu tỏi
                                Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịch
                                                                     
                 
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                Tags
                                    Du lịch bằng đi bộ
                                Khám phá thế giới
                                Việt nam
                                Phong nha
    Tin Khác
                                                     
                            Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                                                             
                            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịch
                                                             
                            Việt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật Bản
                                                             
                            Không gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
                                    Đầm Sen ưu đãi ngày Nhà giáo Việt Nam
                                    Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                                                             
                                    Hoàng tử Anh ăn đêm bên Hồ Gươm
                                    Những món đồ không thể thiếu khi đi du lịch cuối năm
                                    Khu cắm trại giữa rừng dương cách Sài Gòn 3 giờ chạy xe
                                                                             
                                    'Vua đầu bếp' để khách tự sáng tạo món ăn
                                                                             
                                    Thủ tướng chỉ ra 3 giải pháp để phát triển du lịch
                                                                             
                                    Vị đậm đà tinh tế trong món gà sốt tiêu tỏi
                                    Món ăn đặc trưng trong ngày lễ Tạ ơn
                                    Những quán ăn không biển hiệu vẫn đông khách ở Hà Nội
                                                                             
        Khuyến mãi Du lịch
		  prev
		   next
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
         Gửi bài viết về tòa soạn
        Ẩm thực
         
                             
            Quán bún măng vịt mỗi ngày chỉ bán một tiếng
            Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
                Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
                'Vua đầu bếp' để khách tự sáng tạo món ăn
        Cộng đồng
         
                'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                        Hãy lên kế hoạch du lịch đúng thời điểm cho năm mới dựa theo lịch trình du lịch khắp miền đất nước.
            Du khách đứng tim vì voi chặn đầu xe5 vùng đất Samurai nổi tiếng của Nhật BảnĐồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
        Ảnh
         
                             
            Chùa 've chai' nắm giữ nhiều kỷ lục ở Đà Lạt
            Tên gọi “ve chai” của chùa Linh Phước (Đà Lạt) xuất phát từ việc hầu hết chi tiết trang trí ở đây đều dùng mảnh sành, sứ hay vỏ chai tạo nên.
                                     
                Ngày Tết du ngoạn Thiền viện Trúc Lâm Tây Thiên
                                     
                Những ngôi nhà mái rạ ở 'thủ phủ' gạo tám
        Video
         
            Du khách la hét vì chơi tàu lượn dốc nhất thế giới
            Tàu lượn Takabisha (Nhật Bản) có độ dốc lên tới 121 độ, vận tốc tối đa đạt 160 km/h khiến người chơi như đứng tim khi nhiều đường ray thẳng đứng, tàu như rơi tự do.
                Cả công viên 'nín thở' chờ du khách béo trượt máng
                Cảnh đẹp ngoạn mục trên khắp nẻo đường Na Uy
        Điểm đến yêu thích                
                    Kế hoạch 4 ngày khám phá đảo thiên đường ở Campuchia                
                    Những món ngon nên thử của Phú Quốc                
                    Khách du lịch được mời dùng toilet miễn phí ở Đà Nẵng                
                    Chuyến bay Cần Thơ - Đà Lạt chỉ mất một giờ                
                    Những cây cầu có tên gọi độc đáo ở miền Tây                
		Liên kết du lịch
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
