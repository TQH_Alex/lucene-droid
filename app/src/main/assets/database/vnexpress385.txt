url:http://dulich.vnexpress.net/tin-tuc/viet-nam/tong-cuc-du-lich-khach-tay-au-tang-truong-chua-tung-co-3492734.html?utm_campaign=boxtracking&utm_medium=box_topic&utm_source=detail
Tittle :Tổng cục Du lịch: Khách Tây Âu tăng trưởng chưa từng có - VnExpress Du lịch
content : 
                     Trang chủ Thời sự Góc nhìn Thế giới Kinh doanh Giải trí Thể thao Pháp luật Giáo dục Sức khỏe Gia đình Du lịch Khoa học Số hóa Xe Cộng đồng Tâm sự Rao vặt Video Ảnh  Cười 24h qua Liên hệ Tòa soạn Thông tin Tòa soạn
                            © Copyright 1997- VnExpress.net, All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                         Hotline:
                            0123.888.0123 (Hà Nội) 
                            0129.233.3555 (TP HCM) 
                                    Thế giớiPháp luậtXã hộiKinh doanhGiải tríÔ tô - Xe máy Thể thaoSố hóaGia đình
                         
                     
                    Thời sựThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
    Việt NamQuốc tếCộng đồngẢnh
                Các địa danh khác
		        24h qua
            Du lịch Việt Nam 
                                        Du Lịch 
                                        Việt Nam 
                                        Các địa danh
                                 
                                Việt NamQuốc tếCộng đồngẢnh
						Thứ sáu, 4/11/2016 | 08:23 GMT+7
|
						Tổng cục Du lịch: Khách Tây Âu tăng trưởng chưa từng có												
					Mức tăng trưởng trung bình trên 20% của khách Tây Âu theo Tổng cục Du lịch đánh giá là chưa từng có với du lịch Việt Nam, cho thấy hiệu quả của việc miễn visa.
					Hà Nội mở 4 tour miễn phí cho khách du lịch  /  Khách Việt đi du lịch Hàn Quốc tăng đột biến dịp mùa thu                    
	Trao đổi với VnExpress, ông Nguyễn Văn Tuấn, Tổng cục trưởng Du lịch Việt Nam cho biết mới 10 tháng đầu năm nhưng lượng khách quốc tế đã đạt bằng cả năm 2015. Đây là mức tăng cao cả về tốc độ tăng và giá trị tuyệt đối.
	- Thưa Tổng cục trưởng, lượng khách quốc tế trong 10 tháng qua đạt hơn 8 triệu lượt, tăng hơn 25% so với cùng kỳ năm ngoái, kết quả này có được do đâu?
	Du lịch Việt Nam vừa trải qua thời kỳ sụt giảm lượng khách và gặp nhiều khó khăn suốt 13 tháng kể từ vụ giàn khoan 981 năm 2014. Sáu tháng cuối năm 2015 mới bắt đầu phục hồi. Đây là sự tăng ít nhiều mang tính quy luật, chu kỳ sau khi sụt giảm sẽ tăng. 
	Khi tình hình ổn định trở lại, du lịch Việt Nam có điều kiện tốt để phục hồi. Đó là nhờ sự quan tâm, chỉ đạo của Chính phủ và Thủ tướng Chính phủ nhằm tạo điều kiện cho du lịch phát triển. Điển hình là đề án phát triển du lịch thành ngành kinh tế mũi nhọn, Quỹ phát triển du lịch, cấp thí điểm visa điện tử, visa tại cửa khẩu cho khách du lịch quốc tế...
	Các địa phương chấn chỉnh, khắc phục hạn chế, yếu kém về quản lý điểm đến, đảm bảo an ninh, an toàn cho khách du lịch. Bộ Văn hóa Thể thao và Du lịch (VH-TT-DL) cũng tiến hành chiến dịch khắc phục những hạn chế yếu kém từ ngay nội bộ ngành du lịch, quản lý chất lượng cơ sở lưu trú, lữ hành và hướng dẫn viên.
					"Quý 1, khách quốc tế tăng 19-20%, quý 2 tăng 22-23% và quý 3 tăng 25%. Nếu năm nay, điều kiện thuận lợi, không có những biến cố bất thường vào hai tháng còn lại của năm thì khả năng du lịch Việt Nam sẽ đạt được 9,6 đến 9,7 triệu lượt khách, tăng 22-23% so với cả năm 2015. Chúng ta sắp cán đích gần 10 triệu khách du lịch quốc tế. Đây thực sự là một dấu mốc đối với ngành du lịch", ông Nguyễn Văn Tuấn (phải) chia sẻ.
	- Vậy tự thân ngành du lịch đã có những bước chuyển biến nào?
	Quá trình thu hút đầu tư, tích tụ nội lực của ngành du lịch về cơ sở hạ tầng, cơ sở vật chất kỹ thuật, dịch vụ đã tăng lên rất nhiều trong 5 năm vừa qua. Đặc biệt là các dự án có quy mô lớn, chất lượng cao bởi các nhà đầu tư chiến lược như Vingroup, Sungroup, FLC, Mường Thanh, Tuần Châu, Bitexco... tạo ra năng lực cạnh tranh đáng kể cho du lịch Việt Nam.
	Đồng thời là sự vào cuộc của các địa phương, nhất là các địa bàn du lịch trọng điểm như Hà Nội, TP HCM, Quảng Ninh, Đà Nẵng, Khánh Hòa, Phú Quốc... Có thể nói năm qua du lịch đã vượt qua giai đoạn khó khăn, suy giảm, và duy trì được đà tăng trưởng tương đối cao, ổn định, phản ánh năng lực cạnh tranh của chúng ta đã có bước cải thiện.
	- Ngoài sự tăng trưởng về tổng khách quốc tế đến Việt Nam, có điểm gì khác biệt so với mọi năm không, thưa ông?
	Hầu như các thị trường khách của chúng ta đều tăng, thị trường xa từ các nước Tây Âu được miễn visa tăng rất cao, khoảng 20%. Việc tăng tỷ lệ 20% với những thị trường xa như Tây Âu là điều hiếm có và chưa từng có ở Việt Nam, điều đó cho thấy chính sách miễn visa đã tác động trực tiếp như thế nào.
	Một hiện tượng là năm ngoái, lần đầu tiên khách du lịch Hàn Quốc vượt qua con số một triệu lượt, năm nay dự kiến khách du lịch Hàn Quốc sẽ đạt 1,5 triệu lượt và trở thành thị trường rất quan trọng của du lịch Việt Nam.
	Khách Trung Quốc sau một giai đoạn sụt giảm nhưng năm nay cũng tăng rất mạnh, mang tính bứt phá và chúng ta còn có thể tiếp tục khai thác. 
	- Ông đánh giá như thế nào về tầm quan trọng của khách Trung Quốc đối với du lịch Việt Nam?
	Với dân số là 1,4 tỷ người, nhu cầu du lịch ra nước ngoài đang tăng lên rất nhanh, dự kiến là 120 triệu lượt năm 2016. Không quốc gia nào quan tâm phát triển du lịch mà không quan tâm đến thị trường khách Trung Quốc. Họ có khả năng chi tiêu rất cao, đặc biệt cho mua sắm và ăn uống.  
	Lượng khách Trung Quốc đến nước ta năm nay dự kiến đạt 2,6 triệu lượt, tăng cao so với các năm. Nhưng con số này vẫn rất khiêm tốn so với 9 triệu khách Trung Quốc đến Thái Lan, 4 triệu khách đến Nhật Bản, Hàn Quốc mặc dù chúng ta có điều kiện thuận lợi hơn về khoảng cách, sự khác biệt về khí hậu, sản phẩm du lịch, hàng hóa có thể cung ứng. Chúng ta phải xác định, thị trường Trung Quốc là thị trường quan trọng hàng đầu của du lịch Việt Nam. Chúng ta không thể bỏ qua, mà phải tìm cách khai thác tốt để tăng trưởng mạnh mẽ hơn nữa số lượng khách Trung Quốc nhưng gắn với công tác quản lý.
	- Vậy làm gì để vẫn giữ được đà tăng trưởng khách Trung Quốc mà giải quyết tốt các vấn đề bất cập xảy ra trong thời gian qua, thưa ông?
	Thứ nhất là hợp tác chặt chẽ với Trung Quốc theo hướng là khuyến khích khách Trung Quốc đến Việt Nam và tăng cả khách Việt Nam đến Trung Quốc, cùng quản lý, kiểm soát chất lượng dịch vụ, đảm bảo quyền lợi cho khách. Chỉ khi khách hài lòng, họ mới quay lại và tuyên truyền, giới thiệu cho những người bạn, người quen của họ đến Việt Nam.
	Thứ hai, đẩy mạnh hơn nữa việc quảng bá, xúc tiến. Trong suốt các tháng vừa qua, chúng tôi đã làm việc này rất nhiều. Từ nay đến cuối năm, chúng tôi sẽ tiếp tục mở các chiến dịch phát động đến tất cả các thành phố lớn Trung Quốc và hợp tác với phía bạn đưa các công ty lữ hành, hãng báo chí tiếp tục sang khảo sát Việt Nam để tuyên truyền quảng bá trên diện rộng, cung cấp thông tin cho khách Trung Quốc.
	Thứ ba, kiểm soát tốt hơn điểm đến, đảm bảo an ninh, an toàn, quản lý được khách du lịch Trung Quốc theo hướng đi vào nề nếp. Thời gian qua xảy ra các vấn đề mà bước đầu đã được khắc phục nhưng vẫn phải quan tâm nhiều hơn nữa đến công tác quản lý.
	Thứ 4, đa dạng hóa dịch vụ, tạo ra nhiều cơ sở mua sắm một cách minh bạch và trung thực với chất lượng tốt để cung ứng hàng hóa cho khách du lịch Trung Quốc. Cùng với đó tăng cường quản lý hoạt động của các công ty lữ hành, hướng dẫn viên theo quy định của pháp luật. Đồng thời có những chính sách mềm dẻo, linh hoạt hơn để đáp ứng được nhu cầu của thị trường này khi tăng cao đột biến trong một số thời điểm.
					Khách đến từ các nước Tây Âu cũng liên tục tăng từ khi được miễn visa.
	- Với khách quốc tế nói chung, Phó Thủ tướng Vương Đình Huệ từng chỉ ra 7 nỗi sợ khiến họ không dám quay lại Việt Nam. Theo ông, những nỗi sợ này đã được giải quyết đến đâu trong thời gian qua?
	Tôi cho rằng năm 2016 là năm có bước chuyển biến khá tích cực trong vấn đề này, điển hình là ở TP HCM, Hà Nội, Đà Nẵng, Hội An, Thanh Hóa.
	Sự thành công của Thanh Hóa chính là việc chấn chỉnh và lập lại trật tự ở Sầm Sơn. Một nơi phức tạp như Sầm Sơn mà lập lại được trật tự thì tôi tin ở đâu cũng có thể làm được, nếu quyết tâm và có những giải pháp đúng, có sự đồng thuận trong nhân dân, cộng đồng doanh nghiệp. Tuy nhiên mức độ thành công, quyết liệt ở các địa phương có sự khác nhau. Đây là vấn đề lâu dài, thường xuyên, bởi chúng ta chỉ cần buông thì nó có thể lập tức trở lại.
	Để giải quyết những nỗi sợ của khách nước ngoài đến Việt Nam cần sự vào cuộc của các cấp chính quyền địa phương. Giải quyết những vấn đề tại điểm đến thì cơ quan quản lý ở Trung ương cũng như Thủ tướng chính phủ không thể làm thay địa phương các cấp được. Trách nhiệm của chúng tôi là tham mưu, đề xuất các giải pháp, kiểm tra, nắm bắt thông tin, có ý kiến với các địa phương để chấn chỉnh.
	- Trong 2 tháng cuối năm, du lịch Việt Nam dự kiến sẽ làm gì để tiếp tục duy trì đà tăng trưởng?
	Để đạt được 9,6 triệu lượt khách trong năm nay, chúng tôi cho rằng nó hoàn toàn nằm trong tầm tay. Chúng tôi còn kỳ vọng hơn thế nữa, việc cần làm là đẩy mạnh quảng bá xúc tiến, tập trung, có hiệu quả vào các thị trường trọng điểm. Tiếp tục chấn chỉnh, khắc phục những vấn đề tại các điểm đến, làm sản phẩm thực sự đồng bộ, hấp dẫn. 
	Thứ ba, tiếp tục chiến dịch kiểm soát chất lượng dịch vụ trong hệ thống khách sạn và cơ sở lưu trú, các nhà hàng. Đây là công việc chúng tôi làm rất quyết liệt, và thành công. Trong thời gian qua, Tổng cục Du lịch đã thu hồi quyết định công nhận hạng sao đối với 22 khách sạn 4 sao và 3 sao trên khắp cả nước. 
	Xem thêm: Việt Nam không kỳ thị khách du lịch Trung Quốc                         
                                    Vy An
                                                                              |  
									Gửi bài viết chia sẻ tại đây hoặc về dulich@vnexpress.net
                                                Khách sạn509Nhà hàng63Tham quan365Giải trí82Mua sắm27                        
                Tin du lịch Việt Nam
                    Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịch (19/11)
                                             
                    Việt Nam mang chùa Cầu đến hội chợ du lịch London (9/11)
                                             
                    Hai khách sạn ở Nha Trang và Phú Quốc bị thu hồi sao (20/9)
                                             
                    Du khách có cơ hội chụp bức ảnh selfie lớn nhất Việt Nam (17/8)
                                             
                    7 nỗi sợ khiến khách quốc tế không trở lại Việt Nam (10/8)
                                             
                Xem thêm
            Xem nhiều nhất
                            Anh Tây đi bộ khắp thế giới mê mẩn núi rừng Việt Nam
                                                             
                                Việt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật Bản
                                                                     
                                Thủ tướng chỉ ra 3 giải pháp để phát triển du lịch
                                                                     
                                Những món đồ không thể thiếu khi đi du lịch cuối năm
                                Vị đậm đà tinh tế trong món gà sốt tiêu tỏi
                 
                Ý kiến bạn đọc (0) 
                Mới nhất | Quan tâm nhất
                Mới nhấtQuan tâm nhất
            Xem thêm
                        Ý kiến của bạn
                            20/1000
                                    Tắt chia sẻĐăng xuất
                         
                                Ý kiến của bạn
                                20/1000
                                 
                Tags
                                    Du lịch việt nam
                                Khách trung quốc
                                Tăng trưởng
                                8 triệu lư
    Tin Khác
                                                     
                            Quán bún măng vịt mỗi ngày chỉ bán một tiếng
                                                             
                            Bắc Trung Bộ hút khách Thái Lan để vực dậy du lịch
                                                             
                            Việt Nam đứng top đầu khách chi tiêu nhiều nhất ở Nhật Bản
                                                             
                            Không gian điện ảnh đẳng cấp tại Vinpearl Đà Nẵng
                                    Đầm Sen ưu đãi ngày Nhà giáo Việt Nam
                                    Đồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
                                                                             
                                    Hoàng tử Anh ăn đêm bên Hồ Gươm
                                    Những món đồ không thể thiếu khi đi du lịch cuối năm
                                    Khu cắm trại giữa rừng dương cách Sài Gòn 3 giờ chạy xe
                                                                             
                                    'Vua đầu bếp' để khách tự sáng tạo món ăn
                                                                             
                                    Thủ tướng chỉ ra 3 giải pháp để phát triển du lịch
                                                                             
                                    Anh Tây đi bộ khắp thế giới mê mẩn núi rừng Việt Nam
                                                                             
                                    Vị đậm đà tinh tế trong món gà sốt tiêu tỏi
                                    Món ăn đặc trưng trong ngày lễ Tạ ơn
        Khuyến mãi Du lịch
		  prev
		   next
                                Tour Nhật Bản đón năm mới giá chỉ 28,9 triệu đồng
                                        28,900,000 đ 35,900,000 đ
                                        17/11/2016 - 14/12/2016                                    
                                                                Hành trình sẽ đưa du khách đến cầu may đầu xuân ở đền Kotohira Shrine, mua sắm hàng hiệu giá rẻ dưới lòng đất, tham 
                                Vietrantour tặng 10 triệu đồng tour Nam Phi 8 ngày
                                        49,900,000 đ 59,900,000 đ
                                        21/10/2016 - 01/12/2016                                    
                                                                Tour 8 ngày Johannesburg - Sun City - Pretoria - Capetown, bay hàng không 5 sao Qatar Airways, giá chỉ 49,99 triệu đồng.
                                Tour Phú Quốc - Vinpearl Land gồm vé máy bay từ 2,9 triệu đồng
                                        2,999,000 đ 4,160,000 đ
                                        16/11/2016 - 30/12/2016                                    
                                                                Hành trình trọn gói và Free and Easy tới Phú Quốc với nhiều lựa chọn hấp dẫn cùng khuyến mại dịp cuối năm dành cho 
                                Tour ngắm hoa tam giác mạch từ 1,99 triệu đồng
                                        1,990,000 đ 2,390,000 đ
                                        08/10/2016 - 30/11/2016                                    
                                                                Danh Nam Travel đưa du khách đi ngắm hoa tam giác mạch tại Hà Giang 3 ngày 2 đêm với giá trọn gói từ 1,99 triệu đồng.
                                Tour Campuchia 4 ngày giá chỉ 8,7 triệu đồng
                                        8,700,000 đ 10,990,000 đ
                                        02/11/2016 - 26/01/2017                                    
                                                                Hành trình Phnom Penh - Siem Reap 4 ngày, bay hãng hàng không quốc gia Cambodia Angkor Air, giá chỉ 8,7 triệu đồng.
                                Tour Hải Nam, Trung Quốc 5 ngày giá từ 7,99 triệu đồng
                                        7,990,000 đ 9,990,000 đ
                                        19/10/2016 - 07/12/2016                                    
                                                                Hành trình 5 ngày Hải Khẩu - Hưng Long - Tam Á khởi hành ngày 1, 8, 15, 22, 29/11 và 6, 13, 20, 27/12, giá từ 7,99 triệu đồng.
                                Giảm hơn 2 triệu đồng tour Nhật Bản 6 ngày 5 đêm
                                        33,800,000 đ 35,990,000 đ
                                        01/10/2016 - 31/12/2016                                    
                                                                Hongai Tours đưa du khách khám phá xứ Phù Tang mùa lá đỏ theo hành trình Nagoya - Nara - Osaka - Kyoto - núi Phú Sĩ - Tokyo.
        Quốc tế
         
                Những câu lạc bộ thoát y nóng bỏng nhất thế giới
                        Những cô nàng chân dài với thân hình đồng hồ cát, điệu nhảy khiêu gợi, đồ uống lạnh hảo hạng và điệu nhảy bốc lửa trong các câu lạc bộ thoát y luôn kích thích các giác quan của du khách.
            Bé gái Ấn Độ hồn nhiên tóm cổ rắn hổ mang chúaBí mật đáng sợ bên dưới hệ thống tàu điện ngầm LondonNgã xuống hồ nước nóng, du khách chết mất xác
         Gửi bài viết về tòa soạn
        Ẩm thực
         
                             
            Quán bún măng vịt mỗi ngày chỉ bán một tiếng
            Nằm sâu trong con hẻm nhỏ trên đường Lê Văn Sỹ quận Tân Bình, TP HCM, quán bún vịt chỉ phục vụ khách từ 15h30 và đóng hàng sau đó chừng một tiếng.
                Vợ chồng Thu Phương nếm 'bún chửi' Hà Nội
                'Vua đầu bếp' để khách tự sáng tạo món ăn
        Cộng đồng
         
                'Thời gian biểu' du lịch Việt Nam bốn mùa đều đẹp
                        Hãy lên kế hoạch du lịch đúng thời điểm cho năm mới dựa theo lịch trình du lịch khắp miền đất nước.
            Du khách đứng tim vì voi chặn đầu xe5 vùng đất Samurai nổi tiếng của Nhật BảnĐồi cỏ hồng Đà Lạt biến thành cỏ tuyết sau một đêm
        Ảnh
         
                             
            Chùa 've chai' nắm giữ nhiều kỷ lục ở Đà Lạt
            Tên gọi “ve chai” của chùa Linh Phước (Đà Lạt) xuất phát từ việc hầu hết chi tiết trang trí ở đây đều dùng mảnh sành, sứ hay vỏ chai tạo nên.
                                     
                Ngày Tết du ngoạn Thiền viện Trúc Lâm Tây Thiên
                                     
                Những ngôi nhà mái rạ ở 'thủ phủ' gạo tám
        Video
         
            Du khách la hét vì chơi tàu lượn dốc nhất thế giới
            Tàu lượn Takabisha (Nhật Bản) có độ dốc lên tới 121 độ, vận tốc tối đa đạt 160 km/h khiến người chơi như đứng tim khi nhiều đường ray thẳng đứng, tàu như rơi tự do.
                Cả công viên 'nín thở' chờ du khách béo trượt máng
                Cảnh đẹp ngoạn mục trên khắp nẻo đường Na Uy
        Điểm đến yêu thích                
                    Kế hoạch 4 ngày khám phá đảo thiên đường ở Campuchia                
                    Những món ngon nên thử của Phú Quốc                
                    Khách du lịch được mời dùng toilet miễn phí ở Đà Nẵng                
                    Chuyến bay Cần Thơ - Đà Lạt chỉ mất một giờ                
                    Những cây cầu có tên gọi độc đáo ở miền Tây                
		Liên kết du lịch
					Trang chủ
                         Đặt VnExpress làm trang chủ
                         Đặt VnExpress làm trang chủ
                         Về đầu trang
                     
								  Thời sự
								  Góc nhìn
								  Pháp luật
								  Giáo dục
								  Thế giới
								  Gia đình
								  Kinh doanh
							   Hàng hóa
							   Doanh nghiệp
							   Ebank
								  Giải trí
							   Giới sao
							   Thời trang
							   Phim
								  Thể thao
							   Bóng đá
							   Tennis
							   Các môn khác
								  Video
								  Ảnh
								  Infographics
								  Sức khỏe
							   Các bệnh
							   Khỏe đẹp
							   Dinh dưỡng
								  Du lịch
							   Việt Nam
							   Quốc tế
							   Cộng đồng
								  Khoa học
							   Môi trường
							   Công nghệ mới
							   Hỏi - đáp
								  Số hóa
							   Sản phẩm
							   Đánh giá
							   Đời sống số
								  Xe
							   Tư vấn
							   Thị trường
							   Diễn đàn
								  Cộng đồng
								  Tâm sự
								  Cười
								  Rao vặt
                    Trang chủThời sựGóc nhìnThế giớiKinh doanhGiải tríThể thaoPháp luậtGiáo dụcSức khỏeGia đìnhDu lịchKhoa họcSố hóaXeCộng đồngTâm sựVideoCườiRao vặt
                            © Copyright 1997 VnExpress.net,  All rights reserved
                            ® VnExpress giữ bản quyền nội dung trên website này.
                            VnExpress tuyển dụng   Liên hệ quảng cáo  / Liên hệ Tòa soạn
                            Thông tin Tòa soạn.0123.888.0123 (HN) - 0129.233.3555 (TP HCM)
                                     Liên hệ Tòa soạn
                                     Thông tin Tòa soạn
                                    0123.888.0123 (HN) 
                                    0129.233.3555 (TP HCM)
                                © Copyright 1997 VnExpress.net,  All rights reserved
                                ® VnExpress giữ bản quyền nội dung trên website này.
                     
         
